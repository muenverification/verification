#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <inttypes.h> 


#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "commonFunctions.h"
#include "global.h"




int noOfSubjects;

struct subject_id_name{
        int id;
        char name[100];
    };
struct subject_id_name *subjectIdName;

int NumberOfNodes(char *expr, xmlXPathContextPtr BxpathCtx);
uint64_t GetCR0(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  );
uint64_t GetCR4(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  );
uint64_t GetExceptions(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc );
uint64_t Get_Pin_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc );
uint64_t Get_Proc_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc );
uint64_t Get_Proc2_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  );
uint64_t Get_Exit_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  );
uint64_t Get_Entry_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  );

uint64_t GetCR0Mask(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  );
uint64_t GetCR4Mask(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  );


int Get_Active_CPU_Count(xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc){
    
        char *BxpathExpr;
        xmlXPathObjectPtr BxpathObj;
        xmlNodeSetPtr BElements;
        xmlNodePtr current_node;

        BxpathExpr ="/system/scheduling/majorFrame";

        
        BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
        BElements = BxpathObj->nodesetval;
        current_node = BElements->nodeTab[0];  



        xmlBufferPtr buf;
        buf = xmlBufferCreate();
        xmlNodeDump(buf, Bpolicydoc, current_node, 0 \
        /* level of indentation */, 0 /* disable formatting */);
        xmlDocPtr MajorDoc;
        xmlXPathContextPtr MajorCtx; 
        MajorDoc = xmlParseDoc((char *)xmlBufferContent(buf));
        //printf("\n\n%s", (char *)xmlBufferContent(buf));

        MajorCtx = xmlXPathNewContext(MajorDoc);
        BxpathExpr = "/majorFrame/cpu[@id]";
        int numberOfActiveCpus = NumberOfNodes(BxpathExpr, MajorCtx);
        GnoOfActiveCPUs = numberOfActiveCpus;
       //  BxpathObj = xmlXPathEvalExpression(BxpathExpr, MajorCtx);
       // //xmlNodeSetPtr BarrierElement;
       //   BElements= BxpathObj->nodesetval;
       //  numberOfActiveCpus =((BElements) ? BElements->nodeNr : 0);

    return numberOfActiveCpus;
}


int HasManaged(char* expr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc ){

			xmlNodePtr current_node;
			xmlNodeSetPtr BElements;
       		//int load, save;
			xmlXPathObjectPtr BxpathObj;
            // sprintf(tempString1,"/system/subjects/subject[@id='%d']/vcpu/vmx/controls/entry/LoadDebugControls", i);    
            // BxpathExpr = tempString1;
            BxpathObj = xmlXPathEvalExpression(expr, BxpathCtx);
			BElements = BxpathObj->nodesetval;
        	current_node = BElements->nodeTab[0];        	
			return atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
}

char* GetSingleMemory(char* expr, xmlXPathContextPtr BxpathCtx, char property[]){

	xmlXPathObjectPtr singleMemObj;
    xmlNodeSetPtr SingleElement;
    singleMemObj= xmlXPathEvalExpression(expr, BxpathCtx);
    if(singleMemObj == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", expr);
        xmlXPathFreeContext(BxpathCtx); 
     	//xmlFreeDoc(Bpolicydoc); 
	    exit(0);
    }
    SingleElement= singleMemObj->nodesetval;
    return xmlGetProp(SingleElement->nodeTab[0], property);

}

int NumberOfNodes(char *expr, xmlXPathContextPtr BxpathCtx){
	//BxpathExpr = "/system/subjects/subject";
	xmlXPathObjectPtr BxpathObj;
	xmlNodeSetPtr BElements;
	BxpathObj = xmlXPathEvalExpression(expr, BxpathCtx);
    /*Validate  xpathobj*/
    if(BxpathObj == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", expr);
        xmlXPathFreeContext(BxpathCtx); 
        //xmlFreeDoc(Bpolicydoc); 
        exit(0);
    }    
    /*Retreive all elements */
    BElements = BxpathObj->nodesetval;
    return (BElements) ? BElements->nodeNr : 0;
}


char* getElementData(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
	///BxpathExpr = tempString1;
		xmlXPathObjectPtr BxpathObj;
		xmlNodeSetPtr SingleElement;
		xmlNodePtr curNodePtr;
		BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
		SingleElement = BxpathObj->nodesetval;
		curNodePtr = SingleElement->nodeTab[0];
		return (xmlNodeListGetString(Bpolicydoc, curNodePtr->xmlChildrenNode,1));
}



int maxMinFrame(int noOfCPUs, int noOfMajFrame, xmlXPathContextPtr BxpathCtx, xmlDocPtr Bpolicydoc);
int maxBarrierCount(int noOfMajFrame, xmlXPathContextPtr BxpathCtx, xmlDocPtr Bpolicydoc );

int subjectNameToId(char *subjectName){
	for(int i = 0; i < noOfSubjects; i++){
		if(!strcmp(subjectIdName[i].name,subjectName)){
			return i;
		}
	}

}

char* subjecIDToName(int id){
	return subjectIdName[id].name;
}


int fileAppender(char ReadFile[],FILE *fptr2){
    
    FILE *fptr1;
    char filename[100],writeFileName[100], c;

    strncpy( filename, ReadFile, 100 );
    //strncpy( writeFileName, WriteFile, 100 );

    fptr1 = fopen(filename, "r");
    if (fptr1 == NULL)
    {
        printf("Cannot open file %s \n", filename);
        exit(0);
    }

    // Read contents from file
    c = fgetc(fptr1);
    while (c != EOF)
    {
        fputc(c, fptr2);
        c = fgetc(fptr1);
    }

    //printf("Contents copied to %s\n", fptr2);
 
    fclose(fptr1);
    //fclose(fptr2);
    return 0;

}




int ds_check(int argc,char *argv[]){
	xmlDocPtr Bpolicydoc;
	xmlXPathContextPtr BxpathCtx; 
	xmlXPathObjectPtr BxpathObjMemAll, singleMemObj, BxpathObj;
	xmlChar* BxpathExprMemory;
	xmlChar* BxpathExpr;
	xmlChar* MemElementExpr;
	xmlNodeSetPtr SingleElement;
	xmlNodeSetPtr BmemoryElements, BElements;
	int noOfMemElements, noOfElements, noOfCPUs;


	FILE *fptr;

	/* Init libxml */     
    xmlInitParser();
    LIBXML_TEST_VERSION

    /* Load XML document */
    Bpolicydoc = xmlParseFile(argv[2]);
    if (Bpolicydoc == NULL) {
    	fprintf(stderr, "Error: unable to parse file \"%s\"\n", argv[1]);
    	return -1;
    }

    /* Create xpath evaluation context */
    BxpathCtx = xmlXPathNewContext(Bpolicydoc);
    if(BxpathCtx == NULL) {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    fptr = fopen("AdaFiles/ds_check.adb", "w");
    if (fptr == NULL)
    {
        printf("Cannot open file\n");
        exit(0);
    }

	printf("\n\nGenerating data structures.....\n\n");
    fileAppender("AdaFiles/ds_check_start.txt",fptr);

    fprintf(fptr, "\n --skp.kernel.ads\n");

    //Skp.Kernel.Stack_Address
    fprintf(fptr, "\tG_Stack_Address : constant :=%s;\n",GetSingleMemory("\
    	/system/kernel/memory/cpu[@id='0']/memory[@logical='stack']", BxpathCtx, "virtualAddress"));
    fprintf(fptr, "\tG_Stack_size : constant := %s;\n",GetSingleMemory(\
    	"/system/memory/memory[@name='kernel_stack_0']", BxpathCtx, "size"));

    //skp.Kernel.CPU_Store_Address
    //fprintf(fptr, "\tG_CPU_Store_Address : constant := %s;\n",GetSingleMemory(\
    //	"/system/kernel/memory/cpu[@id='0']/memory[@logical='store']", BxpathCtx,"virtualAddress") );

    //skp.kernel.Subj_States_Address
    fprintf(fptr, "\tG_Subj_States_Address : constant := %s;\n",GetSingleMemory(\
    	"/system/kernel/memory/cpu/memory[@logical='tau0|state']", BxpathCtx,"virtualAddress") );


    //skp.kernel.Subj_Timers_Address
    // fprintf(fptr, "\tG_Subj_Timers_Address : constant := %s;\n",GetSingleMemory(\
    // 	"/system/kernel/memory/cpu/memory[@logical='tau0|timer']", BxpathCtx,"virtualAddress") ); 

 	//skp.kernel.IO_Apic_Address
    fprintf(fptr, "\tG_IO_Apic_Address : constant := %s;\n",GetSingleMemory(\
    	"/system/kernel/devices/device[@logical='ioapic']/memory", BxpathCtx,"virtualAddress") );

 	//skp.kernel.Subj_Sinfo_Address
	// fprintf(fptr, "\tG_Subj_Sinfo_Address : constant := %s;\n",GetSingleMemory(\
 //    	"/system/kernel/memory/cpu/memory[@logical='tau0|sinfo']", BxpathCtx,"virtualAddress") );
 
 	//skp.kernel.Subj_Sinfo_Size
	fprintf(fptr, "\tG_Subj_Sinfo_Size : constant := %s;\n",GetSingleMemory(\
    	"/system/memory/memory[@name='tau0|sinfo']", BxpathCtx,"size") );
 

	fprintf(fptr, "\n\t----skp.ads\n");
	
	//skp.CPU_Count
	//noOfCPUs = atoi(GetSingleMemory("/system/hardware/processor", BxpathCtx,"cpuCores")); 
    noOfCPUs = Get_Active_CPU_Count(BxpathCtx, Bpolicydoc);
	fprintf(fptr, "\tG_CPU_Count : constant := %d;\n", noOfCPUs);

    
	
	//skp.Number_Of_Subjects
	noOfSubjects = NumberOfNodes("/system/subjects/subject", BxpathCtx);
	fprintf(fptr, "\tG_Subject_Count : constant := %d;\n", noOfSubjects);

	//skp.vmxon_address
	fprintf(fptr, "\tG_Vmxon_Address : constant := %s;\n",GetSingleMemory(\
    	"/system/memory/memory[@type='system_vmxon' and contains(string(@name),'kernel_0')]",\
    	 BxpathCtx,"physicalAddress") );


	fprintf(fptr, "\n\t----skp-scheduling.ads\n");
	//skp-scheduling.VMX_Timer_Rate : constant := 
	fprintf(fptr, "\tG_VMX_Timer_Rate : constant := %s;\n",GetSingleMemory(\
    	"/system/hardware/processor", BxpathCtx,"vmxTimerRate") );


	
	int noOfMajFrame = NumberOfNodes("/system/scheduling/majorFrame", BxpathCtx);

	
	subjectIdName =(struct subject_id_name *) malloc(noOfSubjects * sizeof(struct subject_id_name));



	BxpathExpr ="/system/subjects/subject[@globalId]";
    BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);

    if(BxpathObj == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExpr);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    BElements= BxpathObj->nodesetval;   


	for(int i = 0; i < noOfSubjects; ++i) {    
	    subjectIdName[i].id = atoi(xmlGetProp(BElements->nodeTab[i], "globalId"));
        strcpy(subjectIdName[i].name,  xmlGetProp(BElements->nodeTab[i], "name") );
    }


	int subjectToGroupID[noOfSubjects];
	for(int x=0; x< noOfSubjects; x++){
		subjectToGroupID[x] = 0;
	}
	//memset(subjectToGroupID, 0, noOfSubjects);
	char tempString1[100];

	int subjectID, nextFreeGroupID = 1;
	//xmlXPathObjectPtr Bx;
    xmlNodeSetPtr majFrameElements;

	// xmlKeepBlanksDefault (0);


	uint64_t CPU_Speed_Hz, timerFactor, Cycles_Count;
	BxpathExpr ="/system/hardware/processor";
    BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    BElements= BxpathObj->nodesetval; 	
	CPU_Speed_Hz = atoi(xmlGetProp(BElements->nodeTab[0], "speed"));
	CPU_Speed_Hz = CPU_Speed_Hz * 1000000;
	BxpathExpr ="/system/scheduling";
    BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    BElements= BxpathObj->nodesetval; 	
	timerFactor = CPU_Speed_Hz / atoi(xmlGetProp(BElements->nodeTab[0], "tickRate"));
	 
	

	int maxMinFrameNumber = maxMinFrame(noOfCPUs, noOfMajFrame, BxpathCtx, Bpolicydoc);

	//printf("maxMinFrame %d\n", maxMinFrameNumber);

	 fprintf(fptr,"\tG_Scheduling_Plans : constant Scheduling_Plan_Type := Scheduling_Plan_Type'(\n");
	
  	uint64_t majorFramePeriod[noOfMajFrame];

	printf("\n noof cput %d", noOfCPUs);

 
	//sprintf(tempString1,"/system/scheduling/majorFrame/cpu[@id]");
	
	//int numberOfScheduledCores=NumberOfNodes(tempString1, BxpathCtx);
	//printf("\n noof cput %d", numberOfScheduledCores);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //for(int i = 0; i < 2; i++){

	int activeCPUCount = Get_Active_CPU_Count(BxpathCtx, Bpolicydoc);

   // printf("activeCPUCount = %d\n", activeCPUCount);

    for(int i = 0; i < activeCPUCount; i++){
    	fprintf(fptr, "\t\t%d=> Major_Frame_Array'(\n",i );
    	 
		for(int j = 0; j < noOfMajFrame; j++){
			fprintf(fptr, "\t\t\t%d => Major_Frame_Type'\n",j );
			
	
		 	sprintf(tempString1, "/system/scheduling/majorFrame/cpu[@id='%d']", i);
		 	BxpathExpr = tempString1;
   			BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	    	if(BxpathObj == NULL) {
	        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExpr);
	        	xmlXPathFreeContext(BxpathCtx); 
	        	xmlFreeDoc(Bpolicydoc); 
	        	return -1;
	    	}
    		majFrameElements = BxpathObj->nodesetval;

     		xmlNodePtr curMajElement = majFrameElements->nodeTab[j];



     		/*TODO: change to child/sibling format */
     		xmlBufferPtr buf;
     		buf = xmlBufferCreate();
			// dump the node
			xmlNodeDump(buf, Bpolicydoc, curMajElement, 0 \
			/* level of indentation */, 0 /* disable formatting */);

			//printf("\n\n%s", (char *)xmlBufferContent(buf));

			xmlDocPtr minFramDoc;
			xmlXPathContextPtr minFrameCtx; 
			minFramDoc = xmlParseDoc((char *)xmlBufferContent(buf));
		    if (minFramDoc == NULL) {
			
		    	fprintf(stderr, "Error: unable to parse !!!");
		    	return -1;
		    }

		    /* Create xpath evaluation context */
		    minFrameCtx = xmlXPathNewContext(minFramDoc);
		    if(minFrameCtx == NULL) {
		        fprintf(stderr,"Error: unable to create new XPath context\n");
		        xmlFreeDoc(minFramDoc); 
		        return -1;
		    }


		    BxpathExpr = "/cpu/minorFrame[@subject]";
   			BxpathObj = xmlXPathEvalExpression(BxpathExpr, minFrameCtx);
	    	if(BxpathObj == NULL) {
	        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExpr);
	        	xmlXPathFreeContext(minFrameCtx); 
	        	xmlFreeDoc(minFramDoc); 
	        }
	        xmlNodeSetPtr minElemnt;
	        minElemnt= BxpathObj->nodesetval;
        	

        	int noOfMinElements = (minElemnt) ? minElemnt->nodeNr : 0;
        	fprintf(fptr, "\t\t\t (Length       => %d,\n",noOfMinElements );
        	fprintf(fptr, "\t\t\t  Minor_Frames => Minor_Frame_Array'(\n" );
        	
        	
        	//printf("noofminelements%d\n", noOfMinElements);
	
        	Cycles_Count = 0;
        	for(int z=0; z < noOfMinElements; z++){
        	 	//xmlGetProp(minElemnt->nodeTab[i], "name") ;

		
        	 	fprintf(fptr, "\t\t\t\t%d => Minor_Frame_Type'(Group_ID =>",z+1 );
        	 	subjectID = subjectNameToId(xmlGetProp(minElemnt->nodeTab[z], "subject"));
        	 	//printf("\n%d => %s, %d\n",subjectID,xmlGetProp(minElemnt->nodeTab[0], "subject"),\
        	 	 subjectToGroupID[subjectID]);  
        	 	if(subjectToGroupID[subjectID] == 0){
        	 		subjectToGroupID[subjectID] = nextFreeGroupID;
        	 		nextFreeGroupID = nextFreeGroupID + 1;
        	 	}
        		fprintf(fptr, "%d,\n", subjectToGroupID[subjectID] );
        		fprintf(fptr, "\t\t\t\t\tBarrier  => " );
        		if(!strcmp(xmlGetProp(minElemnt->nodeTab[z], "barrier"), "none")){
        			fprintf(fptr, "No_Barrier,\n");
        		}else{
        			fprintf(fptr, "%s,\n", xmlGetProp(minElemnt->nodeTab[z], "barrier"));
        		}
        		fprintf(fptr, "\t\t\t\t\tDeadline => ");
        		Cycles_Count = Cycles_Count + (timerFactor * atoi(\
        			xmlGetProp(minElemnt->nodeTab[z], "ticks")));

        		fprintf(fptr,"%" PRIu64 ")", Cycles_Count);

        		if(z < maxMinFrameNumber - 1 ){
        			fprintf(fptr, ",\n" );
        		}
        	 	
        	 }

        	 // if((noOfMinElements < maxMinFrameNumber) && (j < noOfMajFrame -1)){
        	 // 	fprintf(fptr, "\t\t\t\t\t others => Null_Minor_Frame))" );
        	 // }else if((noOfMinElements < maxMinFrameNumber) && (j == noOfMajFrame -1)){
        	 // 	fprintf(fptr, "\t\t\t\t\t others => Null_Minor_Frame)))" );
        	 // }else{
        	 // 	fprintf(fptr, "))" );//////////////////////////////////))))))))))))))
        	 // }


             if((noOfMinElements < maxMinFrameNumber) && (j < noOfMajFrame -1)){
                fprintf(fptr, "\t\t\t\t\t others => Null_Minor_Frame))" );
             }else if((noOfMinElements < maxMinFrameNumber) && (j == noOfMajFrame -1) && (i < activeCPUCount-1)  ){
                fprintf(fptr, "\t\t\t\t\t others => Null_Minor_Frame))" );
             }else if((noOfMinElements < maxMinFrameNumber) && (j == noOfMajFrame -1) && (i == activeCPUCount-1)){
                fprintf(fptr, "\t\t\t\t\t others => Null_Minor_Frame)))" );
             }else{
                fprintf(fptr, "))" );//////////////////////////////////))))))))))))))
             }
      		
        	 // if( (i < activeCPUCount -1) || ((i == activeCPUCount -1) && ( (j != noOfMajFrame - 1))) ){
        	 // 	fprintf(fptr, ",\n" );
        	 // }else {
        	 // 	fprintf(fptr, ");\n" );
        	 // } 


             if( (i < activeCPUCount-1) && (j == noOfMajFrame -1) ){
                fprintf(fptr, "),\n" );
             }else if((i < activeCPUCount-1) && (j != noOfMajFrame -1) ){
                        fprintf(fptr, ",\n" );
             }else if((i == activeCPUCount -1) && ( (j != noOfMajFrame - 1))){
                        fprintf(fptr, ",\n" );
             }else {
                fprintf(fptr, ");\n" );
             } 









        	 if(i == 0){
        	 	majorFramePeriod[j] = Cycles_Count;
        	 }  		

		}
	}


	int maxBarrier = maxBarrierCount(noOfMajFrame, BxpathCtx, Bpolicydoc);
	xmlNodeSetPtr BarrierElements, BarrierElement;
	fprintf(fptr, "\n\t\t----MajorFrame_info\n");
	fprintf(fptr, "\t\t G_Major_Frames : constant Major_Frame_Info_Array := Major_Frame_Info_Array'(\n");
	for(int i = 0; i< noOfMajFrame; i++){
//printf("\ncurrent majorFrame  = %d ", i);

		fprintf(fptr, "\t\t\t%d => Major_Frame_Info_Type'\n \
         	\t\t\t(Period => ", i);
		fprintf(fptr,"%" PRIu64 ",\n", majorFramePeriod[i]);
		fprintf(fptr, "\t\t\t\tBarrier_Config => Barrier_Config_Array'(\n");

		BxpathExpr = "/system/scheduling/majorFrame/barriers";
		BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    		BarrierElements = BxpathObj->nodesetval;
 		xmlNodePtr currBarrierElement = BarrierElements->nodeTab[i];
		xmlBufferPtr buf;
 		buf = xmlBufferCreate();
		xmlNodeDump(buf, Bpolicydoc, currBarrierElement, 0 \
		/* level of indentation */, 0 /* disable formatting */);
		xmlDocPtr barrierDoc;
		xmlXPathContextPtr barrierCtx; 
		barrierDoc = xmlParseDoc((char *)xmlBufferContent(buf));
	    	barrierCtx = xmlXPathNewContext(barrierDoc);
	    	BxpathExpr = "/barriers/barrier";

		BxpathObj = xmlXPathEvalExpression(BxpathExpr, barrierCtx);
    	xmlNodeSetPtr BarrierElement;
        BarrierElement= BxpathObj->nodesetval;
        int numberOfBarrier =((BarrierElement) ? BarrierElement->nodeNr : 0);
//printf("  number of barriers = %d", numberOfBarrier);
        for(int j = 0; j < numberOfBarrier ; j++ ){
        		fprintf(fptr, "\t\t\t\t%s => %s ", xmlGetProp(BarrierElement->nodeTab[j], "id"),\
        			xmlGetProp(BarrierElement->nodeTab[j], "size"));
        		if( j < numberOfBarrier -1){
        			fprintf(fptr, ",\n");
        		}


        }

        if( (numberOfBarrier < maxBarrier) &&(numberOfBarrier > 0)){
        	fprintf(fptr, ",\nothers => Barrier_Size_Type'First))");
        }else if((numberOfBarrier < maxBarrier) &&(numberOfBarrier == 0) ){
        	//fprintf(fptr, "))");
        	fprintf(fptr, "\t\t\t\tothers => Barrier_Size_Type'First))");
        }else if((numberOfBarrier == maxBarrier)){
        	fprintf(fptr, "))");
        }

        if(i < noOfMajFrame - 1 ){
        	fprintf(fptr, ",\n" );
        }else{
        	fprintf(fptr, ");\n" );
        }


	}
		
		//printf("maxBarrierCount = %d\n", maxBarrier );


	int LastGroupID = nextFreeGroupID -1;
	int Scheduling_Groups[LastGroupID+1];
	int Group_ID;
	for(int i = 0; i < noOfSubjects; i++){
		Group_ID = subjectToGroupID[i];
		if(Group_ID != 0){
			Scheduling_Groups[Group_ID] = i;
		}
	}




	fprintf(fptr, "\n\t\t\t ---scheduling_group\n" );
	fprintf(fptr, "\t\t  G_Scheduling_Groups : constant Scheduling_Group_Array\n \
     						:= Scheduling_Group_Array'(");
	for(int i = 1; i < LastGroupID+1 ; i++){
		fprintf(fptr, "\n\t\t\t\t  %d => %d", i, Scheduling_Groups[i] );
		if(i < LastGroupID){
			fprintf(fptr, ",");
		}
	}

	fprintf(fptr, ");\n" );

	fprintf(fptr, "\n\t G_Scheduling_Group_Range_Last : constant := %d; \n", LastGroupID );
	fprintf(fptr, "\t G_Barrier_Index_Range_Last : constant := %d; \n", maxBarrier );
	fprintf(fptr, "\t G_Minor_Frame_Range_Last : constant := %d; \n", maxMinFrameNumber );
	fprintf(fptr, "\t G_Major_Frame_Range_Last : constant := %d; \n", noOfMajFrame );

 fprintf(fptr, "\t Active_Cpu_Number : constant := %d; \n", activeCPUCount );

    fprintf(fptr, "\t subtype Active_Cpu_Number_Range is CPU_Range range 0 .. %d - 1;\n",activeCPUCount);
    

	//xmlNodePtr curElement;	
	//xmlXPathObjectPtr singleMemObj;
    
	fprintf(fptr, "\n\n---skp-subjects.adb\n");
	fprintf(fptr, "\tG_Subject_Specs : constant Subject_Spec_Array := Subject_Spec_Array'(\n");
	char* res_getElementData;

	for(int i = 0; i < noOfSubjects; i++){
	//for(int i = 0; i < 1; i++){
		fprintf(fptr, "\t\t%d => Subject_Spec_Type'(\n\t\t CPU_Id\t\t => ", i);
		sprintf(tempString1, "/system/subjects/subject[@globalId='%d']", i);
		fprintf(fptr, "%s,\n",GetSingleMemory(tempString1, BxpathCtx,"cpu"));


		sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/proc2/EnableEPT", i);
		int fl = atoi(getElementData(tempString1,BxpathCtx,Bpolicydoc));

		sprintf(tempString1,"/system/memory/memory[@name='%s|pt']",subjecIDToName(i));
		//printf("%s\n",tempString1 );
		//res_getElementData = 
		if(fl == 1){
			fprintf(fptr, "\t\t PML4_Address\t => 0,\n\t\t EPT_Pointer\t => %s + 16#1e#,",\
			 GetSingleMemory(tempString1, BxpathCtx,"physicalAddress"));
		}else{
			fprintf(fptr, "\t\t PML4_Address\t => %s,\n\t\t EPT_Pointer\t => 0,",\
			 GetSingleMemory(tempString1, BxpathCtx,"physicalAddress"));
		}


		sprintf(tempString1,"/system/memory/memory[@name='%s|vmcs']",subjecIDToName(i));
		fprintf(fptr, "\n\t\t VMCS_Address\t => %s,\n",\
			GetSingleMemory(tempString1, BxpathCtx,"physicalAddress") );

		sprintf(tempString1,"/system/memory/memory[@name='%s|iobm']",subjecIDToName(i));
		fprintf(fptr, "\t\t IO_Bitmap_Address => %s,",\
			GetSingleMemory(tempString1, BxpathCtx,"physicalAddress") );

		sprintf(tempString1,"/system/memory/memory[@name='%s|msrbm']",subjecIDToName(i));
		fprintf(fptr, "\n\t\t MSR_Bitmap_Address => %s,",\
			GetSingleMemory(tempString1, BxpathCtx,"physicalAddress") );


		sprintf(tempString1,"/system/memory/memory[@name='%s|msrstore']",subjecIDToName(i));

	

    	if( NumberOfNodes(tempString1, BxpathCtx) != 0){
    		fprintf(fptr, "\n\t\t MSR_Store_Address => %s,", \
    			GetSingleMemory(tempString1, BxpathCtx,"physicalAddress") );
    	}else{
    		fprintf(fptr, "\n\t\t MSR_Store_Address => 16#0000#,");
    	}

    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/registers/gpr/rsp", i);
    	fprintf(fptr, "\n\t\t Stack_Address => %s,", getElementData(tempString1,BxpathCtx,Bpolicydoc));
    	
    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/registers/gpr/rip", i);
    	fprintf(fptr, "\n\t\t Entry_Point => %s,", getElementData(tempString1,BxpathCtx,Bpolicydoc));
    	

    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/registers/cr0/*", i);
    	fprintf(fptr, "\n\t\t CR0_Value => ");
    	fprintf(fptr,"16#%" PRIx64 "#,", GetCR0(tempString1, BxpathCtx, Bpolicydoc));


    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/masks/cr0/*", i);
    	fprintf(fptr, "\n\t\t CR0_Mask => ");
    	fprintf(fptr,"16#%" PRIx64 "#,", GetCR0Mask(tempString1, BxpathCtx, Bpolicydoc));


    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/registers/cr4/*", i);
    	fprintf(fptr, "\n\t\t CR4_Value => ");
    	fprintf(fptr,"16#%" PRIx64 "#,", GetCR4(tempString1, BxpathCtx, Bpolicydoc));


    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/masks/cr4/*", i);
    	fprintf(fptr, "\n\t\t CR4_Mask => ");
    	fprintf(fptr,"16#%" PRIx64 "#,", GetCR4Mask(tempString1, BxpathCtx, Bpolicydoc));
  	
    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/segments/cs", i);
    	fprintf(fptr, "\n\t\t CS_Access => %s,", \
    		GetSingleMemory(tempString1, BxpathCtx,"access"));
  

    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/masks/exception/*", i);
    	fprintf(fptr, "\n\t\t Exception_Bitmap => ");
    	fprintf(fptr,"16#%" PRIx64 "#,", GetExceptions(tempString1, BxpathCtx, Bpolicydoc));
  	
   
    	int msr_count = 0, no_of_msrs = 0;


    	sprintf(tempString1,"/system/memory/memory[@type='kernel_msrstore' and @name='%s|msrstore']",subjecIDToName(i));
        no_of_msrs = NumberOfNodes(tempString1, BxpathCtx);


       	if(no_of_msrs != 0) {

       		xmlNodePtr current_node;
       		int load, save, Debug_Ctrl, PERF_Ctrl, PAT_Ctrl, EFER_Ctrl, msr_start, msr_end;

            sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/entry/LoadDebugControls", i);    
			load = HasManaged(tempString1, BxpathCtx, Bpolicydoc);
			sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/exit/SaveDebugControls", i); 
			save = HasManaged(tempString1, BxpathCtx, Bpolicydoc);

			Debug_Ctrl = load && save;

			sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/entry/LoadIA32PERFGLOBALCTRL", i); 
			PERF_Ctrl = HasManaged(tempString1, BxpathCtx, Bpolicydoc);


			sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/entry/LoadIA32PAT", i);    
			load = HasManaged(tempString1, BxpathCtx, Bpolicydoc);
			sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/exit/SaveIA32PAT", i); 
			save = HasManaged(tempString1, BxpathCtx, Bpolicydoc);

			PAT_Ctrl = load && save;

			sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/entry/LoadIA32EFER", i);    
			load = HasManaged(tempString1, BxpathCtx, Bpolicydoc);
			sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/exit/SaveIA32EFER", i); 
			save = HasManaged(tempString1, BxpathCtx, Bpolicydoc);

			EFER_Ctrl = load && save;


			sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/registers/msrs/msr[@mode='w' or @mode='rw']", i);    
			BxpathExpr = tempString1;			
    		BxpathObj= xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
       		BElements= BxpathObj->nodesetval;

       		uint64_t start, end;
       		int curMsrElements = 0;
       		curMsrElements = NumberOfNodes(tempString1, BxpathCtx);

       		//printf("noofmsrs%d\n", curMsrElements);

       		uint64_t IA32_SYSENTER_CS   = 0x00000174;
            uint64_t IA32_SYSENTER_ESP  = 0x00000175;
            uint64_t IA32_SYSENTER_EIP  = 0x00000176;
            uint64_t IA32_DEBUGCTL      = 0x000001d9;
            uint64_t IA32_PAT              = 0x00000277;
            uint64_t IA32_PERF_GLOBAL_CTRL = 0x0000038f;
            uint64_t IA32_EFER            = 0xc0000080;
            uint64_t IA32_STAR             = 0xc0000081;
            uint64_t IA32_LSTAR          = 0xc0000082;
            uint64_t IA32_FMASK           = 0xc0000084;
            uint64_t IA32_FS_BASE         = 0xc0000100;
            uint64_t IA32_GS_BASE          = 0xc0000101;
            uint64_t IA32_KERNEL_GS_BASE  = 0xc0000102;



       		for(int k = 0; k < curMsrElements; k++ ){
	   			
   				strcpy(tempString1,xmlGetProp(BElements->nodeTab[k], "start"));
   				formatHex(tempString1);
   				start = convtodecnum(tempString1);
       			
       			strcpy(tempString1,xmlGetProp(BElements->nodeTab[k], "end"));
   				formatHex(tempString1);
   				end = convtodecnum(tempString1);

   				int msrResult = 0;

   				//printf("start = %" PRIu64 " \n ", start);
   				//printf("end = %" PRIu64 " \n ", end);

   				for(uint64_t l = start; l <= end; l++ ){
   						msrResult = 0;

   						if( (l == IA32_SYSENTER_CS) || (l ==IA32_SYSENTER_ESP) || (l ==IA32_SYSENTER_EIP)\
   						 || (l == IA32_FS_BASE)|| (l ==IA32_GS_BASE)){
   							msrResult = 1;
   					}else if(l == IA32_DEBUGCTL){
   							msrResult = Debug_Ctrl;
   					}else if(l == IA32_PAT){
   							msrResult = PAT_Ctrl;
   					}else if(l == IA32_PERF_GLOBAL_CTRL){
   							msrResult = PERF_Ctrl;
   					}else if(l == IA32_EFER){
   							msrResult = EFER_Ctrl;
   					}else{
   						msrResult = 0;
   					}

					
					if(msrResult == 0){
							msr_count = msr_count + 1;
					}
				}

       		}
       		fprintf(fptr, "\n\t\t MSR_Count => %d,", msr_count);
			//printf("%d, %d => %d\n",load, save ,Debug_Ctrl);

        }else{
        	fprintf(fptr, "\n\t\t MSR_Count => 0,");
        }
    	//

//MSR_Count



    	fprintf(fptr, "\n\t\t VMX_Controls => VMX_Controls_Type'(");

    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/pin/*", i);
    	fprintf(fptr, "\n\t\t\t Exec_Pin => ");
    	fprintf(fptr,"%" PRIu64 ",", Get_Pin_Controls(tempString1, BxpathCtx, Bpolicydoc));

    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/proc/*", i);
    	fprintf(fptr, "\n\t\t\t Exec_Proc => ");
    	fprintf(fptr,"%" PRIu64 ",", Get_Proc_Controls(tempString1, BxpathCtx, Bpolicydoc));


    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/proc2/*", i);
    	fprintf(fptr, "\n\t\t\t Exec_Proc2 => ");
    	fprintf(fptr,"%" PRIu64 ",", Get_Proc2_Controls(tempString1, BxpathCtx, Bpolicydoc));

    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/exit/*", i);
    	fprintf(fptr, "\n\t\t\t Exit_Ctrls => ");
    	fprintf(fptr,"%" PRIu64 ",", Get_Exit_Controls(tempString1, BxpathCtx, Bpolicydoc));

    	sprintf(tempString1,"/system/subjects/subject[@globalId='%d']/vcpu/vmx/controls/entry/*", i);
    	fprintf(fptr, "\n\t\t\t Entry_Ctrls => ");
    	fprintf(fptr,"%" PRIu64 ")", Get_Entry_Controls(tempString1, BxpathCtx, Bpolicydoc));



    	int trapCount;int event_id;char tempString2[300];
    	xmlNodeSetPtr trapElements, targetElement;
    	xmlXPathObjectPtr xpathresult;

	
		sprintf(tempString2,"/system/subjects/subject[@globalId='%d']/events/source/group[@name='vmx_exit']/*", i);
		trapCount = 0;
	    trapCount = NumberOfNodes(tempString2, BxpathCtx);
  
    	// fprintf(fptr, "\n\t\t Trap_Table => ");
    	// if(trapCount == 0){
    	// 	fprintf(fptr, "Null_Trap_Table,");
    	// }else{
     //            sprintf(tempString2,"/system/subjects/subject[@globalId='%d']/events/source/group[@name='vmx_exit']/event", i);
     //            BxpathExpr = tempString2;
     //            xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
     //            trapElements = xpathresult->nodesetval;
     //            fprintf(fptr,"Trap_Table_Type'(");


     //        for(int j = 0; j< trapCount; j++){
               
     //            event_id = atoi(xmlGetProp(trapElements->nodeTab[j], "id"));

     //            fprintf(fptr,"\n\t\t\t %d => Trap_Entry_Type'(Dst_Subject => ", event_id );

     //            sprintf(tempString2,"/system/subjects/subject[@globalId='%d']/events/source/group\
     //                    [@name='vmx_exit']/event[@id=%d]/notify", i, event_id);
     //            sprintf(tempString2,"/system/subjects/subject/events/target/event[@physical='%s']", \
     //            GetSingleMemory(tempString2, BxpathCtx, "physical"));   

     //            BxpathExpr = tempString2;
     //            xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
     //            targetElement = xpathresult->nodesetval;


     //           // printf("%d\n", (trapElements) ? trapElements->nodeNr : 0 );
     //            xmlNodePtr cur_node = targetElement->nodeTab[0];

     //            fprintf(fptr,"%d,", subjectNameToId(xmlGetProp(cur_node->parent->parent->parent, "name")) );

     //            //fprintf(fptr," Dst_Vector => %s),", xmlGetProp(cur_node, "vector"));

     //            if(!strcmp(xmlGetProp(cur_node, "vector"),"none")){
     //            	fprintf(fptr," Dst_Vector => Skp.Invalid_Vector),");
     //            }else{
     //            	fprintf(fptr," Dst_Vector => %s),", xmlGetProp(cur_node, "vector"));
     //            }
     //        }
     //        fprintf(fptr,"\n\t\t\t others => Null_Trap),");
	    // }		
	

// 	    int eventCount;
// 	    xmlNodeSetPtr eventElements;

// 	    fprintf(fptr, "\n\t\t Event_Table => ");
// 	    sprintf(tempString2,"/system/subjects/subject[@globalId='%d']/events/source/group[@name='vmcall']/*", i);
// 		eventCount = 0;
// 	    eventCount = NumberOfNodes(tempString2, BxpathCtx);


// 		if(eventCount == 0){
// 			fprintf(fptr, "Null_Event_Table");
// 		}else{

// 			fprintf(fptr, "Event_Table_Type'(" );
// 			sprintf(tempString2,"/system/subjects/subject[@globalId='%d']/events/source/group[@name='vmcall']/event", i);
// 	        BxpathExpr = tempString2;
// 	        xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
// 	        eventElements = xpathresult->nodesetval;


// 			//fprintf(fptr, "Event_Table_Type'(", );

// 			for(int j = 0; j < eventCount; j++ ){
// 				event_id = atoi(xmlGetProp(eventElements->nodeTab[j], "id"));

// 				fprintf(fptr, "\n\t\t   %d => Event_Entry_Type'(",event_id );

// 				// sprintf(tempString2,"/system/subjects/subject[@globalId='%d']/events/source/group\
// 	   //                  [@name='vmcall']/event[@id=%d]/notify", i, event_id);
// 	   //          sprintf(tempString2,"/system/subjects/subject/events/target/event[@physical='%s']", \
// 	   //          GetSingleMemory(tempString2, BxpathCtx, "physical"));   

//                 strcpy(tempString1, xmlGetProp(eventElements->nodeTab[j], "physical"));
//                 sprintf(tempString2,"/system/subjects/subject/events/target/event[@physical='%s']", \
//                   tempString1);   

// 	            BxpathExpr = tempString2;
// 	            xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
// 	            targetElement = xpathresult->nodesetval;

// 	            xmlNodePtr cur_node = targetElement->nodeTab[0];

// 	            fprintf(fptr,"\n\t\t\t Dst_Subject => %d,", subjectNameToId(\
// 	            	xmlGetProp(cur_node->parent->parent->parent, "name")) );

// 	            //fprintf(fptr,"\n\t\t\t Dst_Vector => %s),", xmlGetProp(cur_node, "vector"));
// 	            if(!strcmp(xmlGetProp(cur_node, "vector"),"none")){
// 	            	fprintf(fptr,"\n\t\t\t Dst_Vector => Skp.Invalid_Vector,");
// 	            }else{
// 	            	fprintf(fptr,"\n\t\t\t Dst_Vector => %s,", xmlGetProp(cur_node, "vector"));
// 	            }           
// printf("\n\n Reached Here \n\n"); 
// 	            sprintf(tempString2,"/system/subjects/subject[@globalId='%d']/events/source/group\
// 	                    [@name='vmcall']/event[@id=%d]/notify", i, event_id);
// 	            sprintf(tempString2,"/system/events/event[@name='%s']", \
// 	            GetSingleMemory(tempString2, BxpathCtx, "physical"));   

// 	            BxpathExpr = tempString2;
// 	            xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
// 	            targetElement = xpathresult->nodesetval;

// 	            cur_node = targetElement->nodeTab[0];
// 	            if(!strcmp(xmlGetProp(cur_node, "mode"),"switch")){

// 	            	fprintf(fptr,"\n\t\t\t Handover => True,");
// 	            }else{
// 	            	fprintf(fptr,"\n\t\t\t Handover => False,");
// 	            }

// 	            if(!strcmp(xmlGetProp(cur_node, "mode"),"ipi")){

// 	            	fprintf(fptr,"\n\t\t\t Send_IPI => True),");
// 	            }else{
// 	            	fprintf(fptr,"\n\t\t\t Send_IPI => False),");
// 	            }


// 			}
// 			fprintf(fptr,"\n\t\t\t others => Null_Event)");

// 		}
//printf("\n\n ReachedEnd \n\n");

       if(i < noOfSubjects-1){
       	fprintf(fptr, "),\n");
       }else{
       	fprintf(fptr, "));\n");
       }



	}
  
  	fileAppender("AdaFiles/ds_check_end.txt",fptr);

    //fprintf(fptr, "\n\nend policy_ds;\n");
    fclose(fptr);

    xmlXPathFreeContext(BxpathCtx); 
    xmlFreeDoc(Bpolicydoc); 

    /* Shutdown libxml */
    xmlCleanupParser();

    if(system("gnatmake AdaFiles/ds_check.adb") == -1){
       	printf("ds_chec.adb compilation error.\n");
       	exit(0);                    
    }

    if(system("./ds_check") == -1){
       	printf("ds_chec execution error.\n");
       	exit(0);                    
    }



	return 0;
}











/* TOCHANGE ALGORITHM*/
int maxMinFrame(int noOfCPUs, int noOfMajFrame, xmlXPathContextPtr BxpathCtx, xmlDocPtr Bpolicydoc ){

	char tempString1[100];
	xmlChar* BxpathExpr;
	xmlNodeSetPtr SingleElement;
	xmlNodeSetPtr majFrameElements, minElemnt;
	int maxMinNumber = 0;
	xmlXPathObjectPtr BxpathObj;

	for(int i = 0; i < noOfCPUs; i++){
    	for(int j = 0; j < noOfMajFrame; j++){
			sprintf(tempString1, "/system/scheduling/majorFrame/cpu[@id='%d']", i);
		 	BxpathExpr = tempString1;
   			BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	    	majFrameElements = BxpathObj->nodesetval;
     		xmlNodePtr curMajElement = majFrameElements->nodeTab[j];
			xmlBufferPtr buf;
     		buf = xmlBufferCreate();
			xmlNodeDump(buf, Bpolicydoc, curMajElement, 0 \
			/* level of indentation */, 0 /* disable formatting */);
			xmlDocPtr minFramDoc;
			xmlXPathContextPtr minFrameCtx; 
			minFramDoc = xmlParseDoc((char *)xmlBufferContent(buf));
		    minFrameCtx = xmlXPathNewContext(minFramDoc);
		    BxpathExpr = "/cpu/minorFrame[@subject]";
   			BxpathObj = xmlXPathEvalExpression(BxpathExpr, minFrameCtx);
	    	xmlNodeSetPtr minElemnt;
	        minElemnt= BxpathObj->nodesetval;
	        if(maxMinNumber < ((minElemnt) ? minElemnt->nodeNr : 0)){
	        	maxMinNumber = minElemnt->nodeNr;
	        }
        	
        	}
        }
        return maxMinNumber;
}

int maxBarrierCount(int noOfMajFrame, xmlXPathContextPtr BxpathCtx, xmlDocPtr Bpolicydoc ){

	int MaxCount = 0;
	xmlChar* BxpathExpr;
	
	xmlNodeSetPtr BarrierElements, BarrierElement;
	int maxMinNumber = 0;
	xmlXPathObjectPtr BxpathObj;
	for(int j = 0; j < noOfMajFrame; j++){
		//sprintf(tempString1, "/system/scheduling/majorFrame/barriers/barrier", i);
	 	BxpathExpr = "/system/scheduling/majorFrame/barriers";
		BxpathObj = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    	BarrierElements = BxpathObj->nodesetval;
 		xmlNodePtr currBarrierElement = BarrierElements->nodeTab[j];
		xmlBufferPtr buf;
 		buf = xmlBufferCreate();
		xmlNodeDump(buf, Bpolicydoc, currBarrierElement, 0 \
		/* level of indentation */, 0 /* disable formatting */);
		xmlDocPtr barrierDoc;
		xmlXPathContextPtr barrierCtx; 
		barrierDoc = xmlParseDoc((char *)xmlBufferContent(buf));
	    barrierCtx = xmlXPathNewContext(barrierDoc);
	    BxpathExpr = "/barriers/barrier";
		BxpathObj = xmlXPathEvalExpression(BxpathExpr, barrierCtx);
    	xmlNodeSetPtr BarrierElement;
        BarrierElement= BxpathObj->nodesetval;
        if(MaxCount < ((BarrierElement) ? BarrierElement->nodeNr : 0)){
        	MaxCount = BarrierElement->nodeNr;
        }
        
    }
	return MaxCount;
}


uint64_t GetCR0(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
    xmlNodeSetPtr node;
    xmlXPathObjectPtr xpathresult;
    xmlNodePtr current_node;

    int cr0_bit_positions[11] = {0, 1, 2, 3, 4, 5, 16, 18, 29, 30, 31 };
    int curr_value;
    uint64_t return_value = 0x0000000000000000;

    xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    node = xpathresult->nodesetval;

    for (int cnt=0;cnt<node->nodeNr; cnt++) {
            //xmlNodePtrcurrent_node = node->nodeTab[cnt];
            current_node = node->nodeTab[cnt];
            curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
        //printf("current_pos : %d, current_value : %d\n", cnt, curr_value);
        if(curr_value == 1){
            return_value = return_value | (1 << cr0_bit_positions[cnt]);
        }
        
    }
    return_value = return_value & 0x00000000ffffffff;



    return return_value;

}






uint64_t GetCR0Mask(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
    xmlNodeSetPtr node;
    xmlXPathObjectPtr xpathresult;
    xmlNodePtr current_node;

    int cr0_bit_positions[11] = {0, 1, 2, 3, 4, 5, 16, 18, 29, 30, 31 };
    int curr_value;
    uint64_t return_value = 0xffffffffffffffff;

    xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    node = xpathresult->nodesetval;

    for (int cnt=0;cnt<node->nodeNr; cnt++) {
            //xmlNodePtrcurrent_node = node->nodeTab[cnt];
            current_node = node->nodeTab[cnt];
            curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
        //printf("current_pos : %d, current_value : %d\n", cnt, curr_value);
        if(curr_value == 1){
            return_value = return_value | (1 << cr0_bit_positions[cnt]);
        }else if(curr_value == 0){
            return_value = return_value ^ (1 << cr0_bit_positions[cnt]);
        }
        
    }

    return_value = return_value | 0xffffffff00000000;
    return return_value;

}












uint64_t GetCR4(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
    xmlNodeSetPtr node;
    xmlXPathObjectPtr xpathresult;
    xmlNodePtr current_node;

    
    int curr_value;
    uint64_t return_value = 0x0000000000000000;
    int cr4_bit_positions[20] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 16, 17, 18, 20, 21, 22 };

    xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    node = xpathresult->nodesetval;

    for (int cnt=0;cnt<node->nodeNr; cnt++) {
            //xmlNodePtrcurrent_node = node->nodeTab[cnt];
            current_node = node->nodeTab[cnt];
            curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
        if(curr_value == 1){
            //if(cnt != 11 || cnt != 18 || cnt != 19 )
            return_value = return_value | (1 << cr4_bit_positions[cnt]);
        }
        
    }
    return_value = return_value & 0x00000000ffffffff;

    return return_value;

}


uint64_t GetCR4Mask(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
    xmlNodeSetPtr node;
    xmlXPathObjectPtr xpathresult;
    xmlNodePtr current_node;

    
    int curr_value;
    uint64_t return_value = 0xffffffffffffffff;
    int cr4_bit_positions[20] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 16, 17, 18, 20, 21, 22 };

    xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
    node = xpathresult->nodesetval;

    for (int cnt=0;cnt<node->nodeNr; cnt++) {
            //xmlNodePtrcurrent_node = node->nodeTab[cnt];
            current_node = node->nodeTab[cnt];
            curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
        if(curr_value == 1){
            return_value = return_value | (1 << cr4_bit_positions[cnt]);
        }else{
            return_value = return_value ^ (1 << cr4_bit_positions[cnt]);
        }
        
    }
    //return_value = return_value & 0x00000000ffffffff;

    return return_value;

}

uint64_t GetExceptions(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
	xmlNodeSetPtr node;
	xmlXPathObjectPtr xpathresult;
	xmlNodePtr current_node;

	
	int curr_value;
	uint64_t return_value = 0xffffffffffffffff;
	int exception_bit_positions[18] = {0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, \
										13, 14, 16, 17, 18, 19 };

	xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	node = xpathresult->nodesetval;

	for (int cnt=0;cnt<node->nodeNr; cnt++) {
        	//xmlNodePtrcurrent_node = node->nodeTab[cnt];
        	current_node = node->nodeTab[cnt];
        	curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
		if(curr_value == 1){
			return_value = return_value | (1 << exception_bit_positions[cnt]);
		}else{
			return_value = return_value ^ (1 << exception_bit_positions[cnt]);
		}
		
    }
	return_value = return_value & 0x00000000ffffffff;

	return return_value;

}


uint64_t Get_Pin_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
	xmlNodeSetPtr node;
	xmlXPathObjectPtr xpathresult;
	xmlNodePtr current_node;

	
	int curr_value;
	uint64_t return_value = 0x0000000000000000;
	int exception_bit_positions[5] = {0, 3, 5, 6, 7 };

	xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	node = xpathresult->nodesetval;

	for (int cnt=0;cnt<node->nodeNr; cnt++) {
        	//xmlNodePtrcurrent_node = node->nodeTab[cnt];
        	current_node = node->nodeTab[cnt];
        	curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
		if(curr_value == 1){
			return_value = return_value | (1 << exception_bit_positions[cnt]);
		}
		// else{
		// 	return_value = return_value ^ (1 << exception_bit_positions[cnt]);
		// }
		
    }
	return_value = return_value & 0x00000000ffffffff;

	return return_value;

}


uint64_t Get_Proc_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
	xmlNodeSetPtr node;
	xmlXPathObjectPtr xpathresult;
	xmlNodePtr current_node;

	
	int curr_value;
	uint64_t return_value = 0x0000000000000000;
	int proc_bit_positions[21] = {2, 3,7,9, 10, 11, 12, 15, 16,19, 20, 21, \
									22, 23, 24, 25, 27, 28, 29, 30, 31};


	xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	node = xpathresult->nodesetval;

	for (int cnt=0;cnt<node->nodeNr; cnt++) {
        	//xmlNodePtrcurrent_node = node->nodeTab[cnt];
        	current_node = node->nodeTab[cnt];
        	curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
		if(curr_value == 1){
			return_value = return_value | (1 << proc_bit_positions[cnt]);
		}
		// else{
		// 	return_value = return_value ^ (1 << exception_bit_positions[cnt]);
		// }
		
    }
	return_value = return_value & 0x00000000ffffffff;

	return return_value;

}

uint64_t Get_Proc2_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
	xmlNodeSetPtr node;
	xmlXPathObjectPtr xpathresult;
	xmlNodePtr current_node;

	
	int curr_value;
	uint64_t return_value = 0x0000000000000000;
	
	int proc_bit_positions[14] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

	xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	node = xpathresult->nodesetval;

	for (int cnt=0;cnt<node->nodeNr; cnt++) {
        	//xmlNodePtrcurrent_node = node->nodeTab[cnt];
        	current_node = node->nodeTab[cnt];
        	curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
		if(curr_value == 1){
			return_value = return_value | (1 << proc_bit_positions[cnt]);
		}
		// else{
		// 	return_value = return_value ^ (1 << exception_bit_positions[cnt]);
		// }
		
    }
	return_value = return_value & 0x00000000ffffffff;

	return return_value;

}

uint64_t Get_Exit_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
	xmlNodeSetPtr node;
	xmlXPathObjectPtr xpathresult;
	xmlNodePtr current_node;

	
	int curr_value;
	uint64_t return_value = 0x0000000000000000;
	
	
	int bit_positions[9] = {2, 9, 12, 15, 18, 19, 20, 21, 22};
	xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	node = xpathresult->nodesetval;

	for (int cnt=0;cnt<node->nodeNr; cnt++) {
        	//xmlNodePtrcurrent_node = node->nodeTab[cnt];
        	current_node = node->nodeTab[cnt];
        	curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
		if(curr_value == 1){
			return_value = return_value | (1 << bit_positions[cnt]);
		}
		// else{
		// 	return_value = return_value ^ (1 << exception_bit_positions[cnt]);
		// }
		
    }
	return_value = return_value & 0x00000000ffffffff;

	return return_value;

}


uint64_t Get_Entry_Controls(char *BxpathExpr, xmlXPathContextPtr BxpathCtx,  xmlDocPtr Bpolicydoc  ){
	xmlNodeSetPtr node;
	xmlXPathObjectPtr xpathresult;
	xmlNodePtr current_node;

	
	int curr_value;
	uint64_t return_value = 0x0000000000000000;
	int bit_positions[7] = {2, 9, 10, 11, 13, 14, 15};
	
	
	xpathresult = xmlXPathEvalExpression(BxpathExpr, BxpathCtx);
	node = xpathresult->nodesetval;

	for (int cnt=0;cnt<node->nodeNr; cnt++) {
        	//xmlNodePtrcurrent_node = node->nodeTab[cnt];
        	current_node = node->nodeTab[cnt];
        	curr_value = atoi(xmlNodeListGetString(Bpolicydoc, current_node->xmlChildrenNode,1));
		if(curr_value == 1){
			return_value = return_value | (1 << bit_positions[cnt]);
		}
		// else{
		// 	return_value = return_value ^ (1 << exception_bit_positions[cnt]);
		// }
		
    }
	return_value = return_value & 0x00000000ffffffff;

	return return_value;

}
