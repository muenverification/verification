#include <stdio.h>
#include <string.h>
#include <math.h>
#include <inttypes.h> 

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>


/*Translate logical address to physical using pageTable.
	Also checks permission.*/
int64_t translateAddress(int64_t address, FILE *pageTable, int64_t cr3OrEpt, int writable, int executable, int subjectType);

int64_t find_offset(int64_t value, int64_t Base);

int64_t getPageTableEntry(int64_t address, FILE *pageTable);

int isChannelSegment(char physicalName[], xmlXPathContextPtr UxpathCtx);

struct MemNode{
    char physical_name[100];
    int64_t physical_address;
    int64_t size;
	struct MemNode *next;
};

struct subject_id_name{
    int id;
    char name[100];
    int cpu;
    int type;
    uint64_t cr3;
	long long int ptSize;
};

struct SubjectNode
{
    char LogicalName[100];
    int64_t LogicalAddress;
    int64_t PhysicalAddress;
    char PhysicalName[100];
    int64_t Size;
    char Writable[50];
    char Executable[50];
    int SubjectID;
    struct SubjectNode *Next;
};


/* Function to insert a node at the begining of a linked lsit */
void insertAtTheBegin(struct MemNode **start_ref, char physical_name[], 
						int64_t physical_address, int64_t size);
void printList(struct MemNode *start);
/* Function to bubble sort the given linked lsit */
void bubbleSort(struct MemNode *start);
/* Function to swap data of two nodes a and b*/
void swap(struct MemNode *a, struct MemNode *b);

/* Function to insert a node at the begining of a linked lsit */
void insertSubjectNode(struct SubjectNode **SubjectStart_ref, char PhysicalName[],int64_t PhysicalAddress,
                        int64_t Size,char LogicalName[],int64_t LogicalAddress,char Writable[], 
                        char Executable[], int SubjectID);
/* Function to bubble sort the given linked lsit */
void bubbleSortSubjectNode(struct SubjectNode *SubjectStart);
/* Function to swap data of two nodes a and b*/
void swapSubjectNode(struct SubjectNode *a, struct SubjectNode *b);
/* Function to print nodes in a given linked list */
void printListSubjectNode(struct SubjectNode *SubjectStart);
/* Counts no. of nodes in linked list */
int getCountSubjectNode(struct SubjectNode *start);

struct subject_id_name *subjectIdName;

int memoryCheck(int argc, char *argv[]);
