with numbers1; use numbers1;
with SK; use SK;
with SK.CPU; use SK.CPU;

package ext_int6
with SPARK_Mode is

   -- Abstract types
   type Abs_Redirection_Table_Entry_Type is record
      dest_sub: subject_number;
      dest_vector: Vector_Range;
   end record;

   type Abs_Redirection_Table_type is array (irq_number) of
     Abs_Redirection_Table_Entry_Type;

   type pending_event_sub_array_type is array (Vector_Range) of
     Boolean;

   type pending_event_array_type is array (subject_number) of
     pending_event_sub_array_type;

   type current_subject_type is array (cpu_number) of subject_number;

   type subject_CPU_mapping_type is array (subject_number) of cpu_number;

   type active_array_type is array (subject_number) of Boolean;

   type abs_IO_Bitmap_type is array (subject_number) of
     subject_IO_Bitmap_type;

   type abs_ports_type is array (Port_Number_Type) of SK.Byte;

   type Subject_State_Array_type is array (subject_number) of
     Subject_State_Type;

   type IDT_type is array (Vector_Range) of SK.Word64;

   type Abs_IDTs_type is array (subject_number) of IDT_type;

   type memory_type is array (SK.Word64) of SK.Word64;

   type stack_type is record
      stack: memory_type;
      top: SK.Word64 :=0;
   end record;

   type abs_stack_type is array (subject_number) of stack_type;



   -- Concrete types
   type IOAPIC_Redirection_Table_Entry_type is record
      dest_CPU: cpu_number;
      dest_vector: Vector_Range;
   end record;

   type IOAPIC_Redirection_Table_type is array (irq_number) of
     IOAPIC_Redirection_Table_Entry_type;

   Remap_Offset : constant := 32;

   subtype Remapped_Vector_Type is Dst_Vector_Range range
     Remap_Offset .. Dst_Vector_Range'Last;

   type Vector_Route_Type is record
      Subject : Dst_Subject_Type;
      Vector  : Vector_Range;
   end record;

   type Vector_Routing_Array is array (Remapped_Vector_Type)
     of Vector_Route_Type;

   type vmcs_array is array (subject_number) of SK.CPU.VMCStype;

   type page_table_index_type is array (cpu_number) of subject_number;

   type CPU_Reg_Array_Type is array(cpu_number) of Subject_State_Type;

   type Conc_IDTs_Type is array (subject_number) of IDT_type;

   Bits_In_Word : constant := 64;
   Event_Count  : constant := 256;
   Event_Words  : constant := Event_Count / Bits_In_Word;

   type Bitfield64_Type is mod 2 ** Bits_In_Word;

   type Event_Bit_Type is range 0 .. (Bits_In_Word - 1);

   type Atomic64_Type is record
      Bits : Bitfield64_Type;
   end record;

   type Event_Word_Type is range 0 .. (Event_Words - 1);

   type Event_Array is array (Event_Word_Type) of Atomic64_Type;

   type Global_Event_Array is array (subject_number) of Event_Array;

   type IDTR_type is record
      base: SK.Word64;
      limit: SK.Word16;
   end record;

   type abs_IDTR_array_type is array (subject_number) of IDTR_type;

   type IDTR_array_type is array (cpu_number) of subject_number;

   type conc_stack_type is array (subject_number) of stack_type;

   type Boolean_to_Word_Bit_map_entry_type is record
      Word_index: Event_Word_Type;
      Bit_No: Event_Bit_Type;
   end record;

   type Boolean_to_Word_Bit_map_type is array (SK.Byte) of
      Boolean_to_Word_Bit_map_entry_type;

   type Boolean_to_Word_Bit_map_inverse_subarray_type is array (Event_Bit_Type)
      of Vector_Range;

   type Boolean_to_Word_Bit_map_inverse_type is array (Event_Word_Type) of
      Boolean_to_Word_Bit_map_inverse_subarray_type;



   -- combined state type
   type combined_state_type is record
      -- abstract
      abs_red_tab: Abs_Redirection_Table_type;
      pending_event_array: pending_event_array_type;
      current_subject: current_subject_type;
      Abs_IO_Bitmap: abs_IO_Bitmap_type;
      Abs_IO_Ports: abs_ports_type;
      Abs_IDTs: Abs_IDTs_type;
      Subjects_State: Subject_State_Array_type;
      abs_IDTR_array: abs_IDTR_array_type;
      subject_CPU_mapping: subject_CPU_mapping_type;
      is_active_array: active_array_type;
      abs_stack: abs_stack_type;

      -- concrete
      VMCSs : vmcs_array;
      VMCS_Pointer : page_table_index_type;
      IOAPIC_red_tab: IOAPIC_Redirection_Table_type;
      Vector_Routing : Vector_Routing_Array;
      Descriptors: Subject_State_Array_type;
      CPU_Regs : CPU_Reg_Array_Type;
      Conc_IO_Ports: abs_ports_type;
      Global_Events : Global_Event_Array;
      IDTR_array: IDTR_array_type;
      Conc_IDTS: Conc_IDTs_Type;
      Conc_Stack: conc_stack_type;
      Boolean_to_word_Bit_map: Boolean_to_Word_Bit_map_type;
   end record;


   -- combined state
   comb: combined_state_type;


   function Shift_Left  (Value : Bitfield64_Type; Amount : Natural)
      return Bitfield64_Type;
   pragma Import(Intrinsic, Shift_Left);


   function assumptions(st: combined_state_type) return Boolean is
     (
        (total_no_of_events = Event_Count * no_of_subjects)

        and

         (for all i in Vector_Range =>
             (for all j in Vector_Range =>
                  ((if (i < j) then
                     (((St.Boolean_To_Word_Bit_Map(Sk.Byte(i)).Word_Index) < (St.Boolean_To_Word_Bit_Map(Sk.Byte(j)).Word_Index)) or
                     ((St.Boolean_To_Word_Bit_Map(Sk.Byte(i)).Word_index = St.Boolean_To_Word_Bit_Map(Sk.Byte(j)).Word_index)
                      and (St.Boolean_To_Word_Bit_Map(Sk.Byte(i)).Bit_No < St.Boolean_To_Word_Bit_Map(Sk.Byte(j)).Bit_No))))
     and

        (if(st.Boolean_to_word_Bit_map(Sk.Byte(i)) =
                          st.Boolean_to_word_Bit_map(Sk.Byte(j))) then
                          (i = j)))))
     );


   function inv_holds(st: combined_state_type) return Boolean is
     (
      -- Abstract state wellformedness
        (for all c1 in cpu_number =>
             (for all c2 in cpu_number =>
                  (if(st.current_subject(c1) = st.current_subject(c2)) then
                      c1 = c2)))

      and


      -- Concrete state wellformedness
      -- relationship between



      -- Gluing invariants
      -- Content of Port registers is same
        (for all i in Port_Number_Type =>
             (st.Abs_IO_Ports(i) = st.Conc_IO_Ports(i)))

      and

      -- Pending event arrays are matching
        (for all s in subject_number =>
             (for all e in Vector_Range =>
                  (if(st.pending_event_array(s)(e) = True) then
                     (st.Global_Events(s)(st.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                          Shift_Left(1, Natural(st.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                       Shift_Left(1, Natural(st.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))
                   else
                     (st.Global_Events(s)(st.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                          Shift_Left(1, Natural(st.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) = 0)))

      and

      -- Subject state matching
        ((for all c in cpu_number =>
              (st.Subjects_State(st.current_subject(c)) = st.CPU_Regs(c))) and

           (for all s in subject_number =>
                (if(for all c in cpu_number => (st.current_subject(c) /= s)) then
                    st.Subjects_State(s) = st.Descriptors(s))))

      and

      -- VMCS pointer and current subjects
        (for all c in cpu_number =>
             (st.VMCS_Pointer(c) = st.current_subject(c)))

      and

      -- IOAPIC redirection table with interrupt routing table and abstract redirection table
        (for all irq in irq_number =>
             ((st.abs_red_tab(irq).dest_sub =
                   st.Vector_Routing(Remapped_Vector_Type(st.IOAPIC_red_tab(irq).dest_vector)).Subject) and
                (st.abs_red_tab(irq).dest_vector =
                     st.Vector_Routing(Remapped_Vector_Type(st.IOAPIC_red_tab(irq).dest_vector)).Vector)))

      and

      -- IO Bitmap matching in both abstract and concrete
        (for all s in subject_number =>
             (st.VMCSs(s).IO_BITMAP = st.Abs_IO_Bitmap(s)))

      and

--        -- IDTR of currently running subjects matches with the IDTR on CPU
--          (for all c in cpu_number =>
--               (st.abs_IDTR_array(st.current_subject(c)) =
--                    st.IDTR_array(c)))
--
--        and

      -- IDTR of non-active subjects matches with the IDTR on VMCS
        (for all s in subject_number =>
             (if(for all c in cpu_number =>
                   (st.current_subject(c) /= s)) then
                        ((st.abs_IDTR_array(s).base =
                               st.VMCSs(s).GUEST_BASE_IDTR) and
                             (SK.Word32(st.abs_IDTR_array(s).limit) =
                             st.VMCSs(s).GUEST_LIMIT_IDTR))))

     );


   -- Procedures
   procedure combined_ext_int(irq: irq_number) with
     Pre => (inv_holds(comb) and assumptions(comb)
             and (for some c in cpu_number =>
                      comb.current_subject(c) = comb.abs_red_tab(irq).dest_sub));
--         Post => inv_holds(comb);

   procedure combined_IN(c:cpu_number; port_no: Port_Number_Type) with
     Pre => inv_holds(comb),
     Post => inv_holds(comb);

--     procedure IRET with
--       Pre => inv_holds(comb),
--       Post => inv_holds(comb);

   procedure find_highest_pending_event(s: in subject_number; found: out Boolean;
                                        event: out Vector_Range;
                                        c: in combined_state_type) with
     Import => True,
     Post => ((if(for some v in Vector_Range =>
                    (c.pending_event_array(s)(v) = True)) then
                  (found = True) else
                found = False) and
                  (if(found = True) then
                     (for all v in Vector_Range =>
                        ((c.pending_event_array(s)(v) = True) and (if (v > event) then
                             (c.pending_event_array(s)(v) = False))))));

   procedure Find_Highest_Bit_Set(Bits: in Bitfield64_Type; Found: out Boolean;
                                  Bit_In_Word: out Event_Bit_Type) with
     Import => True,
     Post => ((if(SK.Word64(Bits) /= 0) then (Found = True)
                else (Found = False)) and
                (if (Found = True) then
                     (((Bits and Shift_Left(1,Natural(Bit_In_Word))) =
                          Shift_Left(1, Natural(Bit_In_Word))) and
                          (for all pos in Event_Bit_Type =>
                             (if pos > Bit_In_Word then
                                ((Bits and Shift_Left(1, Natural(pos))) = 0))))));

   procedure experiment
     with Pre => inv_holds(comb);

   function Concrete_map(event: in SK.Byte)
                         return Boolean_to_Word_Bit_map_entry_type
     with Post =>
       ((for all e1 in SK.Byte =>
          (for all e2 in SK.Byte =>
             (if(e1 /= e2) then
              Concrete_map(e1) /= Concrete_map(e2))))
        and
        (for all e in Sk.Byte =>
         ((Concrete_map(e).Word_index = Event_Word_Type(e/Bits_In_Word)) and
          (Concrete_map(e).Bit_No = Event_Bit_Type(e mod Bits_In_Word)))));


end ext_int6;
