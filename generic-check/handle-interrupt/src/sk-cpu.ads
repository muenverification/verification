--
--  Copyright (C) 2013, 2014  Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013, 2014  Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

-- with X86_64;
with Interfaces; use Interfaces;
with Skp; use Skp;
with SK;
with numbers1; use numbers1;

package SK.CPU
is

   type Register_Type is
     (CR0, CR1, CR2, CR3, CR4, EAX, EBX, ECX, EDX, EDI, ESI, EBP);

   type CPU_Registers_Type64 is record
      CR0       : SK.Word64;
      CR1       : SK.Word64;
      CR2       : SK.Word64;
      CR3       : SK.Word64;
      CR4       : SK.Word64;
      RAX       : SK.Word64;
      RBX       : SK.Word64;
      RCX       : SK.Word64;
      RDX       : SK.Word64;
      RDI       : SK.Word64;
      RSI       : SK.Word64;
      RSP       : SK.Word64;
      RBP       : SK.Word64;
      R08       : SK.Word64;
      R09       : SK.Word64;
      R10       : SK.Word64;
      R11       : SK.Word64;
      R12       : SK.Word64;
      R13       : SK.Word64;
      R14       : SK.Word64;
      R15       : SK.Word64;
      RFLAGS    : SK.Word64;
      RIP       : SK.Word64;
   end record;

   type Segment_Selctor_Type is (DS, ES, FS, GS,SS);

   type CPU_Segment_Selector_Type is
      record
         CS     : SK.Word16;
         DS     : SK.Word16;
         ES     : SK.Word16;
         FS     : SK.Word16;
         GS     : SK.Word16;
         SS     : SK.Word16;
      end record;

   ID           : constant Word32_Pos := 21;
   VIP          : constant Word32_Pos := 20;
   VIF          : constant Word32_Pos := 19;
   AC           : constant Word32_Pos := 18;
   VM           : constant Word32_Pos := 17;
   RF           : constant Word32_Pos := 16;
   NT           : constant Word32_Pos := 14;
   IOPL0        : constant Word32_Pos := 12;
   IOPL1        : constant Word32_Pos := 13;
   OVERFLOW     : constant Word32_Pos := 11;
   DF           : constant Word32_Pos := 10;
   INTERRUPT    : constant Word32_Pos := 9;
   TF           : constant Word32_Pos := 8;
   SF           : constant Word32_Pos := 7;
   ZF           : constant Word32_Pos := 6;
   AF           : constant Word32_Pos := 4;
   PF           : constant Word32_Pos := 2;
   CF           : constant Word32_Pos := 0;

   --  TODO: Add other MSRs
   type MSR_Type is record
      IA32_VMX_BASIC : SK.Word64;
      IA32_PAT       : SK.Word64;
   end record;

   type subject_IO_Bitmap_type is array (Port_Number_Type) of Boolean;

   --  VMCS data structure
   type VMCStype is record
      --  Control fields

      PIN_BASED_EXEC_CONTROL       : SK.Word32;
      CPU_BASED_EXEC_CONTROL       : SK.Word32;
      EXCEPTION_BITMAP             : SK.Word32;
      VM_EXIT_CONTROLS             : SK.Word32;
      VM_EXIT_MSR_STORE_COUNT      : SK.Word32;
      VM_ENTRY_CONTROLS            : SK.Word32;
      VM_ENTRY_MSR_LOAD_COUNT      : SK.Word32;
      VM_ENTRY_INTERRUPT_INFO      : SK.Word32;
      VM_ENTRY_EXC_ERROR_CODE      : SK.Word32;
      VM_ENTRY_INSTRUCTION_LEN     : SK.Word32;
      CPU_BASED_EXEC_CONTROL2      : SK.Word32;
      VMX_INST_ERROR               : SK.Word32;
      VMX_EXIT_REASON              : SK.Word32;
      VMX_EXIT_INTR_INFO           : SK.Word32;
      VMX_EXIT_INSTRUCTION_LEN     : SK.Word32;
      CR0_MASK                     : SK.Word64;
      CR4_MASK                     : SK.Word64;
      CR0_READ_SHADOW              : SK.Word64;
      CR4_READ_SHADOW              : SK.Word64;
      VMX_EXIT_QUALIFICATION       : SK.Word64;

      --  Host state fields

      HOST_SEL_ES                  : SK.Word16;
      HOST_SEL_CS                  : SK.Word16;
      HOST_SEL_SS                  : SK.Word16;
      HOST_SEL_DS                  : SK.Word16;
      HOST_SEL_FS                  : SK.Word16;
      HOST_SEL_GS                  : SK.Word16;
      HOST_SEL_TR                  : SK.Word16;
      HOST_IA32_EFER               : SK.Word64;
      HOST_CR0                     : SK.Word64;
      HOST_CR3                     : SK.Word64;
      HOST_CR4                     : SK.Word64;
      HOST_BASE_FS                 : SK.Word64;
      HOST_BASE_GS                 : SK.Word64;
      HOST_BASE_GDTR               : SK.Word64;
      HOST_BASE_IDTR               : SK.Word64;
      HOST_RSP                     : SK.Word64;
      HOST_RIP                     : SK.Word64;

      --  Guest state fields

      GUEST_SEL_ES                 : SK.Word16;
      GUEST_SEL_CS                 : SK.Word16;
      GUEST_SEL_SS                 : SK.Word16;
      GUEST_SEL_DS                 : SK.Word16;
      GUEST_SEL_TR                 : SK.Word16;
      IO_BITMAP                    : subject_IO_Bitmap_type;
      MSR_BITMAP                   : SK.Word64;
      VM_EXIT_MSR_STORE_ADDRESS    : SK.Word64;
      VM_ENTRY_MSR_LOAD_ADDRESS    : SK.Word64;
      EPT_POINTER                  : SK.Word64;
      GUEST_PHYSICAL_ADDRESS       : SK.Word64;
      VMCS_LINK_POINTER            : SK.Word64;
      GUEST_IA32_EFER              : SK.Word64;
      GUEST_LIMIT_ES               : SK.Word32;
      GUEST_LIMIT_CS               : SK.Word32;
      GUEST_LIMIT_SS               : SK.Word32;
      GUEST_LIMIT_DS               : SK.Word32;
      GUEST_LIMIT_TR               : SK.Word32;
      GUEST_LIMIT_GDTR             : SK.Word32;
      GUEST_LIMIT_IDTR             : SK.Word32;
      GUEST_ACCESS_RIGHTS_ES       : SK.Word32;
      GUEST_ACCESS_RIGHTS_CS       : SK.Word32;
      GUEST_ACCESS_RIGHTS_SS       : SK.Word32;
      GUEST_ACCESS_RIGHTS_DS       : SK.Word32;
      GUEST_ACCESS_RIGHTS_FS       : SK.Word32;
      GUEST_ACCESS_RIGHTS_GS       : SK.Word32;
      GUEST_ACCESS_RIGHTS_LDTR     : SK.Word32;
      GUEST_ACCESS_RIGHTS_TR       : SK.Word32;
      GUEST_INTERRUPTIBILITY       : SK.Word32;
      GUEST_SYSENTER_CS            : SK.Word32;
      GUEST_VMX_PREEMPT_TIMER      : SK.Word32;
      GUEST_CR0                    : SK.Word64;
      GUEST_CR3                    : SK.Word64;
      GUEST_CR4                    : SK.Word64;
      GUEST_FS_BASE                : SK.Word64;
      GUEST_GS_BASE                : SK.Word64;
      GUEST_BASE_GDTR              : SK.Word64;
      GUEST_BASE_IDTR              : SK.Word64;
      GUEST_RSP                    : SK.Word64;
      GUEST_RIP                    : SK.Word64;
      GUEST_RFLAGS                 : SK.Word64;
      GUEST_SYSENTER_ESP           : SK.Word64;
      GUEST_SYSENTER_EIP           : Word64;
   end record;

   type cpu is record
      registers: CPU_Registers_Type64;
      segment_registers: CPU_Segment_Selector_Type;
      MSR: MSR_Type;
      current_vmcs: VMCStype;
   end record;

   --  Memory array
   Memory            : array (Unsigned_64) of Unsigned_8;

   --  CPU Registers
   Register64        : CPU_Registers_Type64;

   --  Segment Selectors
   Segment_Selectors : CPU_Segment_Selector_Type;

   --  MSRs
   MSRs             : MSR_Type;

   processors: array (Integer range 0.. CPU_Count) of cpu;

   VMCS: array (Skp.Subject_Id_Type) of VMCStype;
   current_VMCS: VMCStype;

   --  translation array instead of page table, translation functions
   tr: array (Skp.Subject_Id_Type, SK.Word64) of SK.Word64;


   --  Clear Interrupt Flag.
   procedure Cli
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ null),
      Post => ((Register64.RFLAGS and 2**Natural(INTERRUPT)) = 0),
      Inline_Always;

   --  Execute CPUID instruction.
   procedure CPUID
     (EAX : in out SK.Word32;
      EBX :    out SK.Word32;
      ECX : in out SK.Word32;
      EDX :    out SK.Word32)
   with
--      Global  => (Input => X86_64.State),
--      Depends => ((EAX, EBX, ECX, EDX) => (EAX, ECX, X86_64.State)),
      Inline_Always;

   --  Initialize FPU without checking for pending unmasked FP exceptions.
   procedure Fninit
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ null),
      Inline_Always;

   --  Halt the CPU.
   procedure Hlt
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ null),
      Inline_Always;

   --  Set Interrupt Flag.
   procedure Sti
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ null),
      Inline_Always;

   --  Load Interrupt Descriptor Table (IDT) register.
   procedure Lidt (Address : SK.Word64)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ Address),
      Inline_Always;

   --  Load Task Register (TR).
   procedure Ltr (Address : SK.Word16)
   with
--     Global  => (In_Out => X86_64.State),
--     Depends => (X86_64.State =>+ Address),
     Inline_Always;

   --  Spin Loop Hint.
   procedure Pause
   with
--      Global => null,
      Inline_Always;

   --  Panic.
   procedure Panic
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ null),
      Inline_Always,
      No_Return;

   --  RDTSC (Read Time-Stamp Counter). The EDX register is loaded with the
   --  high-order 32 bits of the TSC MSR and the EAX register is loaded with
   --  the low-order 32 bits.
   procedure RDTSC
     (EAX : out SK.Word32;
      EDX : out SK.Word32)
   with
--      Global => (Input => X86_64.State),
      Inline_Always;

   --  RDTSC (Read Time-Stamp Counter).
   function RDTSC64 return SK.Word64;
   --  with
      --  Global => (Input => X86_64.State),
      --  Inline_Always;

   --  Stop CPU.
   procedure Stop
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ null),
      Inline_Always,
      No_Return;

   --  Return current value of CR0 register.
   function Get_CR0 return SK.Word64;
   --  with
      --  Global  => (Input => X86_64.State),
      --  Inline_Always;

   --  Return current value of CR2 register.
   function Get_CR2 return SK.Word64;
   --  with
      --  Global  => (Input => X86_64.State),
      --  Inline_Always;

   --  Return current value of CR3 register.
   function Get_CR3 return SK.Word64;
   --  with
      --  Global  => (Input => X86_64.State),
      --  Inline_Always;

   --  Return current value of CR4 register.
   function Get_CR4 return SK.Word64;
   --  with
      --  Global  => (Input => X86_64.State),
      --  Inline_Always;

      --  Set value of CR2.
   procedure Set_CR2 (Value : SK.Word64)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ Value),
      Inline_Always;

   --  Set value of CR4.
   procedure Set_CR4 (Value : SK.Word64)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ Value),
      Inline_Always;

   --  Return current value of given model specific register.
   function Get_MSR64 (Register : SK.Word32) return SK.Word64;
   --  with
      --  Global  => (Input => X86_64.State),
      --  Inline_Always;

   --  Return value of given MSR as low/high doublewords.
   procedure Get_MSR
     (Register :     SK.Word32;
      Low      : out SK.Word32;
      High     : out SK.Word32)
   with
--      Global  => (Input => X86_64.State),
--      Depends => ((Low, High) => (X86_64.State, Register)),
      Inline_Always;

   --  Write specified quadword to given MSR.
   procedure Write_MSR64
     (Register : SK.Word32;
      Value    : SK.Word64)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ (Register, Value)),
      Inline_Always;

   --  Write specified low/high doublewords to given MSR.
   procedure Write_MSR
     (Register : SK.Word32;
      Low      : SK.Word32;
      High     : SK.Word32)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ (Register, Low, High)),
      Inline_Always;

   --  Return current RFLAGS.
   function Get_RFLAGS return SK.Word64;
   --  with
      --  Global  => (Input => X86_64.State),
      --  Inline_Always;

   --  Set CPU RSP and RBP registers to given address.
   procedure Set_Stack (Address : SK.Word64)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ Address),
      Inline_Always;

   --  Enter VMX operation.
   procedure VMXON
     (Region  :     SK.Word64;
      Success : out Boolean)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => ((X86_64.State, Success) => (X86_64.State, Region)),
      Inline_Always;

   procedure VMCLEAR
     (Region  :     SK.Word64;
      Success : out Boolean)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => ((X86_64.State, Success) => (X86_64.State, Region)),
      Inline_Always;

   procedure VMPTRLD
     (Region  :     SK.Word64;
      Success : out Boolean)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => ((X86_64.State, Success) => (X86_64.State, Region)),
      Inline_Always;

   procedure VMREAD
     (Field   :     SK.Word64;
      Value   : out SK.Word64;
      Success : out Boolean)
   with
--      Global  => (Input => X86_64.State),
--      Depends => ((Value, Success) => (X86_64.State, Field)),
      Inline_Always;

   procedure VMWRITE
     (Field   :     SK.Word64;
      Value   :     SK.Word64;
      Success : out Boolean)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => ((X86_64.State, Success) => (X86_64.State, Field, Value)),
      Inline_Always;

   --  Restore Processor Extended States from given XSAVE area.
   procedure XRSTOR (Source : SK.XSAVE_Area_Type)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ Source),
      Inline_Always;

   --  Save Processor Extended States to given XSAVE area.
   procedure XSAVE (Target : out SK.XSAVE_Area_Type)
   with
--      Global  => (Input => X86_64.State),
--      Depends => (Target => X86_64.State),
      Inline_Always;

   --  Set specified Extended Control Register (XCR) to given value.
   procedure XSETBV
     (Register : SK.Word32;
      Value    : SK.Word64)
   with
--      Global  => (In_Out => X86_64.State),
--      Depends => (X86_64.State =>+ (Register, Value)),
     Inline_Always;

   -----------------------------------------------------------------------

   -- ----------------------------OUR CODE--------------------------------

   -----------------------------------------------------------------------
   --  Adds the destination operand and the source operand
   --  and then stores the result in the destination operand.
   procedure ADD (Value: Word32; Register: Register_Type)
   with Contract_Cases =>
       ((Register = CR0) =>(Word32(Register64.CR0 and (2**32-1)) =
                           (Word32(Register64.CR0'Old and (2**32-1)) + Value)),
        (Register = CR1) =>(Word32(Register64.CR1 and (2**32-1)) =
                            (Word32(Register64.CR1'Old and (2**32-1))+ Value)),
        (Register = CR2) =>(Word32(Register64.CR2 and (2**32-1)) =
                            (Word32(Register64.CR2'Old and (2**32-1))+Value)),
        (Register = CR3) =>(Word32(Register64.CR3 and (2**32-1)) =
                            (Word32(Register64.CR3'Old and (2**32-1))+Value)),
        (Register = CR4) =>(Word32(Register64.CR4 and (2**32-1)) =
                            (Word32(Register64.CR4'Old and (2**32-1))+Value)),
        (Register = EAX) =>(Word32(Register64.RAX and (2**32-1)) =
                            (Word32(Register64.RAX'Old and (2**32-1))+Value)),
        (Register = EBX) =>(Word32(Register64.RBX and (2**32-1)) =
                            (Word32(Register64.RBX'Old and (2**32-1))+Value)),
        (Register = ECX) =>(Word32(Register64.RCX and (2**32-1)) =
                            (Word32(Register64.RCX'Old and (2**32-1))+Value)),
        (Register = EDX) =>(Word32(Register64.RDX and (2**32-1)) =
                            (Word32(Register64.RDX'Old and (2**32-1))+Value)),
        (Register = EDI) =>(Word32(Register64.RDI and (2**32-1)) =
                            (Word32(Register64.RDI'Old and (2**32-1))+Value)),
        (Register = ESI) =>(Word32(Register64.RSI and (2**32-1)) =
                            (Word32(Register64.RSI'Old and (2**32-1))+Value)),
        (Register = EBP) =>(Word32(Register64.RBP and (2**32-1)) =
                            (Word32(Register64.RBP'Old and (2**32-1))+Value)));

   --  Selects the bit in a bit string (specified with the Second operand,
   --  called the bit base) at the bit-position designated by the bit offset
   --  operand (First operand), stores the value of the bit in the CF flag,
   --  and sets the selected bit in the bit string to 1.
   procedure BTSL (Position : Word32_Pos; Register   : Register_Type)
   with Contract_Cases =>
       ((Register = CR0) =>(Word32(Register64.CR0 and (2**32-1)) =
                             (Word32(Register64.CR0'Old and (2**32-1)) or
                                            Word32((2*Natural(Position))))),
         (Register = CR1) =>(Word32(Register64.CR1 and (2**32-1)) =
                             (Word32(Register64.CR1'Old and (2**32-1))or
                                  Word32((2*Natural(Position))))),
        (Register = CR2) =>(Word32(Register64.CR2 and (2**32-1)) =
                            (Word32(Register64.CR2'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = CR3) =>(Word32(Register64.CR3 and (2**32-1)) =
                            (Word32(Register64.CR3'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = CR4) =>(Word32(Register64.CR4 and (2**32-1)) =
                            (Word32(Register64.CR4'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = EAX) =>(Word32(Register64.RAX and (2**32-1)) =
                            (Word32(Register64.RAX'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = EBX) =>(Word32(Register64.RBX and (2**32-1)) =
                            (Word32(Register64.RBX'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = ECX) =>(Word32(Register64.RCX and (2**32-1)) =
                            (Word32(Register64.RCX'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = EDX) =>(Word32(Register64.RDX and (2**32-1)) =
                            (Word32(Register64.RDX'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = EDI) =>(Word32(Register64.RDI and (2**32-1)) =
                            (Word32(Register64.RDI'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = ESI) =>(Word32(Register64.RSI and (2**32-1)) =
                            (Word32(Register64.RSI'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))),
        (Register = EBP) =>(Word32(Register64.RBP and (2**32-1)) =
                            (Word32(Register64.RBP'Old and (2**32-1))or
                                 Word32((2*Natural(Position))))));

   --  Clears the DF flag in the RFLAGS register.
   --  When the DF flag is set to 0, string operations increment the index
   --  registers (RSI and/or RDI). Operation is the same in all modes.
   procedure CLD
     with Post => ((Register64.RFLAGS and 2**Natural(DF)) = 0);

   --  Implements Init function from muen init assembly code.
   procedure Clear (Address : Unsigned_64; Page_Count : Word32);

   --  Compares the first source operand with the second source operand
   --  and sets the status flags in the RFLAGS register according
   --  to the results.
   procedure CMP( Value : Word32; Register : Register_Type)
     with Contract_Cases =>
       ((Register = CR0) =>
          (if Word32(Register64.CR0 and (2**32-1)) < Value
            then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF))and
             ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
           else (if Word32(Register64.CR0 and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                          ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                  else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                    ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
      (Register = CR1) =>
          (if Word32(Register64.CR1 and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                  ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
           else (if Word32(Register64.CR1 and (2**32-1)) = Value
                 then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                        ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                    ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = CR2) =>
          (if Word32(Register64.CR2 and (2**32-1)) < Value
            then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.CR2 and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                    ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = CR3) =>
          (if Word32(Register64.CR3 and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.CR3 and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = CR4) =>
          (if Word32(Register64.CR4 and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.CR4 and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = EAX) =>
          (if Word32(Register64.RAX and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.RAX and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = EBX) =>
          (if Word32(Register64.RBX and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.RBX and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                    ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = ECX) =>
          (if Word32(Register64.RCX and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.RCX and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = EDX) =>
          (if Word32(Register64.RDX and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.RDX and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = EDI) =>
          (if Word32(Register64.RDI and (2**32-1)) < Value
          then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.RDI and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                  ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = ESI) =>
          (if Word32(Register64.RSI and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.RSI and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))),
        (Register = EBP) =>
          (if Word32(Register64.RBP and (2**32-1)) < Value
           then (((Register64.RFLAGS and 2**Natural(CF)) = 2**Natural(CF)) and
                 ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
             else (if Word32(Register64.RBP and (2**32-1)) = Value
                   then (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                   ((Register64.RFLAGS and 2**Natural(ZF)) = 0))
                 else (((Register64.RFLAGS and 2**Natural(CF)) = 0) and
                  ((Register64.RFLAGS and 2**Natural(ZF))=2**Natural(ZF))))));

   --  Implements Deactivate operation from muen init assembly code.
   procedure Deactivate;

   --  Gets content of Memory location; Remember that our memory
   --  is array of bytes So to get 32 bit data from an address we need to
   --  read four contiguous bytes.Here address parameter is first byte address.
   function Get_Memory32(Address : Unsigned_64) return Word32;

   --  Gets content of Memory location;
   --  Remember that our memory is array of bytes
   function Get_Memory8(Address : Unsigned_64) return Unsigned_8
     with Post => (Memory(Address) = Get_Memory8'Result);

   --  Reads 16 bit data from 64 bit register
   function Get_Register16(Register : Register_Type) return Word16;

   --  eads 32 bit data from 64 bit register
   function Get_Register32(Register : Register_Type) return Word32
     with Contract_Cases =>
       ((Register = CR0) =>(Word32(Register64.CR0 and (2**32-1)) =
                              Get_Register32'Result),
        (Register = CR1) =>(Word32(Register64.CR1 and (2**32-1)) =
                              Get_Register32'Result),
        (Register = CR2) =>(Word32(Register64.CR2 and (2**32-1)) =
                              Get_Register32'Result),
        (Register = CR3) =>(Word32(Register64.CR3 and (2**32-1)) =
                              Get_Register32'Result),
        (Register = CR4) =>(Word32(Register64.CR4 and (2**32-1)) =
                              Get_Register32'Result),
        (Register = EAX) =>(Word32(Register64.RAX and (2**32-1)) =
                              Get_Register32'Result),
        (Register = EBX) =>(Word32(Register64.RBX and (2**32-1)) =
                              Get_Register32'Result),
        (Register = ECX) =>(Word32(Register64.RCX and (2**32-1)) =
                              Get_Register32'Result),
        (Register = EDX) =>(Word32(Register64.RDX and (2**32-1)) =
                              Get_Register32'Result),
        (Register = EDI) =>(Word32(Register64.RDI and (2**32-1)) =
                              Get_Register32'Result),
        (Register = ESI) =>(Word32(Register64.RSI and (2**32-1)) =
                              Get_Register32'Result),
        (Register = EBP) =>(Word32(Register64.RBP and (2**32-1)) =
                              Get_Register32'Result));

   --  Reads lowest 8 bit data from 64 bit register. Eg : Al,Bl
   function Get_Register8(Register : Register_Type) return Byte;

   --  Copies the source operand (first operand) to the destination operand
   --  Second Operand).
   procedure MOV (Value: Word32; Register: Register_Type)
     with Contract_Cases =>
         ((Register = CR0) =>((Register64.CR0 and (2**32-1)) =  Word64(Value)),
         (Register = CR1) =>((Register64.CR1 and (2**32-1)) =  Word64(Value)),
         (Register = CR2) =>((Register64.CR2 and (2**32-1)) =  Word64(Value)),
         (Register = CR3) =>((Register64.CR3 and (2**32-1)) =  Word64(Value)),
         (Register = CR4) =>((Register64.CR4 and (2**32-1)) =  Word64(Value)),
         (Register = EAX) =>((Register64.RAX and (2**32-1)) =  Word64(Value)),
         (Register = EBX) =>((Register64.RBX and (2**32-1)) =  Word64(Value)),
         (Register = ECX) =>((Register64.RCX and (2**32-1)) =  Word64(Value)),
         (Register = EDX) =>((Register64.RDX and (2**32-1)) =  Word64(Value)),
         (Register = EDI) =>((Register64.RDI and (2**32-1)) =  Word64(Value)),
         (Register = ESI) =>((Register64.RSI and (2**32-1)) =  Word64(Value)),
         (Register = EBP) =>((Register64.RBP and (2**32-1)) =  Word64(Value)));

   --  Contract for the following procedure needs to be modified
   procedure MOV (Memory_Address: Unsigned_64; Register: Register_Type);

   procedure MOV(Value : Word16; Segment_Selector : Segment_Selctor_Type)
   with Contract_Cases =>
                    ((Segment_Selector = DS) =>(Segment_Selectors.DS = Value),
                    (Segment_Selector = ES) =>(Segment_Selectors.ES = Value),
                    (Segment_Selector = FS) =>(Segment_Selectors.FS = Value),
                    (Segment_Selector = GS) =>(Segment_Selectors.GS = Value),
                    (Segment_Selector = SS) =>(Segment_Selectors.SS = Value));

   procedure MOV (Value: Word32; Memory_Address: Unsigned_64); -- overloaded

   procedure MOVL (Value: Word32; Register: Register_Type)
   with Contract_Cases =>
          ((Register = CR0) =>(Word32(Register64.CR0 and (2**32-1)) =  Value),
          (Register = CR1) =>(Word32(Register64.CR1 and (2**32-1)) =  Value),
          (Register = CR2) =>(Word32(Register64.CR2 and (2**32-1)) =  Value),
          (Register = CR3) =>(Word32(Register64.CR3 and (2**32-1)) =  Value),
          (Register = CR4) =>(Word32(Register64.CR4 and (2**32-1)) =  Value),
          (Register = EAX) =>(Word32(Register64.RAX and (2**32-1)) =  Value),
          (Register = EBX) =>(Word32(Register64.RBX and (2**32-1)) =  Value),
          (Register = ECX) =>(Word32(Register64.RCX and (2**32-1)) =  Value),
          (Register = EDX) =>(Word32(Register64.RDX and (2**32-1)) =  Value),
          (Register = EDI) =>(Word32(Register64.RDI and (2**32-1)) =  Value),
          (Register = ESI) =>(Word32(Register64.RSI and (2**32-1)) =  Value),
          (Register = EBP) =>(Word32(Register64.RBP and (2**32-1)) =  Value));

   procedure MOVL (Value: Word32; Memory_Address : Unsigned_64);

   --  Performs an unsigned multiplication of the destination operand
   --  and the Source operand and stores the result in the destination operand.
   --  The destination operand is an implied operand located in
   --  register AL, AX or EAX (depending on the size of the operand).
   procedure MUL (Value: Word32)
     with Post => (Word32(Register64.RAX and (2**32-1))=
                   (Word32(Register64.RAX'Old and (2**32-1)) * Value));

   --  Just to implement mull assembly instruction from the Init file
   --  Functions same as MUL procedure.
   procedure MULL (Value: Word32)
   with Post => (Word32(Register64.RAX and (2**32-1))=
                 (Word32(Register64.RAX'Old and (2**32-1)) * Value));

   --  Repeats a string instruction the number of times specified in
   --  the count register or until the indicated condition of the ZF flag
   --  is no longer met. The Instruction operand used to speicfy type of REP
   --  operation. Eg: MOVSB for strings
   procedure REP (Instruction : String);

   --  Reads the contents of a 64-bit model specific register (MSR)
   --  specified in the ECX register into registers EDX:EAX.(On processors
   --  that support the Intel 64 architecture, the high-order 32 bits of
   --  RCX are ignored.) The EDX register is loaded with the high-order
   --  32 bits of the MSR and the EAX register is loaded with the
   --  low-order 32 bits.
   procedure RDMSR;
   --  with;

   --  Subtracts the source operand(value) from the destination
   --  operand(Register)and stores the result in the destination operand.
   procedure SUB (Value: Word32; Register: Register_Type)
   with Contract_Cases =>
       ((Register = CR0) =>(Word32(Register64.CR0 and (2**32-1)) =
                            (Word32(Register64.CR0'Old and (2**32-1))-Value)),
        (Register = CR1) =>(Word32(Register64.CR1 and (2**32-1)) =
                            (Word32(Register64.CR1'Old and (2**32-1))-Value)),
        (Register = CR2) =>(Word32(Register64.CR2 and (2**32-1)) =
                            (Word32(Register64.CR2'Old and (2**32-1))-Value)),
        (Register = CR3) =>(Word32(Register64.CR3 and (2**32-1)) =
                            (Word32(Register64.CR3'Old and (2**32-1))-Value)),
        (Register = CR4) =>(Word32(Register64.CR4 and (2**32-1)) =
                            (Word32(Register64.CR4'Old and (2**32-1))-Value)),
        (Register = EAX) =>(Word32(Register64.RAX and (2**32-1)) =
                            (Word32(Register64.RAX'Old and (2**32-1))-Value)),
        (Register = EBX) =>(Word32(Register64.RBX and (2**32-1)) =
                            (Word32(Register64.RBX'Old and (2**32-1))-Value)),
        (Register = ECX) =>(Word32(Register64.RCX and (2**32-1)) =
                            (Word32(Register64.RCX'Old and (2**32-1))-Value)),
        (Register = EDX) =>(Word32(Register64.RDX and (2**32-1)) =
                            (Word32(Register64.RDX'Old and (2**32-1))-Value)),
        (Register = EDI) =>(Word32(Register64.RDI and (2**32-1)) =
                            (Word32(Register64.RDI'Old and (2**32-1))-Value)),
        (Register = ESI) =>(Word32(Register64.RSI and (2**32-1)) =
                            (Word32(Register64.RSI'Old and (2**32-1))-Value)),
        (Register = EBP) =>(Word32(Register64.RBP and (2**32-1)) =
                            (Word32(Register64.RBP'Old and (2**32-1))-Value)));

   --  Writes 8 bit value to memory; Address is given as parameter.
   procedure Write_Memory8(Address : Unsigned_64; Value : Unsigned_8)
     with Post => (Memory(Address) = Value);

   --  Writes 32 bit value to memory; First byte address is given as parameter.
   --  write into four contiguous Bytes.
   procedure Write_Memory32(Address : Unsigned_64; Value : Unsigned_32);

   --  Writes 32 bit data to 64 bit register
   procedure Write_Register32(Register : Register_Type; Value : Word32)
   with Contract_Cases =>
       ((Register = CR0) =>(Word32(Register64.CR0 and (2**32-1)) =  Value),
       (Register = CR1) =>(Word32(Register64.CR1 and (2**32-1)) =  Value),
       (Register = CR2) =>(Word32(Register64.CR2 and (2**32-1)) =  Value),
       (Register = CR3) =>(Word32(Register64.CR3 and (2**32-1)) =  Value),
       (Register = CR4) =>(Word32(Register64.CR4 and (2**32-1)) =  Value),
       (Register = EAX) =>(Word32(Register64.RAX and (2**32-1)) =  Value),
       (Register = EBX) =>(Word32(Register64.RBX and (2**32-1)) =  Value),
       (Register = ECX) =>(Word32(Register64.RCX and (2**32-1)) =  Value),
       (Register = EDX) =>(Word32(Register64.RDX and (2**32-1)) =  Value),
       (Register = EDI) =>(Word32(Register64.RDI and (2**32-1)) =  Value),
       (Register = ESI) =>(Word32(Register64.RSI and (2**32-1)) =  Value),
       (Register = EBP) =>(Word32(Register64.RBP and (2**32-1)) =  Value));

   --  Writes the contents of registers EDX:EAX into the 64-bit model specific
   --  register (MSR) specified in the ECX register. The input value
   --  loaded into the ECX register is the address of the MSR to be written
   --  to. The contents of the EDX register are copied to high-order 32 bits
   --  of the selected MSR and the contents of the EAX register are copied
   --  to low-order 32 bits of the MSR.
   procedure WRMSR;

   --  Exchanges the first operand (destination operand)
   --  with the second operand (source operand), then loads the sum of
   --  the two values into the destination operand.
   procedure XADD(Register : Register_Type; Value : Integer);

end SK.CPU;
