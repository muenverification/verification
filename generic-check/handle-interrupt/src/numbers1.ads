package numbers1
is

   max_no_of_channels: constant Positive := 50;

   max_no_of_subjects: constant Positive := 50;

   max_no_of_cpus: constant Positive := 20;

   max_no_of_major_frames: constant Positive := 10;

   max_no_of_minor_frames: constant Positive := 15;

   no_of_channels : Constant Natural range 0..max_no_of_channels
     with Import => True;

   no_of_subjects: Constant Positive range 1..max_no_of_subjects
     with Import => True;

   no_of_cpus: Constant Positive range 1..max_no_of_cpus
     with Import => True;

   no_of_major_frames: Constant Positive range 1..max_no_of_major_frames
     with Import => True;

   no_of_minor_frames: Constant Positive range 1..max_no_of_minor_frames
     with Import => True;

   no_of_groups: Constant Positive
     with Import => True;

   no_of_barriers: Constant Positive
     with Import => True;

   total_no_of_events: Constant Natural
     with Import => True; -- this includes total for all subjects

   type channel_number is new Natural range 0..no_of_channels;

   subtype subject_number is Natural range 0..no_of_subjects-1;

   subtype Dst_Subject_Type is Natural range 0 .. no_of_subjects;

   type cpu_number is new Natural range 0..no_of_cpus-1;

   type Major_Frame_Range is new Integer range 0..no_of_major_frames-1;

   type Minor_Frame_Range is new Integer range 1..no_of_minor_frames;

   type Scheduling_Group_Range is new Integer range 0..no_of_groups;

   type Barrier_Index_Range is new Integer range 0..no_of_barriers;

   subtype Barrier_Size_Type is
     Positive range 1 .. Integer (cpu_number'Last) + 1;

   subtype Barrier_Range is
     Barrier_Index_Range range 1 .. Barrier_Index_Range'Last;

   No_Barrier : constant Barrier_Index_Range := Barrier_Index_Range'First;

   type Vector_Range is range 0 .. 255;

   Invalid_Vector : constant := 256;

   type Dst_Vector_Range is range 0 .. Invalid_Vector;

   max_no_of_irqs: Constant Natural := 16;

   no_of_irqs: Constant Natural
     with Import => True;

   type irq_number is new Integer range 0..no_of_irqs;

   type Port_Number_Type is new Natural range 0..2**16;

   type Event_Pos_Type is new Natural range 0..total_no_of_events-1;

end numbers1;
