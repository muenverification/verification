with ext_int; use ext_int;
with numbers1; use numbers1;

package experiments
with SPARK_Mode is

   exp_comb: combined_state_type;

   type Word64 is mod 2**64;

   type index_type is range 0..3;

   type PEA_type is array (index_type) of Word64;

   type word_pos_type is range 0..63;

   PEA: PEA_type;

   procedure exp1(s:subject_number; e:Vector_Range)
     with
       Pre => (for all s in subject_number =>
                 (for all e in Vector_Range =>
                    (if(exp_comb.pending_event_array(s)(e) = True) then
                         (exp_comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
                           2**Natural(e mod 64)
                         else
                       (exp_comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) = 0))) and
     (for all s in subject_number =>
        (for all e in Vector_Range =>
             (if(exp_comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
                  2**Natural(e mod 64) then
                (exp_comb.pending_event_array(s)(e) = True)
                  else
                (exp_comb.pending_event_array(s)(e) = False)))),
     Post => (for all s in subject_number =>
                 (for all e in Vector_Range =>
                    (if(exp_comb.pending_event_array(s)(e) = True) then
                         (exp_comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
                           2**Natural(e mod 64)
                         else
                       (exp_comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) = 0))) and
     (for all s in subject_number =>
        (for all e in Vector_Range =>
             (if(exp_comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
                  2**Natural(e mod 64) then
                (exp_comb.pending_event_array(s)(e) = True)
                  else
                (exp_comb.pending_event_array(s)(e) = False))));

   procedure exp2;

   procedure exp3;

   procedure Find_Highest_Bit_Set(Bits: in Word64; Found: out Boolean;
                                  Bit_In_Word: out word_pos_type) with
     Import => True,
     Post => ((if(Bits /= 0) then (Found = True)
                else (Found = False)) and
                (if (Found = True) then
                     (((Bits and 2**Natural(Bit_In_Word)) =
                          2**Natural(Bit_In_Word)) and
                          (for all pos in word_pos_type =>
                             (if pos > Bit_In_Word then
                                ((Bits and 2**Natural(pos)) = 0))))));

end experiments;
