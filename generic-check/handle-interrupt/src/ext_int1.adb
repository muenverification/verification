--  with Skp.Interrupts;
with SK.Constants;
package body ext_int3
with SPARK_Mode is

   procedure combined_ext_int(irq: irq_number)
   is
      abs_dest: Abs_Redirection_Table_Entry_Type;
      is_current_sub: Boolean := False;
      conc_dest: IOAPIC_Redirection_Table_Entry_type;
      cpu: cpu_number;
      Vector: Vector_Range;
      Vect_Nr : Remapped_Vector_Type;
      Route   : Vector_Route_Type;
      Pos : Event_Pos_Type;
      Subject: subject_number;
      Event: SK.Byte;
      Event_Word: Bitfield64_Type;
      Event_Word_temp: Bitfield64_Type;
      Subject_Id: subject_number;
      Current_Subject: subject_number;
      RFLAGS        : SK.Word64;
      Intr_State    : SK.Word64;
      Event_Present : Boolean := False;
      Event_Pending : Boolean;
      Bits        : Bitfield64_Type;
      Bit_In_Word : Event_Bit_Type;
      Cur_Flags : SK.Word64;
      Interrupt_Window_Exit_Flag : constant SK.Word64_Pos := 2;
      tmp_vector: Vector_Range;
      found_word: Event_Word_Type := Event_Word_Type'Last;
      subject_tmp_event_array: Event_Array;
      found: Boolean;
      highest_event: Vector_Range;
      tmp_pea: pending_event_array_type;
      tmp_global_events: Global_Event_Array;
      tmp_event_word: Bitfield64_Type;
   begin
      pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(comb.pending_event_array(s)(e) = True) then
                              (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                                 Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                              Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))
                          else
                            (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                                 Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) = 0)));

      pragma Assert(for all i in SK.Byte =>
                      (for all j in SK.Byte =>
                         (if(comb.Boolean_to_word_Bit_map(i) =
                              comb.Boolean_to_word_Bit_map(j)) then
                               i = j)));

      tmp_pea := comb.pending_event_array;


      -- abstract
      abs_dest := comb.abs_red_tab(irq);

      -- checking whether the destination subject is running
      pragma Assume(comb.is_active_array(abs_dest.dest_sub) = True); -- currently assuming the dest sub is running
      is_current_sub := comb.is_active_array(abs_dest.dest_sub);

      pragma Assert(is_current_sub);
--        if(is_current_sub) then
         pragma Assume((comb.Subjects_State(abs_dest.dest_sub).RFLAGS and --Assumption not ready for interrupts
                         16#0000000000000200#) = 0);
         pragma Assert((comb.Subjects_State(abs_dest.dest_sub).RFLAGS and --Assumption not ready for interrupts
                         16#0000000000000200#) = 0);
--           if((comb.Subjects_State(abs_dest.dest_sub).RFLAGS and --IF set means ready for interrupts
--                 16#0000000000000200#) = 0)                      --IF clear means block interrupts
--           then
            comb.pending_event_array(abs_dest.dest_sub)(abs_dest.dest_vector) := True;
            tmp_pea(abs_dest.dest_sub)(abs_dest.dest_vector) := True;
            pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(s /= abs_dest.dest_sub or e /= abs_dest.dest_vector) then
                              (if(tmp_pea(s)(e) = True) then
                                   (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                                      Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                                   Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))
                               else
                                 (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits
                                  and Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) = 0)
                         )));
--           else
--              -- setting the bit for the event
--              comb.pending_event_array(abs_dest.dest_sub)(abs_dest.dest_vector) := True;
--              tmp_pea(abs_dest.dest_sub)(abs_dest.dest_vector) := True;
--
--              -- finding the highest pending event
--              find_highest_pending_event(abs_dest.dest_sub,
--                                         found, highest_event,
--                                         comb);
--
--              -- clearing the highest pending event
--              if(found) then
--                 comb.pending_event_array(abs_dest.dest_sub)(highest_event) := False;
--                 -- clearing the IF flag
--                 comb.Subjects_State(abs_dest.dest_sub).RFLAGS :=
--                   comb.Subjects_State(abs_dest.dest_sub).RFLAGS -
--                   16#0000000000000200#;
--
--                 -- updating the stack
--                 comb.abs_stack(abs_dest.dest_sub).stack
--                   (comb.abs_stack(abs_dest.dest_sub).top):=
--                     comb.Subjects_State(abs_dest.dest_sub).RSP;
--                 comb.abs_stack(abs_dest.dest_sub).top :=
--                   comb.abs_stack(abs_dest.dest_sub).top + 1;
--
--                 comb.abs_stack(abs_dest.dest_sub).stack
--                   (comb.abs_stack(abs_dest.dest_sub).top):=
--                     comb.Subjects_State(abs_dest.dest_sub).RFLAGS;
--                 comb.abs_stack(abs_dest.dest_sub).top :=
--                   comb.abs_stack(abs_dest.dest_sub).top + 1;
--
--                 comb.abs_stack(abs_dest.dest_sub).stack
--                   (comb.abs_stack(abs_dest.dest_sub).top):=
--                     comb.Subjects_State(abs_dest.dest_sub).RIP;
--                 comb.abs_stack(abs_dest.dest_sub).top :=
--                   comb.abs_stack(abs_dest.dest_sub).top + 1;
--
--                 comb.Subjects_State(abs_dest.dest_sub).RIP :=
--                   comb.Abs_IDTs(abs_dest.dest_sub)(highest_event);
--
--              end if;
--           end if;
--        else
--           comb.pending_event_array(abs_dest.dest_sub)(abs_dest.dest_vector) := True;
--           tmp_pea(abs_dest.dest_sub)(abs_dest.dest_vector) := True;
--        end if;


--        --------------
--        -- concrete --
--        --------------
--

      pragma Assert(tmp_pea = comb.pending_event_array);

      -- IOAPIC/LAPIC modelling
      conc_dest := comb.IOAPIC_red_tab(irq);
      cpu := conc_dest.dest_CPU;
      Vector := conc_dest.dest_vector;


      -- VM exit modelling - saving state and etc.

      -- processor state is saved
      --    saving control registers
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0 := comb.CPU_Regs(cpu).CR0;
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR3 := comb.CPU_Regs(cpu).CR3;
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR4 := comb.CPU_Regs(cpu).CR4;


      --    saving debug registers


      --    saving MSRs


      --    saving segment registers and descriptor table registers
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_CS :=
        SK.Word16(comb.CPU_Regs(cpu).CS);


--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_DS := ;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_ES := ;
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_SS :=
        SK.Word16(comb.CPU_Regs(cpu).SS);


      --    saving RIP, RSP and RFLAGS --assumptions involved while saving
      --                                         RIP and RFLAGS, check manual
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP := comb.CPU_Regs(cpu).RIP;

      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP := comb.CPU_Regs(cpu).RSP;

      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RFLAGS := comb.CPU_Regs(cpu).RFLAGS;

      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_IA32_EFER :=
        comb.CPU_Regs(cpu).IA32_EFER;



      -- MSRs are saved to VM-exit MSR-store area


      -- processor state is loaded from host state area
      --     loading control registers
      comb.CPU_Regs(cpu).CR0 := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_CR0;

      comb.CPU_Regs(cpu).CR3 := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_CR3;

      comb.CPU_Regs(cpu).CR4 := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_CR4;



      --     loading host segment and descriptor-table registers
      comb.CPU_Regs(cpu).CS :=
        SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_CS);
--        comb.CPU_Regs(cpu).DS :=
--          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_DS);
--        comb.CPU_Regs(cpu).ES :=
--          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_ES);
      comb.CPU_Regs(cpu).SS :=
        SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_SS);
--        comb.CPU_Regs(cpu).FS :=
--          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_FS);
--        comb.CPU_Regs(cpu).GS :=
--          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_GS);
--                                                             _
--        comb.IDTR_array(cpu).base :=                          | look at it
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_BASE_IDTR; _| later
--
--        comb.IDTR_array(cpu).limit :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_;
--         :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_BASE_GDTR;


      --     loading RIP, RSP and RFLAGS
      comb.CPU_Regs(cpu).RIP := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_RIP;
      comb.CPU_Regs(cpu).RSP := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_RSP;
      comb.CPU_Regs(cpu).RFLAGS := 2;



      -- MSRs are loaded from VM-exit MSR-load area



      -- actual kernel code
      -- saving state in descriptors
      comb.Descriptors(comb.VMCS_Pointer(cpu)).Exit_Reason :=
        Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_REASON);
      comb.Descriptors(comb.VMCS_Pointer(cpu)).Exit_Qualification :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_QUALIFICATION;
      comb.Descriptors(comb.VMCS_Pointer(cpu)).Interrupt_Info :=
        Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_INTR_INFO);
      comb.Descriptors(comb.VMCS_Pointer(cpu)).Instruction_Len :=
        Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_INSTRUCTION_LEN);

      comb.Descriptors(comb.VMCS_Pointer(cpu)).Guest_Phys_Addr :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_PHYSICAL_ADDRESS;
      comb.Descriptors(comb.VMCS_Pointer(cpu)).RIP :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP;
      comb.Descriptors(comb.VMCS_Pointer(cpu)).CS :=
        Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_CS);
      comb.Descriptors(comb.VMCS_Pointer(cpu)).RSP :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP;
      comb.Descriptors(comb.VMCS_Pointer(cpu)).CR0 :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0;

      comb.Descriptors(comb.VMCS_Pointer(cpu)).SHADOW_CR0 :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).CR0_READ_SHADOW;
      comb.Descriptors(comb.VMCS_Pointer(cpu)).CR3 :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR3;
      comb.Descriptors(comb.VMCS_Pointer(cpu)).CR4 :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR4;

      comb.Descriptors(comb.VMCS_Pointer(cpu)).RFLAGS :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RFLAGS;
      comb.Descriptors(comb.VMCS_Pointer(cpu)).IA32_EFER :=
        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_IA32_EFER;

      comb.Descriptors(comb.VMCS_Pointer(cpu)).Regs := comb.CPU_Regs(cpu).Regs;


      -- Assumptions
      -- 1. Vector is always >=32
      pragma Assume(Vector >= 32);
      -- 2. VT-d not available

--        if Vector >= Skp.Interrupts.Remap_Offset then
--           if Vector = SK.Constants.VTd_Fault_Vector then
--              VTd.Process_Fault;
--           else
      Vect_Nr := Remapped_Vector_Type (Vector);
      Route   := comb.Vector_Routing (Vect_Nr);

      pragma Assert(Route.Subject in subject_number);
--        if Route.Subject in subject_number then
--           Events.Insert_Event
--             (Subject => Route.Subject,
--              Event   => SK.Byte (Route.Vector));
         Subject := Route.Subject; -- parameters
         Event:= SK.Byte(Route.Vector); -- parameters

         Pos := Event_Count * Event_Pos_Type (Subject) + Event_Pos_Type (Event);
         pragma Assert (Natural (Pos) >= Event_Count * Subject and
                          Natural (Pos) < Event_Count * Subject + Event_Count,
                        "Events of unrelated subject changed");
--           Atomic_Event_Set (Event_Bit_Pos => Pos);
-- this is being done in assembly in actual code.

         -- Ada implementation of setting a bit in a 64-bit word
         Event_Word :=
           comb.Global_Events(Subject)(comb.Boolean_to_word_Bit_map(Event).Word_index).Bits;

         tmp_Event_Word := Event_Word;

         tmp_global_events := comb.Global_Events;

         Event_Word := Event_Word or
           Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(Event).Bit_No));

      pragma Assert(for all e in Vector_Range =>
                      (if(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No /=
                           comb.Boolean_to_word_Bit_map(SK.Byte(Event)).Bit_No) then
                          (Event_Word and
                              Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                           (tmp_Event_Word and
                           Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No)))));

         pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(s /= abs_dest.dest_sub or e /= abs_dest.dest_vector) then
                              (if(tmp_pea(s)(e) = True) then
                                   (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                                      Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                                   Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))
                               else
                                 (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits
                                  and Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) = 0)
                         )));

      pragma Assert(Event = SK.Byte(abs_dest.dest_vector));
      pragma Assert(Subject = abs_dest.dest_sub);

      comb.Global_Events(Subject)(comb.Boolean_to_word_Bit_map(Event).Word_index).Bits :=
        Event_Word;

      pragma Assert((if(tmp_pea(Subject)(Vector_Range(Event)) = True) then
                                   (comb.Global_Events(Subject)(comb.Boolean_to_word_Bit_map(Event).Word_index).Bits and
                                      Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(Event).Bit_No))) =
                                   Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(Event).Bit_No))
                               else
                                 (comb.Global_Events(Subject)(comb.Boolean_to_word_Bit_map(Event).Word_index).Bits
                                  and Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(Event).Bit_No))) = 0));

         pragma Assert(for all s in subject_number =>
                         (if(s /= Subject) then
                              (for all e in Event_Word_Type =>
                                   (comb.Global_Events(s)(e).Bits =
                                          tmp_global_events(s)(e).Bits))));

      pragma Assert(for all e in Vector_Range =>
                      (if(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No /=
                           comb.Boolean_to_word_Bit_map(SK.Byte(Event)).Bit_No) then
                          (comb.Global_Events(Subject)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                              Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                           (tmp_global_events(Subject)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                            Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No)))));

      pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(s /= abs_dest.dest_sub or
                              comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No /=
                            comb.Boolean_to_word_Bit_map(SK.Byte(abs_dest.dest_vector)).Bit_No) then
                              (if(tmp_pea(s)(e) = True) then
                                   (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                                      Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                                   Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))
                               else
                                 (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits
                                  and Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) = 0)
                         )));

         pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(comb.pending_event_array(s)(e) = True) then
                              (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                                 Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) =
                            Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No)))));

         pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(comb.pending_event_array(s)(e) = False) then
                              (comb.Global_Events(s)(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Word_index).Bits and
                                 Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No))) /=
                                    Shift_Left(1, Natural(comb.Boolean_to_word_Bit_map(SK.Byte(e)).Bit_No)))));
--        end if;

      -- implementation of pending event injection in the VMCS
--        Current_Subject := CPU_Global.Get_Current_Subject_ID;
--
--        Inject_Event (Subject_Id => Current_Subject);
-- parameters

      pragma Assume(comb.VMCS_Pointer(cpu) = Route.Subject);
      Current_Subject := comb.VMCS_Pointer(cpu);
      Subject_Id := Current_Subject;
      -- actual code of inject event

      --        RFLAGS := Subjects.Get_RFLAGS (Id => Subject_Id);
      RFLAGS:= comb.Descriptors(Subject_Id).RFLAGS;

--        VMX.VMCS_Read (Field => Constants.GUEST_INTERRUPTIBILITY,
--                       Value => Intr_State);


      pragma Assume((RFLAGS and 2 ** Natural (Constants.RFLAGS_IF_FLAG)) = 0);
      pragma Assert((RFLAGS and 2 ** Natural (Constants.RFLAGS_IF_FLAG)) = 0);
--        if ((RFLAGS and 2 ** Natural (Constants.RFLAGS_IF_FLAG)) /= 0) then
--  --           Events.Consume_Event (Subject => Subject_Id,
--  --                                 Found   => Event_Present,
--  --                                 Event   => Event);
--           -- Consume_Event actual code
--           Event := 0;
--
--           -- auxiliary variable
--           subject_tmp_event_array := comb.Global_Events(Subject_Id);
--
--           Search_Event_Words :
--           for Event_Word in reverse Event_Word_Type loop
--              pragma Loop_Invariant(
--                                    (for all j in Event_Word_Type =>
--                                       ((if (j > Event_Word) then
--                                           comb.Global_Events(Subject_Id)(j).Bits = 0) and
--                                          (if (j < Event_Word) then
--                                                comb.Global_Events(Subject_Id)(j).Bits =
--                                               subject_tmp_event_array(j).Bits))) and
--                                        (if (Event_Present) then
--                                           (for all k in Event_Bit_Type =>
--                                              ((if(k > Bit_in_Word) then
--                                                   (comb.Global_Events(Subject_Id)(Event_Word).Bits and
--                                                          Bitfield64_Type(2**Natural(k))) = 0) and
--                                                 (if(k < Bit_in_Word) then
--                                                      (comb.Global_Events(Subject_Id)(Event_Word).Bits and
--                                                             Bitfield64_Type(2**Natural(k))) =
--                                                  (comb.Global_Events(Subject_Id)(Event_Word).Bits and
--                                                       Bitfield64_Type(2**Natural(k)))))))
--                                   );
--              Bits := comb.Global_Events (Subject_Id) (Event_Word).Bits;
--
--  --              Find_Highest_Bit_Set
--  --                (Field => SK.Word64 (Bits),
--  --                 Found => Found,
--  --                 Pos   => Bit_In_Word); -- this is in assembly in actual code
--              -- implementation of Find highest bit set
--              Find_Highest_Bit_Set(Bits, Event_Present, Bit_In_Word);
--
--              -- end of Find highest bit set
--
--              if Event_Present then
--                 Event := SK.Byte (Event_Word) * SK.Byte (Bits_In_Word)
--                   + SK.Byte (Bit_In_Word);
--                 Pos := Event_Count * Event_Pos_Type
--                   (Subject_Id) + Event_Pos_Type (Event);
--                 pragma Assert
--                   (Natural (Pos) >= Event_Count * Subject_Id and then
--                    Natural (Pos) <  Event_Count * Subject_Id + Event_Count,
--                    "Events of unrelated subject consumed");
--  --                 Atomic_Event_Clear (Event_Bit_Pos => Pos); -- this is in assembly
--
--                 -- Ada implementation of clearing a bit in a 64-bit word
--                 Event_Word_temp := comb.Global_Events(Subject_Id)(Event_Word_Type(Event/64)).Bits;
--                 Event_Word_temp := Event_Word_temp and (not Bitfield64_Type(2**(Natural(Event mod 64))));
--                 comb.Global_Events(Subject_Id)(Event_Word_Type(Event/64)).Bits := Event_Word_temp;
--                 --auxiliary statement
--                 found_word := Event_Word;
--                 exit Search_Event_Words;
--              end if;
--
--  --              pragma Loop_Invariant((for all j in Event_Word_Type =>
--  --                                         ((if j > Event_Word then
--  --                                               comb.Global_Events(Subject_Id)(j).Bits = 0) and
--  --                                            (if j < Event_Word then
--  --                                                  comb.Global_Events(Subject_Id)(j).Bits =
--  --                                                 subject_tmp_event_array(j).Bits))) and
--  --                                           (if Event_Present then
--  --                                              (for all k in Event_Bit_Type =>
--  --                                                 ((if k >= Bit_In_Word then
--  --                                                      (comb.Global_Events(Subject_Id)(Event_Word).Bits and
--  --                                                             2**Natural(k)) = 0) and
--  --                                                    (if k < Bit_In_Word then
--  --                                                         (comb.Global_Events(Subject_Id)(Event_Word).Bits and
--  --                                                                2**Natural(k)) =
--  --                                                    (subject_tmp_event_array(Event_Word).Bits and 2**Natural(k)))))));
--           end loop Search_Event_Words;
--
--           -- end of consume event
--
--
--           if Event_Present then
--  --              VMX.VMCS_Write -- this has to be done.
--  --                (Field => Constants.VM_ENTRY_INTERRUPT_INFO,
--  --                 Value => Constants.VM_INTERRUPT_INFO_VALID + SK.Word64 (Event));
--              -- assumption
--              comb.VMCSs(comb.VMCS_Pointer(cpu)).VM_ENTRY_INTERRUPT_INFO :=
--                SK.Word32(Constants.VM_INTERRUPT_INFO_VALID + SK.Word64(Event));
--              comb.CPU_Regs(cpu).RFLAGS := comb.CPU_Regs(cpu).RFLAGS and
--                (not (2**(Natural(Constants.RFLAGS_IF_FLAG))));
--           end if;
--        end if;

--        Events.Has_Pending_Events (Subject       => Subject_Id,
--                                   Event_Pending => Event_Pending);
      -- code of Has_Pending_Events
      Search_Event_Words_Pending :
      for Event_Word in reverse Event_Word_Type loop
         pragma Loop_Invariant(True);
         Bits := comb.Global_Events (Subject_Id) (Event_Word).Bits;

         pragma Warnings
           (GNATprove, Off, "unused assignment to ""Unused_Pos""",
            Reason => "Only Event_Pending is needed");
--           Find_Highest_Bit_Set
--             (Field => SK.Word64 (Bits),
--              Found => Event_Pending,
--              Pos   => Unused_Pos);
         Find_Highest_Bit_Set(Bits, Event_Pending, Bit_In_Word);

         exit Search_Event_Words_Pending when Event_Pending;
      end loop Search_Event_Words_Pending;

      if Event_Pending then
--           VMX.VMCS_Set_Interrupt_Window (Value => True); -- this has to be done
         -- implementation of VMCS_Set_Interrupt_Window
--           VMCS_Read (Field => Constants.CPU_BASED_EXEC_CONTROL,
--                      Value => Cur_Flags);
         Cur_Flags := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).CPU_BASED_EXEC_CONTROL);

--           Cur_Flags := SK.Bit_Set -- implement bit set - can be just addition
--             (Value => Cur_Flags,
--              Pos   => Interrupt_Window_Exit_Flag);

         Cur_Flags := Cur_Flags or SK.Word64(2**Natural(Interrupt_Window_Exit_Flag));

--           VMCS_Write (Field => Constants.CPU_BASED_EXEC_CONTROL,
--                       Value => Cur_Flags);
         comb.VMCSs(comb.VMCS_Pointer(cpu)).CPU_BASED_EXEC_CONTROL := SK.Word32(Cur_Flags);
         -- end of VMCS_Set_Interrupt_Window
      end if;
      -- end of Has_Pending_Events

      -- code of set_vmx_timer

      -- restoring the state of the subject
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP :=
        comb.Descriptors(comb.VMCS_Pointer(cpu)).RIP;
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP :=
        comb.Descriptors(comb.VMCS_Pointer(cpu)).RSP;
      comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0 :=
        comb.Descriptors(comb.VMCS_Pointer(cpu)).CR0;
      comb.VMCSs(comb.VMCS_Pointer(cpu)).CR0_READ_SHADOW :=
        comb.Descriptors(comb.VMCS_Pointer(cpu)).SHADOW_CR0;

      comb.CPU_Regs(cpu).Regs := comb.Descriptors(comb.VMCS_Pointer(cpu)).Regs;

      -- modelling VM-entry
      -- loading guest control registers, debug registers and MSRs
      comb.CPU_Regs(cpu).CR0 := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0;
      comb.CPU_Regs(cpu).CR3 := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR3;
      comb.CPU_Regs(cpu).CR4 := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR4;

      -- loading guest segment registers and descriptor table registers
      comb.CPU_Regs(cpu).CS := Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_CS);
      comb.CPU_Regs(cpu).SS := Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_SS);

--        comb.IDTR_array(cpu).base := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_BASE_IDTR;           | look at it
--        comb.IDTR_array(cpu).limit := Word16(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_IDTR); | later

      -- loading guest RIP, RSP and RFLAGS
      comb.CPU_Regs(cpu).RIP := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP;
      comb.CPU_Regs(cpu).RSP := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP;
      comb.CPU_Regs(cpu).RFLAGS := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RFLAGS;

      -- modelling event injection
      if(comb.VMCSs(comb.VMCS_Pointer(cpu)).VM_ENTRY_INTERRUPT_INFO
         >= 16#8000_0000#) then

         -- updating stack
         comb.Conc_Stack(comb.VMCS_Pointer(cpu)).stack
           (comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top) :=
           comb.CPU_Regs(cpu).RSP;
         comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top :=
           comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top + 1;

         comb.Conc_Stack(comb.VMCS_Pointer(cpu)).stack
           (comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top) :=
           comb.CPU_Regs(cpu).RFLAGS;
         comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top :=
           comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top + 1;

         comb.Conc_Stack(comb.VMCS_Pointer(cpu)).stack
           (comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top) :=
           comb.CPU_Regs(cpu).RIP;
         comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top :=
           comb.Conc_Stack(comb.VMCS_Pointer(cpu)).top + 1;

         -- jumping to the ISR
         comb.CPU_Regs(cpu).RIP :=
           Word64(comb.Conc_IDTs(comb.IDTR_array(cpu))
                  (Vector_Range(comb.VMCSs(comb.VMCS_Pointer(cpu)).VM_ENTRY_INTERRUPT_INFO and
                       16#0000_00FF#)));
         comb.CPU_Regs(cpu).RFLAGS := comb.CPU_Regs(cpu).RFLAGS or
           2 ** Natural (Constants.RFLAGS_IF_FLAG);
      end if;

--
      -- Assertions
      pragma Assert(for all c1 in cpu_number =>
                      (for all c2 in cpu_number =>
                         (if(comb.current_subject(c1) = comb.current_subject(c2)) then
                               c1 = c2)));

      pragma Assert(for all i in Port_Number_Type =>
                      (comb.Abs_IO_Ports(i) = comb.Conc_IO_Ports(i)));

      pragma Assert(for all s in subject_number =>
              (for all e in Vector_Range =>
                   (if(comb.pending_event_array(s)(e) = True) then
                      (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
                        2**Natural(e mod 64)
                    else
                   (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) = 0)));

      pragma Assert((for all c in cpu_number =>
                       (comb.Subjects_State(comb.current_subject(c)) = comb.CPU_Regs(c))) and

                        (for all s in subject_number =>
                           (if(for all c in cpu_number => (comb.current_subject(c) /= s)) then
                                 comb.Subjects_State(s) = comb.Descriptors(s))));

      pragma Assert(for all c in cpu_number =>
                      (comb.VMCS_Pointer(c) = comb.current_subject(c)));

      pragma Assert(for all irq in irq_number =>
                      ((comb.abs_red_tab(irq).dest_sub =
                         comb.Vector_Routing(Remapped_Vector_Type(comb.IOAPIC_red_tab(irq).dest_vector)).Subject) and
                         (comb.abs_red_tab(irq).dest_vector =
                              comb.Vector_Routing(Remapped_Vector_Type(comb.IOAPIC_red_tab(irq).dest_vector)).Vector)));

      pragma Assert(for all s in subject_number =>
                      (comb.VMCSs(s).IO_BITMAP = comb.Abs_IO_Bitmap(s)));

      pragma Assert(for all s in subject_number =>
              (for all e in Vector_Range =>
                   (if(comb.pending_event_array(s)(e) = True) then
                      (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
                        2**Natural(e mod 64)
                    else
                   (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) = 0)));

   end combined_ext_int;

   ------------------------------------------------------------------
   ------------------------------------------------------------------

   -- proved.
   procedure combined_IN(c: cpu_number; port_no: Port_Number_Type) is
      bitmap: subject_IO_Bitmap_type;
   begin
      -- abstract

      if comb.Abs_IO_Bitmap(comb.current_subject(c))(port_no) = True then
         comb.Subjects_State(comb.current_subject(c)).Regs.RAX :=
           SK.Word64(comb.Abs_IO_Ports(port_no));
      end if;

      -- concrete
      bitmap := comb.VMCSs(comb.VMCS_Pointer(c)).IO_BITMAP; -- change the IO_Bitmap in the SK_CPU
                                                            -- bring full IO_Bitmap in the VMCS
      if bitmap(port_no) = True then
         comb.CPU_Regs(c).Regs.RAX := SK.Word64(comb.Conc_IO_Ports(port_no));
      end if;

      pragma Assert(for all s in subject_number =>
              (for all e in Vector_Range =>
                   (if(comb.pending_event_array(s)(e) = True) then
                      (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
                        2**Natural(e mod 64)
                    else
                   (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) = 0)));
   end combined_IN;

   -----------------------------------------------------------------
   -----------------------------------------------------------------

--     procedure IRET(cpu: cpu_number) is
--        found: Boolean;
--        event: Vector_Range;
--        Cur_Flags : SK.Word64;
--        Interrupt_Window_Exit_Flag : constant SK.Word64_Pos := 2;
--        Current_Subject: subject_number;
--        Subject_Id: subject_number;
--        RFLAGS        : SK.Word64;
--        subject_tmp_event_array: Event_Array;
--        Bits        : Bitfield64_Type;
--        Bit_In_Word : Event_Bit_Type;
--        Event_Present : Boolean := False;
--        Event_Pending : Boolean;
--        Event_Word_temp: Bitfield64_Type;
--        Pos : Event_Pos_Type;
--        found_word: Event_Word_Type := Event_Word_Type'Last;
--     begin
--
--        -- abstract
--        -- setting IF flag
--        if((comb.Subjects_State(comb.current_subject(cpu)).RFLAGS and
--              16#0000000000000200#) = 0)
--        then
--           comb.Subjects_State(comb.current_subject(cpu)).RFLAGS :=
--             comb.Subjects_State(comb.current_subject(cpu)).RFLAGS +
--               16#0000000000000200#;
--        end if;
--
--        -- if there are pending events then clear them in the array
--        find_highest_pending_event(comb.current_subject(cpu), found, event, comb);
--        if(found) then
--           comb.pending_event_array(comb.current_subject(cpu))(event) := False;
--           -- clearing the IF flag
--           comb.Subjects_State(comb.current_subject(cpu)).RFLAGS :=
--             comb.Subjects_State(comb.current_subject(cpu)).RFLAGS -
--             16#0000000000000200#;
--           comb.Subjects_State(comb.current_subject(cpu)).RIP :=
--             comb.Abs_IDTs(comb.current_subject(cpu))(event);
--        end if;
--
--
--        -- concrete
--        -- IF flag set
--        comb.CPU_Regs(cpu).RFLAGS := comb.CPU_Regs(cpu).RFLAGS or 16#0000000000000200#;
--
--        -- VM exit modelling - saving state and etc.
--
--        -- processor state is saved
--        --    saving control registers
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0 := comb.CPU_Regs(cpu).CR0;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR3 := comb.CPU_Regs(cpu).CR3;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR4 := comb.CPU_Regs(cpu).CR4;
--
--        --    saving debug registers
--
--
--        --    saving MSRs
--
--
--        --    saving segment registers and descriptor table registers
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_CS :=
--          SK.Word16(comb.CPU_Regs(cpu).CS);
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_DS := ;
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_ES := ;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_SS :=
--          SK.Word16(comb.CPU_Regs(cpu).SS);
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_CS := ;
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_DS := ;
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_ES := ;
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_SS := ;
--  --                                                               _
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_BASE_IDTR :=   |
--  --          comb.IDTR_array(cpu).base;                            |Look at these
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_IDTR :=  |later
--  --          SK.Word32(comb.IDTR_array(cpu).limit);                |
--  --                                                               _|
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_BASE_GDTR := comb.CPU_Regs();
--  --        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_GDTR := ;
--
--        --    saving RIP, RSP and RFLAGS --assumptions involved while saving
--        --                                         RIP and RFLAGS, check manual
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP := comb.CPU_Regs(cpu).RIP;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP := comb.CPU_Regs(cpu).RSP;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RFLAGS := comb.CPU_Regs(cpu).RFLAGS;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_IA32_EFER :=
--          comb.CPU_Regs(cpu).IA32_EFER;
--
--
--        -- MSRs are saved to VM-exit MSR-store area
--
--
--        -- processor state is loaded from host state area
--        --     loading control registers
--        comb.CPU_Regs(cpu).CR0 := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_CR0;
--        comb.CPU_Regs(cpu).CR3 := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_CR3;
--        comb.CPU_Regs(cpu).CR4 := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_CR4;
--
--        --     loading host segment and descriptor-table registers
--        comb.CPU_Regs(cpu).CS :=
--          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_CS);
--  --        comb.CPU_Regs(cpu).DS :=
--  --          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_DS);
--  --        comb.CPU_Regs(cpu).ES :=
--  --          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_ES);
--        comb.CPU_Regs(cpu).SS :=
--          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_SS);
--  --        comb.CPU_Regs(cpu).FS :=
--  --          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_FS);
--  --        comb.CPU_Regs(cpu).GS :=
--  --          SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_SEL_GS);
--  --                                                             _
--  --        comb.IDTR_array(cpu).base :=                          | look at it
--  --          comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_BASE_IDTR; _| later
--  --
--  --        comb.IDTR_array(cpu).limit :=
--  --          comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_;
--  --         :=
--  --          comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_BASE_GDTR;
--
--        --     loading RIP, RSP and RFLAGS
--        comb.CPU_Regs(cpu).RIP := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_RIP;
--        comb.CPU_Regs(cpu).RSP := comb.VMCSs(comb.VMCS_Pointer(cpu)).HOST_RSP;
--        comb.CPU_Regs(cpu).RFLAGS := 2;
--
--
--
--        -- MSRs are loaded from VM-exit MSR-load area
--
--
--        -- actual kernel code
--        -- saving state in descriptors
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).Exit_Reason :=
--          Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_REASON);
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).Exit_Qualification :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_QUALIFICATION;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).Interrupt_Info :=
--          Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_INTR_INFO);
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).Instruction_Len :=
--          Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).VMX_EXIT_INSTRUCTION_LEN);
--
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).Guest_Phys_Addr :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_PHYSICAL_ADDRESS;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).RIP :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).CS :=
--          Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_CS);
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).RSP :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).CR0 :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).SHADOW_CR0 :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).CR0_READ_SHADOW;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).CR3 :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR3;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).CR4 :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR4;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).RFLAGS :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RFLAGS;
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).IA32_EFER :=
--          comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_IA32_EFER;
--
--        comb.Descriptors(comb.VMCS_Pointer(cpu)).Regs := comb.CPU_Regs(cpu).Regs;
--
--  --        VMX.VMCS_Set_Interrupt_Window (Value => False); -- this has to be done
--           -- implementation of VMCS_Set_Interrupt_Window
--  --           VMCS_Read (Field => Constants.CPU_BASED_EXEC_CONTROL,
--  --                      Value => Cur_Flags);
--           Cur_Flags := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).CPU_BASED_EXEC_CONTROL);
--
--  --           Cur_Flags := SK.Bit_Set -- implement bit set - can be just addition
--  --             (Value => Cur_Flags,
--  --              Pos   => Interrupt_Window_Exit_Flag);
--
--           Cur_Flags := Cur_Flags and not (SK.Word64(2**Natural(Interrupt_Window_Exit_Flag)));
--
--  --           VMCS_Write (Field => Constants.CPU_BASED_EXEC_CONTROL,
--  --                       Value => Cur_Flags);
--           comb.VMCSs(comb.VMCS_Pointer(cpu)).CPU_BASED_EXEC_CONTROL := SK.Word32(Cur_Flags);
--           -- end of VMCS_Set_Interrupt_Window
--
--        -- implementation of pending event injection in the VMCS
--  --        Current_Subject := CPU_Global.Get_Current_Subject_ID;
--  --
--  --        Inject_Event (Subject_Id => Current_Subject);
--  -- parameters
--        Current_Subject := comb.VMCS_Pointer(cpu);
--        Subject_Id := Current_Subject;
--        -- actual code of inject event
--
--        --        RFLAGS := Subjects.Get_RFLAGS (Id => Subject_Id);
--        RFLAGS:= comb.Descriptors(Subject_Id).RFLAGS;
--
--  --        VMX.VMCS_Read (Field => Constants.GUEST_INTERRUPTIBILITY,
--  --                       Value => Intr_State);
--
--
--        if ((RFLAGS and 2 ** Natural (Constants.RFLAGS_IF_FLAG)) /= 0) then
--  --           Events.Consume_Event (Subject => Subject_Id,
--  --                                 Found   => Event_Present,
--  --                                 Event   => Event);
--           -- Consume_Event actual code
--           Event := 0;
--
--           -- auxiliary variable
--           subject_tmp_event_array := comb.Global_Events(Subject_Id);
--
--           Search_Event_Words :
--           for Event_Word in reverse Event_Word_Type loop
--  --              pragma Loop_Invariant((for all word_index in Event_Word_Type =>
--  --                                      (if(word_index > Event_Word) then
--  --                                           (comb.Global_Events(Subject_Id)(word_index).Bits = 0))) and
--  --                                      (if Event_Present then )));
--              Bits := comb.Global_Events (Subject_Id) (Event_Word).Bits;
--
--  --              Find_Highest_Bit_Set
--  --                (Field => SK.Word64 (Bits),
--  --                 Found => Found,
--  --                 Pos   => Bit_In_Word); -- this is in assembly in actual code
--              -- implementation of Find highest bit set
--              Find_Highest_Bit_Set(Bits, Event_Present, Bit_In_Word);
--
--              -- end of Find highest bit set
--
--              if Event_Present then
--                 Event := SK.Byte (Event_Word) * SK.Byte (Bits_In_Word)
--                   + SK.Byte (Bit_In_Word);
--                 Pos := Event_Count * Event_Pos_Type
--                   (Subject_Id) + Event_Pos_Type (Event);
--                 pragma Assert
--                   (Natural (Pos) >= Event_Count * Subject_Id and then
--                    Natural (Pos) <  Event_Count * Subject_Id + Event_Count,
--                    "Events of unrelated subject consumed");
--  --                 Atomic_Event_Clear (Event_Bit_Pos => Pos); -- this is in assembly
--
--                 -- Ada implementation of clearing a bit in a 64-bit word
--                 Event_Word_temp := comb.Global_Events(Subject_Id)(Event_Word_Type(Event/64)).Bits;
--                 Event_Word_temp := Event_Word_temp and (not Bitfield64_Type(2**(Natural(Event) mod 64)));
--                 comb.Global_Events(Subject_Id)(Event_Word_Type(Event)/64).Bits := Event_Word_temp;
--                 --auxiliary statement
--                 found_word := Event_Word;
--                 exit Search_Event_Words;
--              end if;
--
--  --              pragma Loop_Invariant((for all j in Event_Word_Type =>
--  --                                         ((if j > Event_Word then
--  --                                               comb.Global_Events(Subject_Id)(j).Bits = 0) and
--  --                                            (if j < Event_Word then
--  --                                                  comb.Global_Events(Subject_Id)(j).Bits =
--  --                                                 subject_tmp_event_array(j).Bits))) and
--  --                                           (if Event_Present then
--  --                                              (for all k in Event_Bit_Type =>
--  --                                                 ((if k >= Bit_In_Word then
--  --                                                      (comb.Global_Events(Subject_Id)(Event_Word).Bits and
--  --                                                             2**Natural(k)) = 0) and
--  --                                                    (if k < Bit_In_Word then
--  --                                                         (comb.Global_Events(Subject_Id)(Event_Word).Bits and
--  --                                                                2**Natural(k)) =
--  --                                                    (subject_tmp_event_array(Event_Word).Bits and 2**Natural(k)))))));
--           end loop Search_Event_Words;
--           pragma Assert((for all j in Event_Word_Type =>
--                                         ((if j > found_word then
--                                               comb.Global_Events(Subject_Id)(j).Bits = 0) and
--                                            (if j < found_word then
--                                                  comb.Global_Events(Subject_Id)(j).Bits =
--                                                 subject_tmp_event_array(j).Bits))) and
--                                           (if Event_Present then
--                                              (for all k in Event_Bit_Type =>
--                                                 ((if k >= Bit_In_Word then
--                                                      (comb.Global_Events(Subject_Id)(found_word).Bits and
--                                                             2**Natural(k)) = 0) and
--                                                    (if k < Bit_In_Word then
--                                                         (comb.Global_Events(Subject_Id)(found_word).Bits and
--                                                                2**Natural(k)) =
--                                                    (subject_tmp_event_array(found_word).Bits and 2**Natural(k)))))));
--
--           -- end of consume event
--
--
--           if Event_Present then
--  --              VMX.VMCS_Write -- this has to be done.
--  --                (Field => Constants.VM_ENTRY_INTERRUPT_INFO,
--  --                 Value => Constants.VM_INTERRUPT_INFO_VALID + SK.Word64 (Event));
--              -- assumption
--              comb.VMCSs(comb.VMCS_Pointer(cpu)).VM_ENTRY_INTERRUPT_INFO :=
--                SK.Word32(Constants.VM_INTERRUPT_INFO_VALID + SK.Word64(Event));
--              comb.CPU_Regs(cpu).RFLAGS := comb.CPU_Regs(cpu).RFLAGS and
--                (not (2**(Natural(Constants.RFLAGS_IF_FLAG))));
--           end if;
--        end if;
--
--  --        Events.Has_Pending_Events (Subject       => Subject_Id,
--  --                                   Event_Pending => Event_Pending);
--        -- code of Has_Pending_Events
--        Search_Event_Words_Pending :
--        for Event_Word in reverse Event_Word_Type loop
--           pragma Loop_Invariant(True);
--           Bits := comb.Global_Events (Subject_Id) (Event_Word).Bits;
--
--           pragma Warnings
--             (GNATprove, Off, "unused assignment to ""Unused_Pos""",
--              Reason => "Only Event_Pending is needed");
--  --           Find_Highest_Bit_Set
--  --             (Field => SK.Word64 (Bits),
--  --              Found => Event_Pending,
--  --              Pos   => Unused_Pos);
--           Find_Highest_Bit_Set(Bits, Event_Pending, Bit_In_Word);
--
--           exit Search_Event_Words_Pending when Event_Pending;
--        end loop Search_Event_Words_Pending;
--
--        if Event_Pending then
--  --           VMX.VMCS_Set_Interrupt_Window (Value => True); -- this has to be done
--           -- implementation of VMCS_Set_Interrupt_Window
--  --           VMCS_Read (Field => Constants.CPU_BASED_EXEC_CONTROL,
--  --                      Value => Cur_Flags);
--           Cur_Flags := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).CPU_BASED_EXEC_CONTROL);
--
--  --           Cur_Flags := SK.Bit_Set -- implement bit set - can be just addition
--  --             (Value => Cur_Flags,
--  --              Pos   => Interrupt_Window_Exit_Flag);
--
--           Cur_Flags := Cur_Flags or SK.Word64(2**Natural(Interrupt_Window_Exit_Flag));
--
--  --           VMCS_Write (Field => Constants.CPU_BASED_EXEC_CONTROL,
--  --                       Value => Cur_Flags);
--           comb.VMCSs(comb.VMCS_Pointer(cpu)).CPU_BASED_EXEC_CONTROL := SK.Word32(Cur_Flags);
--           -- end of VMCS_Set_Interrupt_Window
--        end if;
--        -- end of Has_Pending_Events
--
--        -- code of set_vmx_timer
--
--        -- restoring the state of the subject
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP :=
--          comb.Descriptors(comb.VMCS_Pointer(cpu)).RIP;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP :=
--          comb.Descriptors(comb.VMCS_Pointer(cpu)).RSP;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0 :=
--          comb.Descriptors(comb.VMCS_Pointer(cpu)).CR0;
--        comb.VMCSs(comb.VMCS_Pointer(cpu)).CR0_READ_SHADOW :=
--          comb.Descriptors(comb.VMCS_Pointer(cpu)).SHADOW_CR0;
--
--        comb.CPU_Regs(cpu).Regs := comb.Descriptors(comb.VMCS_Pointer(cpu)).Regs;
--
--        -- modelling VM-entry
--        -- loading guest control registers, debug registers and MSRs
--        comb.CPU_Regs(cpu).CR0 := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR0;
--        comb.CPU_Regs(cpu).CR3 := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR3;
--        comb.CPU_Regs(cpu).CR4 := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_CR4;
--
--        -- loading guest segment registers and descriptor table registers
--        comb.CPU_Regs(cpu).CS := Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_CS);
--        comb.CPU_Regs(cpu).SS := Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_SEL_SS);
--
--  --        comb.IDTR_array(cpu).base := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_BASE_IDTR;           | look at it
--  --        comb.IDTR_array(cpu).limit := Word16(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_LIMIT_IDTR); | later
--
--        -- loading guest RIP, RSP and RFLAGS
--        comb.CPU_Regs(cpu).RIP := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RIP;
--        comb.CPU_Regs(cpu).RSP := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RSP;
--        comb.CPU_Regs(cpu).RFLAGS := comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_RFLAGS;
--
--        -- modelling event injection
--        if(comb.VMCSs(comb.VMCS_Pointer(cpu)).VM_ENTRY_INTERRUPT_INFO
--           >= 16#8000_0000#) then
--           comb.CPU_Regs(cpu).RIP :=
--             Word64(comb.Conc_IDTs(comb.IDTR_array(cpu))
--                    (Vector_Range(comb.VMCSs(comb.VMCS_Pointer(cpu)).VM_ENTRY_INTERRUPT_INFO and
--                         16#0000_00FF#)));
--           comb.CPU_Regs(cpu).RFLAGS := comb.CPU_Regs(cpu).RFLAGS or
--             2 ** Natural (Constants.RFLAGS_IF_FLAG);
--        end if;
--
--  --
--        -- Assertions
--        pragma Assert(for all c1 in cpu_number =>
--                        (for all c2 in cpu_number =>
--                           (if(comb.current_subject(c1) = comb.current_subject(c2)) then
--                                 c1 = c2)));
--
--        pragma Assert(for all i in Port_Number_Type =>
--                        (comb.Abs_IO_Ports(i) = comb.Conc_IO_Ports(i)));
--
--        pragma Assert(for all s in subject_number =>
--                (for all e in Vector_Range =>
--                     (if(comb.pending_event_array(s)(e) = True) then
--                        (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
--                          2**Natural(e mod 64)
--                      else
--                     (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) = 0)));
--
--        pragma Assert((for all c in cpu_number =>
--                         (comb.Subjects_State(comb.current_subject(c)) = comb.CPU_Regs(c))) and
--
--                          (for all s in subject_number =>
--                             (if(for all c in cpu_number => (comb.current_subject(c) /= s)) then
--                                   comb.Subjects_State(s) = comb.Descriptors(s))));
--
--        pragma Assert(for all c in cpu_number =>
--                        (comb.VMCS_Pointer(c) = comb.current_subject(c)));
--
--        pragma Assert(for all irq in irq_number =>
--                        ((comb.abs_red_tab(irq).dest_sub =
--                           comb.Vector_Routing(Remapped_Vector_Type(comb.IOAPIC_red_tab(irq).dest_vector)).Subject) and
--                           (comb.abs_red_tab(irq).dest_vector =
--                                comb.Vector_Routing(Remapped_Vector_Type(comb.IOAPIC_red_tab(irq).dest_vector)).Vector)));
--
--        pragma Assert(for all s in subject_number =>
--                        (comb.VMCSs(s).IO_BITMAP = comb.Abs_IO_Bitmap(s)));
--
--        pragma Assert(for all s in subject_number =>
--                (for all e in Vector_Range =>
--                     (if(comb.pending_event_array(s)(e) = True) then
--                        (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) =
--                          2**Natural(e mod 64)
--                      else
--                     (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**Natural(e mod 64)) = 0)));
--
--     end IRET;

   procedure experiment
   is
      Subject: subject_number := 0;
      Event: Vector_Range := 20;
      tmp_pea: pending_event_array_type;
      tmp_global_events: Global_Event_Array;
      check_event: Vector_Range := 0;
   begin
      -- assertion before start of the program
      pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(comb.pending_event_array(s)(e) = True) then
                              (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and
                                 Shift_Left(1, Natural(e mod 64))) =
                                Shift_Left(1, Natural(e mod 64))
                          else
                            (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and
                                 Shift_Left(1, Natural(e mod 64))) = 0)));

      -- copying state in auxiliary variable
      tmp_pea := comb.pending_event_array;

      -- assertion - all except current (subject, event) are still glued
      pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(s /= Subject or
                              e /= Event) then
                              (if(tmp_pea(s)(e) = True) then
                                   (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and
                                      Shift_Left(1, Natural(e mod 64))) =
                                   Shift_Left(1, Natural(e mod 64))
                               else
                                 (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits
                                  and Shift_Left(1, Natural(e mod 64))) = 0)
                         )));

      -- abstract operation
      comb.pending_event_array(Subject)(Event) := True;

      -- assert - temp pea still glues with concrete
      pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(s /= Subject or
                              e /= Event) then
                              (if(tmp_pea(s)(e) = True) then
                                   (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and
                                      Shift_Left(1, Natural(e mod 64))) =
                                   Shift_Left(1, Natural(e mod 64))
                               else
                                 (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits
                                  and Shift_Left(1, Natural(e mod 64))) = 0)
                         )));

      -- auxiliary operation
      tmp_global_events := comb.Global_Events;

      -- concrete operation
      comb.Global_Events(Subject)(Event_Word_Type(Event/64)).Bits :=
        (comb.Global_Events(Subject)(Event_Word_Type(Event/64)).Bits OR
             Bitfield64_Type(2**Natural(Event mod 64)));

      -- assertions for proof
      pragma Assert(for all s in subject_number =>
                      (for all e in Event_Word_Type =>
                         (if(e /= Event_Word_Type(e/64)) then
                               comb.Global_Events(s)(e) =
                              tmp_global_events(s)(e))));

      pragma Assert((comb.Global_Events(Subject)(Event_Word_Type(Check_event/64)).Bits and
                      Bitfield64_Type(2**Natural(Check_event mod 64))) =
                    (tmp_global_events(Subject)(Event_Word_Type(check_event/64)).Bits and
                      Bitfield64_Type(2**Natural(check_event mod 64))));
      pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(s /= Subject or
                              e /= Event) then
                              (if(tmp_pea(s)(e) = True) then
                                   (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and
                                      Shift_Left(1, Natural(e mod 64))) =
                                   Shift_Left(1, Natural(e mod 64))
                               else
                                 (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits
                                  and Shift_Left(1, Natural(e mod 64))) = 0)
                         )));

      pragma Assert(comb.pending_event_array(Subject)(Event) = True);

      pragma Assert((comb.Global_Events(Subject)(Event_Word_Type(Event/64)).Bits and
                      Bitfield64_Type(2**Natural(Event mod 64))) =
                        Bitfield64_Type(2**Natural(Event mod 64)));

      pragma Assert(if(comb.pending_event_array(Subject)(Event) = True) then
                      ((comb.Global_Events(Subject)(Event_Word_Type(Event/64)).Bits and
                         Bitfield64_Type(2**Natural(Event mod 64))) =
                           Bitfield64_Type(2**Natural(Event mod 64))));
      pragma Assert(for all s in subject_number =>
                      (for all e in Vector_Range =>
                         (if(comb.pending_event_array(s)(e) = True) then
                              (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and
                                 Shift_Left(1, Natural(e mod 64))) =
                                Shift_Left(1, Natural(e mod 64))
                          else
                            (comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and
                                 Shift_Left(1, Natural(e mod 64))) = 0)));
   end;

   function Concrete_map (event: in SK.Byte)
                          return Boolean_to_Word_Bit_map_entry_type is
   begin
      case event is
         when 0 => return (0,0);
         when 1 => return (0,1);
         when 2 => return (0,2);
         when 3 => return (0,3);
         when 4 => return (0,4);
         when 5 => return (0,5);
         when 6 => return (0,6);
         when 7 => return (0,7);
         when 8 => return (0,8);
         when 9 => return (0,9);
         when 10 => return (0,10);
         when 11 => return (0,11);
         when 12 => return (0,12);
         when 13 => return (0,13);
         when 14 => return (0,14);
         when 15 => return (0,15);
         when 16 => return (0,16);
         when 17 => return (0,17);
         when 18 => return (0,18);
         when 19 => return (0,19);
         when 20 => return (0,20);
         when 21 => return (0,21);
         when 22 => return (0,22);
         when 23 => return (0,23);
         when 24 => return (0,24);
         when 25 => return (0,25);
         when 26 => return (0,26);
         when 27 => return (0,27);
         when 28 => return (0,28);
         when 29 => return (0,29);
         when 30 => return (0,30);
         when 31 => return (0,31);
         when 32 => return (0,32);
         when 33 => return (0,33);
         when 34 => return (0,34);
         when 35 => return (0,35);
         when 36 => return (0,36);
         when 37 => return (0,37);
         when 38 => return (0,38);
         when 39 => return (0,39);
         when 40 => return (0,40);
         when 41 => return (0,41);
         when 42 => return (0,42);
         when 43 => return (0,43);
         when 44 => return (0,44);
         when 45 => return (0,45);
         when 46 => return (0,46);
         when 47 => return (0,47);
         when 48 => return (0,48);
         when 49 => return (0,49);
         when 50 => return (0,50);
         when 51 => return (0,51);
         when 52 => return (0,52);
         when 53 => return (0,53);
         when 54 => return (0,54);
         when 55 => return (0,55);
         when 56 => return (0,56);
         when 57 => return (0,57);
         when 58 => return (0,58);
         when 59 => return (0,59);
         when 60 => return (0,60);
         when 61 => return (0,61);
         when 62 => return (0,62);
         when 63 => return (0,63);
         when 64 => return (1,0);
         when 65 => return (1,1);
         when 66 => return (1,2);
         when 67 => return (1,3);
         when 68 => return (1,4);
         when 69 => return (1,5);
         when 70 => return (1,6);
         when 71 => return (1,7);
         when 72 => return (1,8);
         when 73 => return (1,9);
         when 74 => return (1,10);
         when 75 => return (1,11);
         when 76 => return (1,12);
         when 77 => return (1,13);
         when 78 => return (1,14);
         when 79 => return (1,15);
         when 80 => return (1,16);
         when 81 => return (1,17);
         when 82 => return (1,18);
         when 83 => return (1,19);
         when 84 => return (1,20);
         when 85 => return (1,21);
         when 86 => return (1,22);
         when 87 => return (1,23);
         when 88 => return (1,24);
         when 89 => return (1,25);
         when 90 => return (1,26);
         when 91 => return (1,27);
         when 92 => return (1,28);
         when 93 => return (1,29);
         when 94 => return (1,30);
         when 95 => return (1,31);
         when 96 => return (1,32);
         when 97 => return (1,33);
         when 98 => return (1,34);
         when 99 => return (1,35);
         when 100 => return (1,36);
         when 101 => return (1,37);
         when 102 => return (1,38);
         when 103 => return (1,39);
         when 104 => return (1,40);
         when 105 => return (1,41);
         when 106 => return (1,42);
         when 107 => return (1,43);
         when 108 => return (1,44);
         when 109 => return (1,45);
         when 110 => return (1,46);
         when 111 => return (1,47);
         when 112 => return (1,48);
         when 113 => return (1,49);
         when 114 => return (1,50);
         when 115 => return (1,51);
         when 116 => return (1,52);
         when 117 => return (1,53);
         when 118 => return (1,54);
         when 119 => return (1,55);
         when 120 => return (1,56);
         when 121 => return (1,57);
         when 122 => return (1,58);
         when 123 => return (1,59);
         when 124 => return (1,60);
         when 125 => return (1,61);
         when 126 => return (1,62);
         when 127 => return (1,63);
         when 128 => return (2,0);
         when 129 => return (2,1);
         when 130 => return (2,2);
         when 131 => return (2,3);
         when 132 => return (2,4);
         when 133 => return (2,5);
         when 134 => return (2,6);
         when 135 => return (2,7);
         when 136 => return (2,8);
         when 137 => return (2,9);
         when 138 => return (2,10);
         when 139 => return (2,11);
         when 140 => return (2,12);
         when 141 => return (2,13);
         when 142 => return (2,14);
         when 143 => return (2,15);
         when 144 => return (2,16);
         when 145 => return (2,17);
         when 146 => return (2,18);
         when 147 => return (2,19);
         when 148 => return (2,20);
         when 149 => return (2,21);
         when 150 => return (2,22);
         when 151 => return (2,23);
         when 152 => return (2,24);
         when 153 => return (2,25);
         when 154 => return (2,26);
         when 155 => return (2,27);
         when 156 => return (2,28);
         when 157 => return (2,29);
         when 158 => return (2,30);
         when 159 => return (2,31);
         when 160 => return (2,32);
         when 161 => return (2,33);
         when 162 => return (2,34);
         when 163 => return (2,35);
         when 164 => return (2,36);
         when 165 => return (2,37);
         when 166 => return (2,38);
         when 167 => return (2,39);
         when 168 => return (2,40);
         when 169 => return (2,41);
         when 170 => return (2,42);
         when 171 => return (2,43);
         when 172 => return (2,44);
         when 173 => return (2,45);
         when 174 => return (2,46);
         when 175 => return (2,47);
         when 176 => return (2,48);
         when 177 => return (2,49);
         when 178 => return (2,50);
         when 179 => return (2,51);
         when 180 => return (2,52);
         when 181 => return (2,53);
         when 182 => return (2,54);
         when 183 => return (2,55);
         when 184 => return (2,56);
         when 185 => return (2,57);
         when 186 => return (2,58);
         when 187 => return (2,59);
         when 188 => return (2,60);
         when 189 => return (2,61);
         when 190 => return (2,62);
         when 191 => return (2,63);
         when 192 => return (3,0);
         when 193 => return (3,1);
         when 194 => return (3,2);
         when 195 => return (3,3);
         when 196 => return (3,4);
         when 197 => return (3,5);
         when 198 => return (3,6);
         when 199 => return (3,7);
         when 200 => return (3,8);
         when 201 => return (3,9);
         when 202 => return (3,10);
         when 203 => return (3,11);
         when 204 => return (3,12);
         when 205 => return (3,13);
         when 206 => return (3,14);
         when 207 => return (3,15);
         when 208 => return (3,16);
         when 209 => return (3,17);
         when 210 => return (3,18);
         when 211 => return (3,19);
         when 212 => return (3,20);
         when 213 => return (3,21);
         when 214 => return (3,22);
         when 215 => return (3,23);
         when 216 => return (3,24);
         when 217 => return (3,25);
         when 218 => return (3,26);
         when 219 => return (3,27);
         when 220 => return (3,28);
         when 221 => return (3,29);
         when 222 => return (3,30);
         when 223 => return (3,31);
         when 224 => return (3,32);
         when 225 => return (3,33);
         when 226 => return (3,34);
         when 227 => return (3,35);
         when 228 => return (3,36);
         when 229 => return (3,37);
         when 230 => return (3,38);
         when 231 => return (3,39);
         when 232 => return (3,40);
         when 233 => return (3,41);
         when 234 => return (3,42);
         when 235 => return (3,43);
         when 236 => return (3,44);
         when 237 => return (3,45);
         when 238 => return (3,46);
         when 239 => return (3,47);
         when 240 => return (3,48);
         when 241 => return (3,49);
         when 242 => return (3,50);
         when 243 => return (3,51);
         when 244 => return (3,52);
         when 245 => return (3,53);
         when 246 => return (3,54);
         when 247 => return (3,55);
         when 248 => return (3,56);
         when 249 => return (3,57);
         when 250 => return (3,58);
         when 251 => return (3,59);
         when 252 => return (3,60);
         when 253 => return (3,61);
         when 254 => return (3,62);
         when 255 => return (3,63);
      end case;
   end;

end ext_int3;
