package body experiments
with SPARK_Mode is

   procedure exp1(s:subject_number; e: Vector_Range)
   is
   begin
      comb.pending_event_array(s)(e) := True;
      if((comb.Global_Events(s)(Event_Word_Type(e/64)).Bits and 2**(Natural(e) mod 64)) = 0) then
         comb.Global_Events(s)(Event_Word_Type(e/64)).Bits :=
           comb.Global_Events(s)(Event_Word_Type(e/64)).Bits +
           2**(Natural(e) mod 64);
      end if;
      pragma Assert(for all sub in subject_number =>
                      (for all ev in Vector_Range =>
                         (if(exp_comb.pending_event_array(sub)(ev) = True) then
                              (exp_comb.Global_Events(sub)(Event_Word_Type(ev/64)).Bits and 2**Natural(ev mod 64)) =
                                2**Natural(ev mod 64)
                          else
                            (exp_comb.Global_Events(sub)(Event_Word_Type(ev/64)).Bits and 2**Natural(ev mod 64)) = 0)));
      pragma Assert((for all sub in subject_number =>
                      (for all ev in Vector_Range =>
                         (if(exp_comb.pending_event_array(sub)(ev) = True) then
                              (exp_comb.Global_Events(sub)(Event_Word_Type(ev/64)).Bits and 2**Natural(ev mod 64)) =
                                2**Natural(ev mod 64)
                          else
                            (exp_comb.Global_Events(sub)(Event_Word_Type(ev/64)).Bits and 2**Natural(ev mod 64)) = 0))) and
                        (for all sub in subject_number =>
                           (for all ev in Vector_Range =>
                              (if(exp_comb.Global_Events(sub)(Event_Word_Type(ev/64)).Bits and 2**Natural(ev mod 64)) =
                                   2**Natural(ev mod 64) then
                                   (exp_comb.pending_event_array(sub)(ev) = True)
                               else
                                 (exp_comb.pending_event_array(sub)(ev) = False)))));
--        pragma Assert(False);
   end exp1;

   procedure exp2
   is
      Bit_in_Word: word_pos_type := 63;
      word_found: index_type := 3;
      PEA_old: PEA_type;
      Non_zero_found: Boolean := False;
   begin
      PEA_old := PEA;
      search:
      for i in reverse index_type loop
         pragma Loop_Invariant((for all j in index_type =>
                                  ((if (j > i) then PEA(j) = 0) and
                                     (if (j < i) then PEA(j) = PEA_old(j)))) and
                                   (if (Non_zero_found) then
                                      (for all k in word_pos_type =>
                                         ((if(k > Bit_in_Word) then
                                              (PEA(i) and 2**Natural(k)) = 0) and
                                            (if(k < Bit_in_Word) then
                                                 (PEA(i) and 2**Natural(k)) =
                                             (PEA_old(i) and 2**Natural(k)))))));
         Find_Highest_Bit_Set(PEA(i), Non_zero_found, Bit_in_Word);

         if Non_zero_found then
            PEA(i) := PEA(i) and (not (2**Natural(Bit_in_Word)));
            word_found := i;
            exit search;
         end if;
      end loop search;
      pragma Assert((for all j in index_type =>
                                       ((if j > word_found then
                                             PEA(j) = 0) and
                                          (if j < word_found then
                                                PEA(j) =
                                               PEA_old(j)))) and
                                         (if Non_zero_found then
                                            (for all k in word_pos_type =>
                                               ((if k >= Bit_In_Word then
                                                    (PEA(word_found) and
                                                           Word64(2**Natural(k))) = 0) and
                                                  (if k < Bit_In_Word then
                                                       (PEA(word_found) and
                                                              Word64(2**Natural(k))) =
                                                   (PEA_old(word_found) and
                                                        Word64(2**Natural(k))))))));

      pragma Assert(if(Non_zero_found) then
                      (for all i in index_type =>
                         ((if i > word_found then PEA(i) = 0) and
                              (if i < word_found then PEA(i) = PEA_old(i)))));
      pragma Assert(if PEA(word_found) /= 0 then
                       (for all i in word_pos_type =>
                          (if i > Bit_in_Word then (PEA(word_found) and 2**Natural(i))= 0)));
--        pragma Assert(False);
   end exp2;

   procedure exp3 is
      word1: Word64;
      x:Natural range 0..63;
   begin
      word1 := word1 or 2**(x mod 5);
      pragma Assert((word1 and 2**5) = 2**5);
   end exp3;

end experiments;
