# Code for Verification of Handle-Interrupt Operation #

Handle-Interrupt operation takes only one input (`irq`) which is the interrupt request line number.

The operation is declared at lines 267-269 in `./src/ext_int_full.ads`  with pre and post conditions.

### File containing combined abstract-concrete code ###

`./src/ext_int_full.adb` and `./src/ext_int_full.ads`

### Combined State ###

Defined in lines 127-155 in `./src/ext_int_full.ads`

There are two components of the state:

1. Abstract state (lines 129-140)
2. Concrete state (lines 142-154)

Abstract state components include:

1. `abs_red_tab` - The table which redirects an IRQ request to a pair (`dest_sub`, `dest_vector`). `dest_sub` is the subject which will handle the interrupt. `dest_vector` is the vector which will be received by the subject. This is a parameter of the abstract parametric machine program.
2. `pending_event_array` - This is a bit-array cotaining a bit for each of the 256 interrupt vectors. A bit corresponding to an interrupt vector is set if there is an interrupt which is pending for service.
3. `current_subject` - This array is indexed by logical CPU number. The entry at index i stores the subject number of currently running subject on logical CPU i.
4. `Abs_IO_Bitmap` - This is a per-subject bit-array containing a bit for each of the 2^16 port addresses. A bit corresponding to a port address for a subject is set if the subject is allowed to access the port address. This is a parameter of the abstract parametric machine program.
5. `Abs_IO_Ports` - This is the array which stores the contents at each port address.
6. `Abs_IDTs` - This is an array storing the IDT of each subject.
7. `Subjects_State` - Saves state (register contents) of each subject.
8. `abs_IDTR_array` - IDTR register for each subject
9. `subject_CPU_mapping` - This is a map which tells which subject is mapped to which logical CPU.
10. `is_active_array` - This is a boolean array indexed by subject number. Entry at index i is set if subject i is currently active.
11. `abs_stack` - This is the global stack.

Concrete state components include:

1. `VMCSs` - This is an array indexed by subject number. It models per-subject VMCS.
2. `VMCS_Pointer` - This models the VMCS pointer register in VMX.
3. `IOAPIC_red_tab` - This is a IOAPIC redirection table which is used to find out the destination CPU and the corresponding vector to be sent to the CPU upon an IRQ request. This is a parameter of the concrete parametric machine program. We have modelled the table in SPARK Ada as an array.
4. `Vector_Routing` - This is a redirection table which is used by Muen to redirect the interrupt corresponding to the received interrupt vector to the handler subject with the destination vector. This is a parameter of the concrete parametric machine program.
5. `Descriptors` - This is used to store the state (like content of general purpose registers) of a subject when the subject is not active.
6. `CPU_Regs` - These model the general purpose CPU registers.
7. `Conc_IO_Ports` - This array stores the content of each IO port.
8. `Global_Events` - This is an array used to store pending interrupts for each of the subject. Defined as a 4-word array per subject. Each word is of 64-bit long.
9. `IDTR_array` - This array stores the content of IDTR for each CPU.
10. `Conc_IDTS` - This array stores the IDT of each subject.
11. `Conc_Stack` - This is the global stack.
12. `Boolean_to_word_Bit_map` - This is a function which maps an interrupt vector to a word and a position in the word. This is used to update `Global_Events`

### Gluing invariant ###

Defined in lines 204-246 in `./src/ext_int_full.ads`

Gluing invariant is conjunction of following:

1. Contents of IO ports match in both abstract and concrete. (lines 204-206)
2. `Global_Events` and `pending_event_array` match modulo their appropriate interpretations. (lines 210-219)
3. State of each subject in the abstract matches with that in the concrete. (lines 223-229)
4. VMCS pointer is pointing to a subject which is the currently running subject in the abstract on each logical CPU. (lines 233-235)
5. IDTR of non-active subjects in the abstract match with IDTR in the VMCS in the concrete. (lines 239-246)

### Condition R

Defined in lines 251-261 in `./src/ext_int_full.ads`

Condition R is conjunction of following:

1. IO bitmap of the abstract matches with that in the concrete. (lines 250-255)
2. `abs_red_tab` is same as the join of the arrays `IOAPIC_red_tab` and `Vector_Routing`. (lines 259-261)

### Abstract Code ###

Lines 45-102 in `./src/ext_int_full.adb`

When an external device sends an interrupt, it turns on one of the IRQ request lines. 

When the abstract system receives such a request, 

1. it checks the `abs_red_table` and finds out the handler subject and the destination vector which has to be transferred to the subject. (line 46)

2. If the subject is currently active (i.e. `is_active_array` entry is true for the destination subject) then 

	a. If Interrupt Flag (IF flag) is clear then it sets the bit for the vector in the pending event array for the handler subject. (lines 52-56)
		
	b. If IF flag is set then it finds the highest vector for which the bit is set in the pending event array, clears it and the handler subject is interrupted. (lines 57-98)

3. If the subject is not currently active (i.e. `is_active_array` entry is false for the destination subject) then it sets the bit for the vector in the pending event array for the handler subject. (lines 99-102)

### Concrete Code ###

Lines 105-456 in `./src/ext_int_full.adb`

When an external device sends an interrupt, it turns on one of the IRQ request lines. 

When the concrete system receives such a request, 

1. The IOAPIC redirection table `IOAPIC_red_tab` is used to find out the destination CPU to which the interrupt has to be routed and the vector which has to be sent to the destination CPU. (lines 110-113)

2. VM-exit occurs because of the interrupt and is modelled in SPARK Ada in lines 115-162

3. Subject state is saved in the `Descriptors` (lines 167-200)

4. The separation kernel finds the handler subject and the destination vector which has to be sent to the handler subject from `Vector_Routing`. (lines 207-213)

5. The interrupt is saved in `Global_Events` so that the subject can handle it later. (lines 212-235)

6. Pending interrupts are checked in the `Global_Events` for the currently running subject and is injected in VMCS if found any. (lines 238-311)

7. VM-entry is modelled in lines 351-408 which includes modelling of event injection.

### Files for cases based on the paths in `./src/ext_int_full.adb` to simplify the proof ###

1. Case 1 - Destination subject is currently active and not ready to take interrupts. Files for this case - `./src/ext_int1.ads` and `./src/ext_int1.adb`.

2. Case 2 - Destination subject currently not active. Files for this case - `./src/ext_int2.ads` and `./src/ext_int2.adb`.

3. Case 3 - Destination subject currently active and ready to take interrupts. Highest pending event is less than the destination vector. Files for this case - `./src/ext_int3.ads` and `./src/ext_int3.adb`.

4. Case 4 - Destination subject currently active and ready to take interrupts. Highest pending event is equal to the destination vector. Files for this case - `./src/ext_int4.ads` and `./src/ext_int4.adb`.

5. Case 5 - Destination subject currently active and ready to take interrupts. Highest pending event is greater than the destination vector. Files for this case - `./src/ext_int5.ads` and `./src/ext_int5.adb`.

These cases are implemented as preconditions on the operations in `.ads` files and using assumptions in the `.adb` files.