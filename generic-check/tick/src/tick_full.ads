with SK; use SK;
with numbers1; use numbers1;
with SK.CPU;
with SK.Barriers;

package tick_full
with SPARK_Mode is

   -- type for ticks
   type tick_type is array (cpu_number) of SK.Word64;



   -- types for defining scheduling plans type
   type Minor_Frame_Type is record
      Group_ID : Scheduling_Group_Range;
      Barrier  : Barrier_Index_Range;
      Deadline : SK.Word64;
   end record;

   type Minor_Frame_Array is array (Minor_Frame_Range) of Minor_Frame_Type;

   type Major_Frame_Type is record
      Length       : Minor_Frame_Range;
      Minor_Frames : Minor_Frame_Array;
   end record;

   type Major_Frame_Array is array (Major_Frame_Range) of Major_Frame_Type;

   type Scheduling_Plan_Type is array (cpu_number) of Major_Frame_Array;



   --type to define perCPU Storage
   type Scheduling_Group_Array is array (Scheduling_Group_Range)
     of subject_number;

   type Storage_Type is record
      Scheduling_Groups   : Scheduling_Group_Array;
      Current_Minor_Frame : Minor_Frame_Range;
   end record;

   type Storage_Type_Array is array (cpu_number) of Storage_Type;



   -- type to define Major frame ends
   type Barrier_Config_Array is array (Barrier_Range) of Barrier_Size_Type;

   type Major_Frame_Info_Type is record
      Period         : SK.Word64;
      Barrier_Config : Barrier_Config_Array;
   end record;

   type Major_Frame_Info_Array is array (Major_Frame_Range)
     of Major_Frame_Info_Type;



   -- minor and major frame pointers types
   type minor_fp_array is array (cpu_number) of Minor_Frame_Range;

   type major_fp_array is array (cpu_number) of Major_Frame_Range;



   -- vmcs data structure
   type vmcs_array is array (subject_number) of SK.CPU.VMCStype;

   type page_table_index_type is array (cpu_number) of subject_number;

   type boolean_array is array (cpu_number) of Boolean;

   type Minor_Frame_Barriers_Array is
     array (Barrier_Index_Range) of Barriers.Sense_Barrier_Type;

   -- combined state type
   type combined_state is record

      -- common
      Scheduling_Plans : Scheduling_Plan_Type;
      Major_Frames : Major_Frame_Info_Array; -- array storing period length of each major frame

      -- abstract
      ticks : tick_type; -- equivalent to tsc, keeps on incrementing once initialized
      min_ticks : tick_type; -- ticks within a major frame. grows as the deadlines of minor frames in a major frame
      cycles : SK.Word64;
      ideal_cycles : tick_type;

      min_fp : minor_fp_array; -- minor frame pointer for each cpu
      maj_fp : Major_Frame_Range; -- major frame pointer for each cpu
      ideal_maj_fp : major_fp_array;
--        abs_first_major_start : tick_type; -- tick count at the first major frame, updated when the first major frame is repeated
--        abs_current_major_start : tick_type; -- tick count when the current major frame started
      L : SK.Word64; -- L = major_frame_ends(Major_Frame_Range)
      major_frame_ends : Major_Frame_Info_Array; -- array which stores the end tick of each major frame in a single schedule
      enabled : boolean_array;
      last : boolean_array;
      no_of_last : cpu_number;

      -- concrete
      tsc : tick_type;
      vmx_timer : tick_type;
      VMCSs : vmcs_array;
      VMCS_Pointer : page_table_index_type;

      Per_CPU_Storage : Storage_Type_Array;
      Current_Major_Frame : Major_Frame_Range;
      Current_Major_Start_Cycles : SK.Word64;

      All_Barrier : Barriers.Sense_Barrier_Type;
      Minor_Frame_Barriers : Minor_Frame_Barriers_Array;
      Barrier_Set : boolean_array;
   end record;

   comb: combined_state;

   procedure combined_tick(cpu: cpu_number) with
     Global => (In_Out => comb,
               Input => (no_of_barriers, no_of_cpus, no_of_major_frames, no_of_minor_frames)),
     Pre =>(
              inv_holds(comb)

            and
              -- boundary conditions for vmx timer and tsc
              (comb.vmx_timer(cpu) > SK.Word64'First and comb.tsc(cpu) < SK.Word64'Last)

            and
              -- boundary condition for ticks and min_ticks
              (comb.ticks(cpu) < SK.Word64'Last and comb.min_ticks(cpu) < SK.Word64'Last)

           ),
     Post => inv_holds(comb);

   function inv_holds(comb_st: combined_state) return Boolean is
     (
      -- 1 - ideal cycles always >= cycles
        (for all c in cpu_number =>
             (comb_st.ideal_cycles(c) >= comb_st.cycles))

      and

      -- 2 - maj_fp = least of ideal_maj_fp's
        (for all c in cpu_number =>
             (if (comb_st.last(c) = True) then
                  (comb_st.maj_fp = comb_st.ideal_maj_fp(c) and comb_st.cycles = comb_st.ideal_cycles(c))))

      and

      -- 3 - min_ticks < deadline of the last minor frame of the major frame
        (for all c in cpu_number =>
             (comb_st.min_ticks(c) <
                    comb_st.Scheduling_Plans(c)(comb_st.ideal_maj_fp(c)).Minor_Frames
              (comb_st.Scheduling_Plans(c)(comb_st.ideal_maj_fp(c)).Length).Deadline))

      and

      -- 4 - ticks < L
        (for all c in cpu_number =>
             (comb_st.ticks(c) < comb_st.L))

      and

      -- 5 - min_ticks <= ticks
        (for all c in cpu_number =>
             (comb_st.min_ticks(c) <= comb_st.ticks(c)))

      and

      -- 6 - min_fp < number of minor frames in the major frame
        (for all c in cpu_number =>
             (comb_st.min_fp(c) <=
                    comb_st.Scheduling_Plans(c)(comb_st.ideal_maj_fp(c)).Length))

      and

      -- 7 - relationship between min_ticks, ticks and ideal_maj_fp
        (for all c in cpu_number =>
             (if(comb_st.ideal_maj_fp(c) /= 0) then
                  (comb_st.ticks(c) =
                   (comb_st.major_frame_ends(comb_st.ideal_maj_fp(c) - 1).Period +
                      comb_st.min_ticks(c)))
              else
                (comb_st.ticks(c) = comb_st.min_ticks(c))))

      and

      -- 8 - deadlines of minor frames within a major frame are all > 0
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min in 1..comb_st.Scheduling_Plans(c)(maj).Length =>
                     (comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline > 0))))

      and

      -- 9 - deadline of minor frames in a major frame are strictly increasing
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min in 1..comb_st.Scheduling_Plans(c)(maj).Length =>
                     (if(min/=1) then
                          (comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline >
                                 comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min-1).Deadline)))))

      and

      -- 10 - every value in major frame ends and major_frames are > 0
        (for all maj in Major_Frame_Range =>
             ((comb_st.major_frame_ends(maj).Period > 0) and
                  (comb_st.Major_Frames(maj).Period > 0)))

      and

      -- 11 - major_frame_ends values are strictly increasing
        (for all maj in Major_Frame_Range =>
             (if (maj/=Major_Frame_Range'First) then
                (comb_st.major_frame_ends(maj).Period >
                     comb_st.major_frame_ends(maj - 1).Period)))

      and

      -- 12 - relationship between major_frames and scheduling_plans
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (comb_st.Major_Frames(maj).Period =
                         comb_st.Scheduling_Plans(c)
                   (maj).Minor_Frames(comb_st.Scheduling_Plans(c)(maj).Length).Deadline
                  )))

      and

      -- 13A - relationship between major_frame_ends and major_frames (a)
        (for all maj in Major_Frame_Range =>
             (if (maj /= 0) then
                (comb_st.Major_Frames(maj).Period =
                 (comb_st.major_frame_ends(maj).Period -
                    comb_st.major_frame_ends(maj - 1).Period))))

      and

      -- 13B - relationship between major_frame_ends and major_frames (b)
        (comb_st.Major_Frames(Major_Frame_Range'First).Period =
             comb_st.major_frame_ends(Major_Frame_Range'First).Period)

      and

      -- 14 - L = major_frame_ends(Major_Frame_Range'Last)
        (comb_st.L = comb_st.major_frame_ends(Major_Frame_Range'Last).Period)

      and

      -- 15 - ticks should be between two major frame end values
        (for all c in cpu_number =>
             (if(comb_st.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                  ((comb_st.ticks(c) <
                      comb_st.major_frame_ends(comb_st.ideal_maj_fp(c)).Period)
                   and
                     (comb_st.ticks(c) >=
                          comb_st.major_frame_ends(comb_st.ideal_maj_fp(c) - 1).Period))
              else
                ((comb_st.ticks(c) <
                    comb_st.major_frame_ends(comb_st.ideal_maj_fp(c)).Period)
                 and
                   (comb_st.ticks(c) >= 0))))

      and

      -- 16 - min_ticks should be between two minor frame deadlines
        (for all c in cpu_number =>
             (if (comb_st.min_fp(c) /= Minor_Frame_Range'First) then
                  ((comb_st.min_ticks(c) <
                      comb_st.Scheduling_Plans(c)
                    (comb_st.ideal_maj_fp(c)).Minor_Frames(comb_st.min_fp(c)).Deadline)
                   and
                     (comb_st.min_ticks(c) >=
                          comb_st.Scheduling_Plans(c)
                      (comb_st.ideal_maj_fp(c)).Minor_Frames(comb_st.min_fp(c) - 1).Deadline))
              else
                ((comb_st.min_ticks(c) <
                    comb_st.Scheduling_Plans(c)
                  (comb_st.ideal_maj_fp(c)).Minor_Frames(comb_st.min_fp(c)).Deadline)
                 and
                   (comb_st.min_ticks(c) >= 0))
             ))

      and

      -- 17A - monotonicity of deadlines of minor frames within a major frame (a)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(min1 < min2) then
                             (comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                    comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                        )))))

      and

      -- 17B - monotonicity of deadlines of minor frames within a major frame (b)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(min1 = min2) then
                             (comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                    comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                        )))))

      and

      -- 17C - monotonicity of deadlines of minor frames within a major frame (c)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                             (min1 < min2)
                        )))))

      and

      -- 17D - monotonicity of deadlines of minor frames within a major frame (d)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                comb_st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                             (min1 = min2)
                        )))))

      and

      -- 18A - monotonicity of values in major_frame_ends
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(maj1 < maj2) then
                     (comb_st.major_frame_ends(maj1).Period < comb_st.major_frame_ends(maj2).Period))))

      and

      --
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(maj1 = maj2) then
                     (comb_st.major_frame_ends(maj1).Period = comb_st.major_frame_ends(maj2).Period))))

      and

      --
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(comb_st.major_frame_ends(maj1).Period < comb_st.major_frame_ends(maj2).Period) then
                     (maj1 < maj2))))

      and

      --
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(comb_st.major_frame_ends(maj1).Period = comb_st.major_frame_ends(maj2).Period) then
                     (maj1 = maj2))))

      and

      -- 19 - all the CPUs can never be in barrier
        (for some c in cpu_number =>
             (comb_st.ideal_cycles(c) = comb_st.cycles and
                    comb_st.ideal_maj_fp(c) = comb_st.maj_fp))

      and

      -- 20 - if cycles = ideal_cycles then ideal_maj_fp >= maj_fp
        (for all c in cpu_number =>
             (if(comb_st.ideal_cycles(c) = comb_st.cycles) then
                  (comb_st.ideal_maj_fp(c) >= comb_st.maj_fp)))

      and

      -- 21A - enabled <=> maj_fp = ideal_maj_fp and cycles = ideal_cycles
        (for all c in cpu_number =>
             (if(comb_st.ideal_cycles(c) = comb_st.cycles and
                     comb_st.ideal_maj_fp(c) = comb_st.maj_fp) then
                  (comb_st.enabled(c) = True)))

      and

      -- 21B -
        (for all c in cpu_number =>
             (if(comb_st.enabled(c) = True) then
                  (comb_st.ideal_cycles(c) = comb_st.cycles and
                         comb_st.ideal_maj_fp(c) = comb_st.maj_fp)
              else
                (comb_st.ideal_cycles(c) /= comb_st.cycles or
                     comb_st.maj_fp /= comb_st.ideal_maj_fp(c))))

      and


      -- concrete state invariants

      -- 1 - least of the tsc's will be in the current major frame
        (for all c in cpu_number =>
             (if(comb_st.last(c) = True) then
                  (comb_st.tsc(c) <=
                   (comb_st.Current_Major_Start_Cycles +
                          comb_st.Major_Frames(comb_st.Current_Major_Frame).Period))))

      and

      -- 2 - all the CPUs can never be in the barrier
        (for some c in cpu_number =>
             (comb_st.Barrier_Set(c) = False))

      and


      -- gluing invariants

      -- 1 - relationship between tsc, cmsc, ticks, min_ticks
        (for all c in cpu_number =>
             (if(comb_st.ideal_cycles(c) = comb_st.cycles) then
                  (if(comb_st.ideal_maj_fp(c) /= Major_Frame_Range'First and
                        comb_st.maj_fp /= Major_Frame_Range'First) then
                       ((comb_st.tsc(c) - comb_st.Current_Major_Start_Cycles) =
                        (comb_st.major_frame_ends(comb_st.ideal_maj_fp(c) - 1).Period -
                           comb_st.major_frame_ends(comb_st.maj_fp - 1).Period +
                           comb_st.min_ticks(c)))
                   else (if(comb_st.maj_fp = Major_Frame_Range'First and
                              comb_st.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                           ((comb_st.tsc(c) - comb_st.Current_Major_Start_Cycles) =
                            (comb_st.major_frame_ends(comb_st.ideal_maj_fp(c) - 1).Period +
                                 comb_st.min_ticks(c)))
                         else (if(comb_st.maj_fp = Major_Frame_Range'First and
                                    comb_st.ideal_maj_fp(c) = Major_Frame_Range'First) then
                                 ((comb_st.tsc(c) - comb_st.Current_Major_Start_Cycles) =
                                  (comb_st.min_ticks(c)))
                              )))
              else
                (if(comb_st.maj_fp /= Major_Frame_Range'First) then
                     ((comb_st.tsc(c) - comb_st.Current_Major_Start_Cycles) =
                      (comb_st.major_frame_ends(Major_Frame_Range'Last).Period -
                         comb_st.major_frame_ends(comb_st.maj_fp - 1).Period +
                       (comb_st.ideal_cycles(c) - comb_st.cycles) * comb_st.L +
                         comb_st.ticks(c)))
                 else (if(comb_st.maj_fp = Major_Frame_Range'First) then
                         ((comb_st.tsc(c) - comb_st.Current_Major_Start_Cycles) =
                          (comb_st.major_frame_ends(Major_Frame_Range'Last).Period +
                           (comb_st.ideal_cycles(c) - comb_st.cycles) * comb_st.L +
                             comb_st.ticks(c)))
                      ))))

      and

      -- 2 - relationship between maj_fp and Current_Major_Frame
        (comb_st.maj_fp = comb_st.Current_Major_Frame)

      and

      -- 3 - relationship between min_fp and Current_Minor_Frame
        (for all c in cpu_number =>
             (if(comb_st.ideal_maj_fp(c) = comb_st.maj_fp and
                     comb_st.ideal_cycles(c) = comb_st.cycles) then
                  (comb_st.min_fp(c) = comb_st.Per_CPU_Storage(c).Current_Minor_Frame)))

      and

      -- 4 - relationship between vmx and min_ticks
        (for all c in cpu_number =>
             (if(comb_st.ideal_maj_fp(c) = comb_st.maj_fp and
                     comb_st.ideal_cycles(c) = comb_st.cycles) then
                  (comb_st.vmx_timer(c) = comb_st.Scheduling_Plans(c)
                   (comb_st.maj_fp).Minor_Frames(comb_st.min_fp(c)).Deadline -
                     comb_st.min_ticks(c))))

      and

      -- 5A - relationship between barrier and enabled
        (for all c in cpu_number =>
             (if(comb_st.enabled(c) = False) then
                  (comb_st.Barrier_Set(c) = True)
              else (comb_st.Barrier_Set(c) = False)))

      and

      -- 5B
        (for all c in cpu_number =>
             (if(comb_st.Barrier_Set(c) = True) then
                  (comb_st.enabled(c) = False)
              else (comb_st.enabled(c) = True)))
     );

   procedure lemma_major_end(c: cpu_number) with
     Ghost,
     Import => True,
     Pre => (((comb.min_ticks(c) + 1) = comb.Scheduling_Plans(c)
             (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c)).Deadline) and
               (comb.min_fp(c) = comb.Scheduling_Plans(c)(comb.ideal_maj_fp(c)).Length)),
     Post => ((comb.ticks(c) + 1) = comb.major_frame_ends(comb.ideal_maj_fp(c)).Period);

   procedure lemma_cycles_maj_fp(c: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.ideal_maj_fp(c) < comb.maj_fp),
     Post => (comb.ideal_cycles(c) /= comb.cycles);

   procedure lemma_barrier1(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.All_Barrier.Wait_Count = comb.All_Barrier.Size - 1),
     Post => (for all c in cpu_number =>
                   (if(c /= p) then
                      (comb.ideal_cycles(c) /= comb.cycles or
                               comb.ideal_maj_fp(c) /= comb.maj_fp)));

   procedure lemma_barrier2(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (for all c in cpu_number =>
               (if(c /= p) then
                    (comb.ideal_cycles(c) /= comb.cycles or
                           comb.ideal_maj_fp(c) /= comb.maj_fp))),
       Post => (comb.All_Barrier.Wait_Count = comb.All_Barrier.Size - 1);

   procedure lemma_barrier3(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.enabled(p) = True and comb.last(p) = True and
                 comb.no_of_last = 1 and (for all c in cpu_number =>
                                              (if(c /= p) then
                                                 (comb.enabled(c) = False)))),
     Post => (comb.All_Barrier.Wait_Count = comb.All_Barrier.Size - 1);

   -- whenever this CPU is not in barrier,
   -- those behind it are also not in barrier,
   -- hence waitcount is less than max-1
   procedure lemma_barrier4(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.enabled(p) = True and comb.last(p) /= True),
     Post => (comb.All_Barrier.Wait_Count < comb.All_Barrier.Size - 1);

   -- whenever this CPU is not last, and not in barrier,
   -- those behind it are also not in barrier
   procedure lemma_barrier5(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.enabled(p) = True and comb.last(p) /= True),
     Post => (for some c in cpu_number =>
                c /= p and comb.enabled(c) = True);

   -- whenever this CPU is last and not in barrier,
   -- but another CPU is in parallel so that will be last after this transition
   procedure lemma_barrier6(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.enabled(p) = True and comb.last(p) = True and
                 comb.no_of_last > 1),
     Post => (for some c in cpu_number =>
                (c /= p) and comb.enabled(c) = True);

   -- whenever this CPU is not last and not in barrier,
   -- but another CPU is in parallel so that will be last after this transition
   -- hence, wait-count /= size
   procedure lemma_barrier7(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.enabled(p) = True and comb.last(p) = True and
                 comb.no_of_last > 1),
     Post => (comb.All_Barrier.Wait_Count < comb.All_Barrier.Size - 1);

   -- whenever this CPU is last and not in barrier,
   -- and this CPU is one tick away from the major frame end
   -- then others are already disabled and in the barrier
   procedure lemma_barrier8(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.enabled(p) = True and comb.last(p) = True and
                 comb.no_of_last = 1),-- and
            -- write the deadline reaching condition and last minor frame),
     Post => (for all c in cpu_number =>
                  (if(c /= p) then
                       comb.enabled(c) = False));

   -- whenever this CPU is last and not in the barrier,
   -- and this CPU is one tick away from the major frame end
   -- then others are already in the barrier
   -- and hence wait count = barrier size - 1
   procedure lemma_barrier9(p: cpu_number) with
     Ghost,
     Import => True,
     Pre => (comb.enabled(p) = True and comb.last(p) = True and
                 comb.no_of_last = 1),
     Post => (comb.All_Barrier.Wait_Count =
                comb.All_Barrier.Size - 1);

end tick_full;
