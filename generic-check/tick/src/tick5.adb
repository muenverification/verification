package body tick5
with SPARK_Mode is

   procedure combined_tick(cpu: cpu_number) is
      all_crossed: Boolean := True;
      next_maj: Major_Frame_Range;
      Current_Major_ID : constant Major_Frame_Range
        := comb.Current_Major_Frame;
      Current_Minor_ID : constant Minor_Frame_Range
        := comb.per_cpu_storage(cpu).Current_Minor_Frame;

      Current_Major_Frame_Start : constant SK.Word64
        := comb.Current_Major_Start_Cycles;
      Now : SK.Word64;
      Cycles : SK.Word64;
      deadline : SK.Word64;

      Next_Minor_ID   : Minor_Frame_Range;
      temp : Major_Frame_Range;
   begin
      -- unwrapping
      pragma Assume(inv_holds(comb));
      pragma Assume(for all c in cpu_number =>
                      (if(comb.last(c) = True) then
                          (for all c1 in cpu_number =>
                               (comb.ideal_cycles(c1) >= comb.ideal_cycles(c) and
                                  (if(comb.ideal_cycles(c1) = comb.ideal_cycles(c)) then
                                     (comb.ideal_maj_fp(c1) >= comb.ideal_maj_fp(c))) and
                                    (if(comb.ideal_maj_fp(c1) = comb.ideal_maj_fp(c)) then
                                        (comb.ticks(c1) >= comb.ticks(c)))
                               ))));
      pragma Assume(for all c in cpu_number =>
                      (if(comb.last(c) = True) then
                         (for all c1 in cpu_number =>
                              (comb.tsc(c) <= comb.tsc(c1)))));
--        pragma Assert(False);
      -- checking all the invariants are satisfied in the beginning
      -- 1 - ideal cycles always >= cycles
      pragma Assert(comb.ideal_cycles(cpu) >= comb.cycles);

      -- 2 - maj_fp = least of ideal_maj_fp's
--        pragma Assert((for all c in cpu_number =>
--                              ((comb.ideal_maj_fp(comb.last) <=
--                                 comb.ideal_maj_fp(c)))) and
--                           (comb.maj_fp = comb.ideal_maj_fp(comb.last)));

      -- 3 - min_ticks < deadline of the last minor frame of the major frame
      pragma Assert(for all c in cpu_number =>
                      (comb.min_ticks(c) <
                           comb.Scheduling_Plans(c)(comb.ideal_maj_fp(c)).Minor_Frames
                       (comb.Scheduling_Plans(c)(comb.ideal_maj_fp(c)).Length).Deadline));

      -- 4 - ticks < L
      pragma Assert(for all c in cpu_number =>
                      (comb.ticks(c) < comb.L));

      -- 5 - min_ticks <= ticks
      pragma Assert(for all c in cpu_number =>
                      (comb.min_ticks(c) <= comb.ticks(c)));

      -- 6 - min_fp < number of minor frames in the major frame
      pragma Assert(for all c in cpu_number =>
                      (comb.min_fp(c) <=
                           comb.Scheduling_Plans(c)(comb.ideal_maj_fp(c)).Length));

      -- 7 - relationship between min_ticks, ticks and ideal_maj_fp
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_maj_fp(c) /= 0) then
                         (comb.ticks(c) =
                          (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period +
                                   comb.min_ticks(c)))
                       else
                         (comb.ticks(c) = comb.min_ticks(c))));

      -- 8 - deadlines of minor frames within a major frame are all > 0
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min in 1..comb.Scheduling_Plans(c)(maj).Length =>
                              (comb.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline > 0))));

      -- 9 - deadline of minor frames in a major frame are strictly increasing
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min in 1..comb.Scheduling_Plans(c)(maj).Length =>
                              (if(min/=1) then
                                 (comb.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline >
                                      comb.Scheduling_Plans(c)(maj).Minor_Frames(min-1).Deadline)))));

      -- 10 - every value in major frame ends and major_frames are > 0
      pragma Assert(for all maj in Major_Frame_Range =>
                      ((comb.major_frame_ends(maj).Period > 0) and
                         (comb.Major_Frames(maj).Period > 0)));

      -- 11 - major_frame_ends values are strictly increasing
      pragma Assert(for all maj in Major_Frame_Range =>
                      (if (maj/=Major_Frame_Range'First) then
                           (comb.major_frame_ends(maj).Period >
                                  comb.major_frame_ends(maj - 1).Period)));

      -- 12 - relationship between major_frames and scheduling_plans
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (comb.Major_Frames(maj).Period =
                              comb.Scheduling_Plans(c)
                          (maj).Minor_Frames(comb.Scheduling_Plans(c)(maj).Length).Deadline
                         )));

      -- 13A - relationship between major_frame_ends and major_frames (a)
      pragma Assert(for all maj in Major_Frame_Range =>
                      (if (maj /= 0) then
                           (comb.Major_Frames(maj).Period =
                            (comb.major_frame_ends(maj).Period -
                                 comb.major_frame_ends(maj - 1).Period))));

      -- 13B - relationship between major_frame_ends and major_frames (b)
      pragma Assert(comb.Major_Frames(Major_Frame_Range'First).Period =
                      comb.major_frame_ends(Major_Frame_Range'First).Period);

      -- 14 - L = major_frame_ends(Major_Frame_Range'Last)
      pragma Assert(comb.L = comb.major_frame_ends(Major_Frame_Range'Last).Period);

      -- 15 - ticks should be between two major frame end values
      pragma Assert(for all c in cpu_number =>
             (if(comb.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                  ((comb.ticks(c) <
                       comb.major_frame_ends(comb.ideal_maj_fp(c)).Period)
                   and
                     (comb.ticks(c) >=
                          comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period))
                else
                  ((comb.ticks(c) <
                       comb.major_frame_ends(comb.ideal_maj_fp(c)).Period)
                   and
                   (comb.ticks(c) >= 0))));

      -- 16 - min_ticks should be between two minor frame deadlines
      pragma Assert(for all c in cpu_number =>
                      (if (comb.min_fp(c) /= Minor_Frame_Range'First) then
                         ((comb.min_ticks(c) <
                              comb.Scheduling_Plans(c)
                          (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c)).Deadline)
                          and
                            (comb.min_ticks(c) >=
                                 comb.Scheduling_Plans(c)
                             (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c) - 1).Deadline))
                       else
                         ((comb.min_ticks(c) <
                              comb.Scheduling_Plans(c)
                          (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c)).Deadline)
                          and
                            (comb.min_ticks(c) >= 0))
                      ));

      -- 17A - monotonicity of deadlines of minor frames within a major frame (a)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(min1 < min2) then
                                      (comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                           comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                                   )))));

      -- 17B - monotonicity of deadlines of minor frames within a major frame (b)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(min1 = min2) then
                                      (comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                           comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                                   )))));

      -- 17C - monotonicity of deadlines of minor frames within a major frame (c)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                      comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                                      (min1 < min2)
                                   )))));

      -- 17D - monotonicity of deadlines of minor frames within a major frame (d)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                      comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                                      (min1 = min2)
                                   )))));

      -- 18 - least of the tsc's will be in the current major frame
--        pragma Assert((for all c in cpu_number =>
--                              (comb.tsc(comb.last) <= comb.tsc(c))) and
--                           (comb.tsc(comb.last) <= (comb.Current_Major_Start_Cycles +
--                                comb.Major_Frames(comb.Current_Major_Frame).Period)));

      -- 19 - relationship between tsc, cmsc, ticks, min_ticks
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_cycles(c) = comb.cycles) then
                         (if(comb.ideal_maj_fp(c) /= Major_Frame_Range'First and
                              comb.maj_fp /= Major_Frame_Range'First) then
                            ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                             (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period +
                                    comb.min_ticks(c)))
                          else (if(comb.maj_fp = Major_Frame_Range'First and
                              comb.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                                ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                                 (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period +
                                      comb.min_ticks(c)))
                            else (if(comb.maj_fp = Major_Frame_Range'First and
                                comb.ideal_maj_fp(c) = Major_Frame_Range'First) then
                                ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                                 (comb.min_ticks(c)))
                             )))
                       else
                         (if(comb.maj_fp /= Major_Frame_Range'First) then
                              ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                               (comb.major_frame_ends(Major_Frame_Range'Last).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period +
                                    (comb.ideal_cycles(c) - comb.cycles) * comb.L +
                                      comb.ticks(c)))
                          else (if(comb.maj_fp = Major_Frame_Range'First) then
                              ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                               (comb.major_frame_ends(Major_Frame_Range'Last).Period +
                                  (comb.ideal_cycles(c) - comb.cycles) * comb.L +
                                    comb.ticks(c)))
                           ))));

      -- 20 - relationship between maj_fp and Current_Major_Frame
      pragma Assert(comb.maj_fp = comb.Current_Major_Frame);

      -- 21 - relationship between min_fp and Current_Minor_Frame
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_maj_fp(c) = comb.maj_fp and
                         comb.ideal_cycles(c) = comb.cycles) then
                         (comb.min_fp(c) = comb.Per_CPU_Storage(c).Current_Minor_Frame)));

      -- 22 - relationship between vmx and min_ticks
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_maj_fp(c) = comb.maj_fp and
                         comb.ideal_cycles(c) = comb.cycles) then
                         (comb.vmx_timer(c) = comb.Scheduling_Plans(c)
                          (comb.maj_fp).Minor_Frames(comb.min_fp(c)).Deadline -
                              comb.min_ticks(c))));

      lemma_major_end(cpu);
      lemma_barrier8(cpu);
      lemma_barrier9(cpu);

      -- **** proof of gluing invariant 1 ****
      pragma Assert(comb.maj_fp = comb.ideal_maj_fp(cpu));
      pragma Assert(comb.ideal_cycles(cpu) = comb.cycles);
      pragma Assert(comb.tsc(cpu) - comb.Current_Major_Start_Cycles =
                      comb.min_ticks(cpu));
      pragma Assert(comb.tsc(cpu) + 1 - comb.Current_Major_Start_Cycles -
                      comb.Major_Frames(comb.maj_fp).Period = 0);
      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period + comb.min_ticks(c))
                                ))));
      pragma Assert(if(comb.maj_fp /= Major_Frame_Range'First) then
                      (comb.Major_Frames(comb.maj_fp).Period =
                           comb.major_frame_ends(comb.maj_fp).Period -
                           comb.major_frame_ends(comb.maj_fp - 1).Period));
      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                        comb.Major_Frames(comb.maj_fp).Period =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                    (comb.major_frame_ends(comb.maj_fp - 1).Period +
                                         comb.Major_Frames(comb.maj_fp).Period) +
                                      comb.min_ticks(c))
                                ))));
      pragma Assert(if(comb.maj_fp /= Major_Frame_Range'First) then
                      (comb.Major_Frames(comb.maj_fp).Period =
                           comb.major_frame_ends(comb.maj_fp).Period -
                           comb.major_frame_ends(comb.maj_fp - 1).Period));
      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.maj_fp /= 0 and comb.ideal_maj_fp(c) /=0) then
                                (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                 (comb.major_frame_ends(comb.maj_fp - 1).Period +
                                      comb.Major_Frames(comb.maj_fp).Period) +
                                   comb.min_ticks(c) =
                                       comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                   comb.major_frame_ends(comb.maj_fp).Period +
                                       comb.min_ticks(c)))));
      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                        comb.Major_Frames(comb.maj_fp).Period =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp).Period +
                                        comb.min_ticks(c))
                                ))));

      -- abstract
      -- update of ideal components

      comb.ticks(cpu) := comb.ticks(cpu) + 1;
      comb.min_ticks(cpu) := comb.min_ticks(cpu) + 1;

      if(comb.min_ticks(cpu) >= -- if <= then can be proved for middle of minor frame case
           comb.Scheduling_Plans(cpu)(comb.ideal_maj_fp(cpu)).Minor_Frames(comb.min_fp(cpu)).Deadline)
      then
         if(comb.min_fp(cpu) /= comb.Scheduling_Plans(cpu)(comb.ideal_maj_fp(cpu)).Length)
         then
            comb.min_fp(cpu) := comb.min_fp(cpu) + 1;
         else
            comb.min_fp(cpu) := Minor_Frame_Range'First;
            comb.min_ticks(cpu) := 0;
            if(comb.ideal_maj_fp(cpu) /= Major_Frame_Range'Last)
            then
               comb.ideal_maj_fp(cpu) := comb.ideal_maj_fp(cpu) + 1;
            else
               comb.ideal_maj_fp(cpu) := Major_Frame_Range'First;
               comb.ticks(cpu) := 0;
               comb.ideal_cycles(cpu) := comb.ideal_cycles(cpu) + 1;
            end if;
         end if;
      end if;
      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                        comb.Major_Frames(comb.maj_fp).Period =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp).Period +
                                        comb.min_ticks(c))
                                ))));

      pragma Assert(comb.ideal_maj_fp(cpu) /= comb.maj_fp or
                      comb.ideal_cycles(cpu) /= comb.cycles);
      pragma Assert(comb.last(cpu) = True and comb.no_of_last = 1);
      if(comb.ideal_maj_fp(cpu) /= comb.maj_fp or
         comb.ideal_cycles(cpu) /= comb.cycles) then

         if(comb.last(cpu) = True) then
            if(comb.no_of_last = 1) then
               if(comb.maj_fp /= Major_Frame_Range'Last) then
                  next_maj := comb.maj_fp + 1; -- think about assigning ideal value
               else
                  next_maj := Major_Frame_Range'First;
                  comb.cycles := comb.cycles + 1;
               end if;

                     pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                        comb.Major_Frames(comb.maj_fp).Period =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp).Period +
                                        comb.min_ticks(c))
                                ))));
               pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                        comb.Major_Frames((comb.maj_fp + 1) - 1).Period =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends((comb.maj_fp + 1) - 1).Period +
                                        comb.min_ticks(c))
                                ))));
               pragma Assert(if(
                             (for all c in cpu_number =>
                                (if(c /= cpu) then
                                    (if(comb.ideal_cycles(c) = comb.cycles) then
                                         (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                            (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                                 comb.Major_Frames((comb.maj_fp + 1) - 1).Period =
                                               comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                                 comb.major_frame_ends((comb.maj_fp + 1) - 1).Period +
                                               comb.min_ticks(c))
                                         )))) and (temp = comb.maj_fp + 1)) then
                               (for all c in cpu_number =>
                                  (if(c /= cpu) then
                                       (if(comb.ideal_cycles(c) = comb.cycles) then
                                            (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                               (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                                    comb.Major_Frames(temp - 1).Period =
                                                  comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                                  comb.major_frame_ends(temp - 1).Period +
                                                    comb.min_ticks(c))
                                            )))));
               temp := comb.maj_fp + 1;
               pragma Assert(for all c in cpu_number =>
                                  (if(c /= cpu) then
                                       (if(comb.ideal_cycles(c) = comb.cycles) then
                                            (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                               (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                                    comb.Major_Frames(temp - 1).Period =
                                                  comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                                  comb.major_frame_ends(temp - 1).Period +
                                                    comb.min_ticks(c))
                                            ))));

               comb.maj_fp := next_maj;
               pragma Assert(next_maj /= Major_Frame_Range'First);
               pragma Assert(for all c in cpu_number =>
                                  (if(c /= cpu) then
                                       (if(comb.ideal_cycles(c) = comb.cycles) then
                                            (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                               (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                                    comb.Major_Frames(temp - 1).Period =
                                                  comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                                  comb.major_frame_ends(temp - 1).Period +
                                                    comb.min_ticks(c))
                                            ))));

               pragma Assert(comb.maj_fp = temp);

               pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                        comb.Major_Frames(comb.maj_fp - 1).Period =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp-1).Period +
                                        comb.min_ticks(c))
                                ))));
               -- update last and no of last
               for c in cpu_number
               loop
                  if (c /= cpu) then
                     if(((comb.ideal_maj_fp(c) = comb.ideal_maj_fp(cpu)) and
                           (comb.ideal_cycles(c) = comb.ideal_cycles(cpu))) and then
                            (comb.ticks(c) = comb.ticks(cpu))) then
                        comb.last(c) := True;
                        comb.no_of_last := comb.no_of_last + 1;
                     end if;
                  end if;
               end loop;
            else
               comb.last(cpu) := False;
               comb.no_of_last := comb.no_of_last - 1;
            end if;
         end if;
      end if;

      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (if(comb.ideal_maj_fp(c) /= 0 and comb.maj_fp /= 0) then
                                   (comb.tsc(c) - comb.Current_Major_Start_Cycles -
                                        comb.Major_Frames(comb.Current_Major_Frame).Period =
                                        comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period +
                                        comb.min_ticks(c))
                                ))));

      pragma Assert(comb.ideal_maj_fp(cpu) = comb.maj_fp);
      pragma Assert(comb.maj_fp /= Major_Frame_Range'First);
      pragma Assert(comb.min_ticks(cpu) = 0);
      pragma Assert(comb.tsc(cpu) + 1 - comb.Current_Major_Start_Cycles -
                      comb.Major_Frames(comb.maj_fp - 1).Period = 0);

      -- concrete
      comb.tsc(cpu) := comb.tsc(cpu) + 1;

      if(comb.vmx_timer(cpu) /= 0) then
         comb.vmx_timer(cpu) := comb.vmx_timer(cpu) - 1;
      end if;

      if(comb.vmx_timer(cpu) = 0) then

         if Current_Minor_ID < comb.Scheduling_Plans(cpu)(comb.Current_Major_Frame).Length then

            --  Sync on minor frame barrier if necessary and switch to next
            --  minor frame in current major frame.

            declare
               Current_Barrier : constant Barrier_Index_Range
                 := comb.Scheduling_Plans(cpu)(Current_Major_ID).Minor_Frames(Current_Minor_ID).Barrier;
               --                    Skp.Scheduling.Get_Barrier
               --                    (CPU_ID   => CPU_Global.CPU_ID,
               --                     Major_ID => Current_Major_ID,
               --                     Minor_ID => Current_Minor_ID);
            begin
               pragma Assume(Current_Barrier /= No_Barrier);
               if Current_Barrier /= No_Barrier then
                  --                   MP.Wait_On_Minor_Frame_Barrier (Index => Current_Barrier);
                  ------------------------------------
                  ------------------------------------
                  ------ put barrier code here.-------
                  ------------------------------------
                  ------------------------------------
                  null;
               end if;
            end;

            Next_Minor_ID := Current_Minor_ID + 1;
            comb.Per_CPU_Storage(cpu).Current_Minor_Frame := Next_Minor_ID;

            Now := comb.tsc(cpu);
            Deadline := comb.Current_Major_Start_Cycles +
              comb.Scheduling_Plans(cpu)(comb.Current_Major_Frame).Minor_Frames
              (comb.Per_CPU_Storage(cpu).Current_Minor_Frame).deadline;

            if Deadline > Now then
               Cycles := Deadline - Now;
            else
               Cycles := 0;
            end if;

            pragma Assume(Cycles < SK.Word64(2**32));
            comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_VMX_PREEMPT_TIMER := SK.Word32(Cycles);
            comb.vmx_timer(cpu) := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_VMX_PREEMPT_TIMER);
         else

            --  Switch to first minor frame in next major frame.

            Next_Minor_ID := Minor_Frame_Range'First;

         --             MP.Wait_For_All;

            declare
               Barrier_Size  : SK.Byte;
               Barrier_Sense : Boolean;
               CPU_Sense     : Boolean;
            begin
               Barrier_Size  := comb.All_Barrier.Size;
               Barrier_Sense := comb.All_Barrier.Sense;
               CPU_Sense     := not Barrier_Sense;

               --                 Get_And_Increment (Sense_Barrier => comb.All_Barrier,
               --                                    Count         => Count);
               if(comb.Barrier_Set(cpu) = False) then
                  comb.All_Barrier.Wait_Count := comb.All_Barrier.Wait_Count + 1;
               end if;

               comb.Barrier_Set(cpu) := True;

               if comb.All_Barrier.Wait_Count = Barrier_Size then
                  comb.All_Barrier.Wait_Count := 0;
                  comb.All_Barrier.Sense      := CPU_Sense;
                  for c in cpu_number
                  loop
                     comb.Barrier_Set(c) := False;
                  end loop;

                  --                 else
                  --                    loop
                  --                       Barrier_Sense := comb.All_Barrier.Sense;
--
--                       exit when Barrier_Sense = CPU_Sense;
--                       CPU.Pause;
--                    end loop;

                  declare

                     --  Next major frame ID used to access the volatile New_Major
                     --  variable. Do not move the declaration outside of this scope
                     --  as it is only updated on the BSP. All other CPUs must get
                     --  the value from CPU_Global.

                     Next_Major_ID    : Major_Frame_Range;
                     Next_Major_Start : SK.Word64;
                  begin
                     --  Tau0_Interface.Get_Major_Frame (ID => Next_Major_ID);
                     --  Next_Major_ID := Current_Major_ID + 1;
                     if Current_Major_ID = Major_Frame_Range'Last then
                        Next_Major_ID := Major_Frame_Range'First;
                     else
                        Next_Major_ID := Current_Major_ID + 1;
                     end if;

                     --  Increment major frame start time by period of major frame
                     --  that just ended.

                     Next_Major_Start := comb.Current_Major_Start_Cycles
                       + comb.Major_Frames (Current_Major_ID).Period;

                     comb.Current_Major_Frame := Next_Major_ID;
                     --                       CPU_Global.Set_Current_Major_Start_Cycles
                     --                         (TSC_Value => Next_Major_Start);

                     comb.Current_Major_Start_Cycles := Next_Major_Start;

                     if Current_Major_ID /= Next_Major_ID then
                        for I in comb.Major_Frames(Next_Major_ID).Barrier_Config'Range loop
                           pragma Loop_Invariant(True);
                           -- add loop invariant
                           Barriers.Initialize (Barrier => comb.Minor_Frame_Barriers (I),
                                                Size    => SK.Byte (comb.Major_Frames
                                                  (Next_Major_ID).Barrier_Config (I)));
                        end loop;
                        --                            (Config => Skp.Scheduling.Major_Frames
                        --                               (Next_Major_ID).Barrier_Config);
                     end if;
                  end;
               pragma Assume(for all c in cpu_number =>
                               (comb.tsc(c) < (comb.Current_Major_Start_Cycles +
                                    comb.Major_Frames(comb.maj_fp).Period)));
                  pragma Assume(for all c in cpu_number =>
                                  (comb.tsc(c) >= comb.Current_Major_Start_Cycles));
                  pragma Assert(for all c in cpu_number =>
                                  (comb.tsc(c) - comb.Current_Major_Start_Cycles >= 0));
                  pragma Assert(Next_Minor_ID = 1);

                  pragma Assert(for all c in cpu_number =>
                                  (for all maj in Major_Frame_Range =>
                                     (for all min1 in Minor_Frame_Range =>
                                          (for all min2 in Minor_Frame_Range =>
                                               (if(min1 < min2) then
                                                  (comb.Scheduling_Plans(c)(maj).Minor_Frames
                                                   (min1).Deadline <
                                                       comb.Scheduling_Plans(c)(maj).Minor_Frames
                                                   (min2).Deadline))))));

                  for c in cpu_number
                  loop
                     pragma Loop_Invariant(for all i in 0..c-1 =>
                                 ((if(comb.Per_CPU_Storage(i).Current_Minor_Frame /= 1) then
                                      (comb.tsc(i) >= comb.Current_Major_Start_Cycles +
                                             comb.Scheduling_Plans(i)(comb.Current_Major_Frame).Minor_Frames
                                       (comb.Per_CPU_Storage(i).Current_Minor_Frame - 1).Deadline)) and
                                    comb.tsc(i) < comb.Current_Major_Start_Cycles + comb.Scheduling_Plans(i)(comb.Current_Major_Frame).Minor_Frames
                                       (comb.Per_CPU_Storage(i).Current_Minor_Frame).Deadline));
                     Next_Minor_ID := Minor_Frame_Range'First;
                     loop
                        pragma Loop_Invariant(for all p in 1..Next_Minor_ID - 1 =>
                                                (comb.tsc(c) >= comb.Current_Major_Start_Cycles +
                                                     comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                                                 (p).Deadline));
                        comb.Per_CPU_Storage(c).Current_Minor_Frame := Next_Minor_ID;
                        Now := comb.tsc(c);
                        Deadline := comb.Current_Major_Start_Cycles +
                          comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                          (comb.Per_CPU_Storage(c).Current_Minor_Frame).deadline;
                        exit when Deadline > Now;
                        Next_Minor_ID := Next_Minor_ID + 1;
                     end loop;
                     pragma Assert(for all p in 1..Next_Minor_ID - 1 =>
                                     (comb.tsc(c) >= comb.Current_Major_Start_Cycles +
                                          comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                                      (p).Deadline));
                     pragma Assert((if(comb.Per_CPU_Storage(c).Current_Minor_Frame /= 1) then
                                     (comb.tsc(c) >= comb.Current_Major_Start_Cycles +
                                          comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                                      (comb.Per_CPU_Storage(c).Current_Minor_Frame - 1).Deadline)) and
                                     comb.tsc(c) < comb.Current_Major_Start_Cycles +
                                     comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                                   (comb.Per_CPU_Storage(c).Current_Minor_Frame).Deadline);
                     Cycles := Deadline - Now;

                     pragma Assume(Cycles < SK.Word64(2**32));
                     comb.VMCSs(comb.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER := SK.Word32(Cycles);
                     comb.vmx_timer(c) := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER);
                  end loop;
--                    pragma Assert(for all c in cpu_number =>
--                                    (comb.tsc))
                  pragma Assert(for all c in cpu_number =>
                      ((if(comb.Per_CPU_Storage(c).Current_Minor_Frame /= 1) then
                           (comb.tsc(c) >= comb.Current_Major_Start_Cycles +
                                  comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                            (comb.Per_CPU_Storage(c).Current_Minor_Frame - 1).Deadline)) and
                         comb.tsc(c) < comb.Current_Major_Start_Cycles +
                           comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                       (comb.Per_CPU_Storage(c).Current_Minor_Frame).Deadline));
                  pragma Assert(for all c in cpu_number =>
                                  (comb.min_fp(c) = comb.Per_CPU_Storage(c).Current_Minor_Frame));
               end if;

            end;
         end if;
      end if;
      pragma Assert(comb.ideal_maj_fp(cpu) = comb.maj_fp);
      pragma Assert(comb.ideal_maj_fp(cpu) > Major_Frame_Range'First);
      pragma Assert(comb.ideal_cycles(cpu) = comb.cycles);
      pragma Assert(comb.tsc(cpu) - comb.Current_Major_Start_Cycles =
                      comb.major_frame_ends(comb.ideal_maj_fp(cpu) - 1).Period -
                        comb.major_frame_ends(comb.maj_fp - 1).Period +
                           comb.min_ticks(cpu));
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_cycles(c) = comb.cycles) then
                         (if(comb.ideal_maj_fp(c) /= Major_Frame_Range'First and
                              comb.maj_fp /= Major_Frame_Range'First) then
                            ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                             (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period +
                                    comb.min_ticks(c)))
                          else (if(comb.maj_fp = Major_Frame_Range'First and
                              comb.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                                ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                                 (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period +
                                      comb.min_ticks(c)))
                            else (if(comb.maj_fp = Major_Frame_Range'First and
                                comb.ideal_maj_fp(c) = Major_Frame_Range'First) then
                                ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                                 (comb.min_ticks(c)))
                             )))
                       else
                         (if(comb.maj_fp /= Major_Frame_Range'First) then
                              ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                               (comb.major_frame_ends(Major_Frame_Range'Last).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period +
                                    (comb.ideal_cycles(c) - comb.cycles) * comb.L +
                                      comb.ticks(c)))
                          else (if(comb.maj_fp = Major_Frame_Range'First) then
                              ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                               (comb.major_frame_ends(Major_Frame_Range'Last).Period +
                                  (comb.ideal_cycles(c) - comb.cycles) * comb.L +
                                    comb.ticks(c)))
                           ))));


      -- 1 - ideal cycles always >= cycles
      pragma Assert(comb.ideal_cycles(cpu) >= comb.cycles);

      -- 2 - maj_fp = least of ideal_maj_fp's
--        pragma Assert((for all c in cpu_number =>
--                              ((comb.ideal_maj_fp(comb.last) <=
--                                 comb.ideal_maj_fp(c)))) and
--                           (comb.maj_fp = comb.ideal_maj_fp(comb.last)));

      -- 3 - min_ticks < deadline of the last minor frame of the major frame
      pragma Assert(for all c in cpu_number =>
                      (comb.min_ticks(c) <
                           comb.Scheduling_Plans(c)(comb.ideal_maj_fp(c)).Minor_Frames
                       (comb.Scheduling_Plans(c)(comb.ideal_maj_fp(c)).Length).Deadline));

      ------------- Proving next invariant ----------------------
      pragma Assert(comb.ticks(cpu) < comb.L);
      pragma Assert(for all c in cpu_number =>
                      (if (c /= cpu) then
                           (comb.ticks(c) < comb.L)));
      -----------------------------------------------------------

      -- 4 - ticks < L
      pragma Assert(for all c in cpu_number =>
                      (comb.ticks(c) < comb.L));

      -- 5 - min_ticks <= ticks
      pragma Assert(for all c in cpu_number =>
                      (comb.min_ticks(c) <= comb.ticks(c)));

      -- 6 - min_fp < number of minor frames in the major frame
      pragma Assert(for all c in cpu_number =>
                      (comb.min_fp(c) <=
                           comb.Scheduling_Plans(c)(comb.ideal_maj_fp(c)).Length));

      -- ****** Assertions helping to prove invariant 7 *******
      pragma Assert(comb.ticks(cpu) = comb.min_ticks(cpu) +
                      comb.major_frame_ends(comb.ideal_maj_fp(cpu) - 1).Period);

      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                                (comb.ticks(c) = comb.min_ticks(c) +
                                       comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period))));
      -- ******************************************************

      -- 7 - relationship between min_ticks, ticks and ideal_maj_fp
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_maj_fp(c) /= 0) then
                         (comb.ticks(c) =
                          (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period +
                                   comb.min_ticks(c)))
                       else
                         (comb.ticks(c) = comb.min_ticks(c))));

      -- 8 - deadlines of minor frames within a major frame are all > 0
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min in 1..comb.Scheduling_Plans(c)(maj).Length =>
                              (comb.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline > 0))));

      -- 9 - deadline of minor frames in a major frame are strictly increasing
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min in 1..comb.Scheduling_Plans(c)(maj).Length =>
                              (if(min/=1) then
                                 (comb.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline >
                                      comb.Scheduling_Plans(c)(maj).Minor_Frames(min-1).Deadline)))));

      -- 10 - every value in major frame ends and major_frames are > 0
      pragma Assert(for all maj in Major_Frame_Range =>
                      ((comb.major_frame_ends(maj).Period > 0) and
                         (comb.Major_Frames(maj).Period > 0)));

      -- 11 - major_frame_ends values are strictly increasing
      pragma Assert(for all maj in Major_Frame_Range =>
                      (if (maj/=Major_Frame_Range'First) then
                           (comb.major_frame_ends(maj).Period >
                                  comb.major_frame_ends(maj - 1).Period)));

      -- 12 - relationship between major_frames and scheduling_plans
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (comb.Major_Frames(maj).Period =
                              comb.Scheduling_Plans(c)
                          (maj).Minor_Frames(comb.Scheduling_Plans(c)(maj).Length).Deadline
                         )));

      -- 13A - relationship between major_frame_ends and major_frames (a)
      pragma Assert(for all maj in Major_Frame_Range =>
                      (if (maj /= 0) then
                           (comb.Major_Frames(maj).Period =
                            (comb.major_frame_ends(maj).Period -
                                 comb.major_frame_ends(maj - 1).Period))));

      -- 13B - relationship between major_frame_ends and major_frames (b)
      pragma Assert(comb.Major_Frames(Major_Frame_Range'First).Period =
                      comb.major_frame_ends(Major_Frame_Range'First).Period);

      -- 14 - L = major_frame_ends(Major_Frame_Range'Last)
      pragma Assert(comb.L = comb.major_frame_ends(Major_Frame_Range'Last).Period);

      -- 15 - ticks should be between two major frame end values
      pragma Assert(for all c in cpu_number =>
             (if(comb.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                  ((comb.ticks(c) <
                       comb.major_frame_ends(comb.ideal_maj_fp(c)).Period)
                   and
                     (comb.ticks(c) >=
                          comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period))
                else
                  ((comb.ticks(c) <
                       comb.major_frame_ends(comb.ideal_maj_fp(c)).Period)
                   and
                   (comb.ticks(c) >= 0))));

      -- ****** Assertions helping to prove invariant 7 *******
      pragma Assert(if (comb.min_fp(cpu) /= Minor_Frame_Range'First) then
                      ((comb.min_ticks(cpu) <
                         comb.Scheduling_Plans(cpu)
                       (comb.ideal_maj_fp(cpu)).Minor_Frames(comb.min_fp(cpu)).Deadline)
                       and
                         (comb.min_ticks(cpu) >=
                              comb.Scheduling_Plans(cpu)
                          (comb.ideal_maj_fp(cpu)).Minor_Frames(comb.min_fp(cpu) - 1).Deadline))
                    else
                      ((comb.min_ticks(cpu) <
                         comb.Scheduling_Plans(cpu)
                       (comb.ideal_maj_fp(cpu)).Minor_Frames(comb.min_fp(cpu)).Deadline)
                       and
                         (comb.min_ticks(cpu) >= 0)));
      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if (comb.min_fp(c) /= Minor_Frame_Range'First) then
                                ((comb.min_ticks(c) <
                                   comb.Scheduling_Plans(c)
                                 (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c)).Deadline)
                                 and
                                   (comb.min_ticks(c) >=
                                        comb.Scheduling_Plans(c)
                                    (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c) - 1).Deadline))
                            else
                              ((comb.min_ticks(c) <
                                 comb.Scheduling_Plans(c)
                               (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c)).Deadline)
                               and
                                 (comb.min_ticks(c) >= 0)))));
      -- ******************************************************

      -- 16 - min_ticks should be between two minor frame deadlines
      pragma Assert(for all c in cpu_number =>
                      (if (comb.min_fp(c) /= Minor_Frame_Range'First) then
                         ((comb.min_ticks(c) <
                              comb.Scheduling_Plans(c)
                          (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c)).Deadline)
                          and
                            (comb.min_ticks(c) >=
                                 comb.Scheduling_Plans(c)
                             (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c) - 1).Deadline))
                       else
                         ((comb.min_ticks(c) <
                              comb.Scheduling_Plans(c)
                          (comb.ideal_maj_fp(c)).Minor_Frames(comb.min_fp(c)).Deadline)
                          and
                            (comb.min_ticks(c) >= 0))
                      ));

      -- 17A - monotonicity of deadlines of minor frames within a major frame (a)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(min1 < min2) then
                                      (comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                           comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                                   )))));

      -- 17B - monotonicity of deadlines of minor frames within a major frame (b)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(min1 = min2) then
                                      (comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                           comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                                   )))));

      -- 17C - monotonicity of deadlines of minor frames within a major frame (c)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                      comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                                      (min1 < min2)
                                   )))));

      -- 17D - monotonicity of deadlines of minor frames within a major frame (d)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(comb.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                      comb.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                                      (min1 = min2)
                                   )))));

      -- 18A - monotonicity of values in major_frame_ends (a)
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(maj1 < maj2) then
                              (comb.major_frame_ends(maj1).Period < comb.major_frame_ends(maj2).Period))));

      -- 18B - monotonicity of values in major_frame_ends (b)
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(maj1 = maj2) then
                              (comb.major_frame_ends(maj1).Period = comb.major_frame_ends(maj2).Period))));

      -- 18C - monotonicity of values in major_frame_ends (c)
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(comb.major_frame_ends(maj1).Period < comb.major_frame_ends(maj2).Period) then
                              (maj1 < maj2))));

      -- 18D - monotonicity of values in major_frame_ends (d)
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(comb.major_frame_ends(maj1).Period = comb.major_frame_ends(maj2).Period) then
                              (maj1 = maj2))));

      -- 19 - all the CPUs can never be in barrier
      pragma Assert(for some c in cpu_number =>
                      (comb.ideal_cycles(c) = comb.cycles and
                           comb.ideal_maj_fp(c) = comb.maj_fp));

      -- ***** Assertions helping to prove invariant 20 *******
      pragma Assert(comb.ideal_cycles(cpu) = comb.cycles);
      pragma Assert(comb.ideal_maj_fp(cpu) >= comb.maj_fp);
      pragma Assert(if(comb.ideal_cycles(cpu) = comb.cycles) then
                       comb.ideal_maj_fp(cpu) >= comb.maj_fp);
      pragma Assert(for all c in cpu_number =>
                      (if(c /= cpu) then
                           (if(comb.ideal_cycles(c) = comb.cycles) then
                                (comb.ideal_maj_fp(c) >= comb.maj_fp))));
      -- ******************************************************

      -- 20 - if cycles = ideal_cycles then ideal_maj_fp >= maj_fp
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_cycles(c) = comb.cycles) then
                         (comb.ideal_maj_fp(c) >= comb.maj_fp)));

      --concrete invariants

      -- 1 - least of the tsc's will be in the current major frame
--        pragma Assert((for all c in cpu_number =>
--                         (comb.tsc(comb.last) <= comb.tsc(c))) and
--                          (comb.tsc(comb.last) <= (comb.Current_Major_Start_Cycles +
--                                 comb.Major_Frames(comb.Current_Major_Frame).Period)));

      -- 2 - all the CPUs can never be in the barrier
      pragma Assert(for some c in cpu_number =>
                      (comb.Barrier_Set(c) = False));

      -- Gluing invariants

      -- ****** Assertions helping to prove invariant 7 *******
      pragma Assert(comb.ideal_cycles(cpu) = comb.cycles);
      -- pragma Assert(comb.maj_fp /= Major_Frame_Range'First);
      pragma Assert(comb.ideal_maj_fp(cpu) /= Major_Frame_Range'First);
            pragma Assert(if(comb.maj_fp /= Major_Frame_Range'First) then
                      (comb.tsc(cpu) - comb.Current_Major_Start_Cycles =
                           comb.major_frame_ends(comb.ideal_maj_fp(cpu) - 1).Period -
                        comb.major_frame_ends(comb.maj_fp - 1).Period)
                    else
                      (comb.tsc(cpu) - comb.Current_Major_Start_Cycles =
                           comb.major_frame_ends(comb.ideal_maj_fp(cpu) - 1).Period));

--        pragma Assert((comb.tsc(cpu) - comb.Current_Major_Start_Cycles) =
--                     (comb.major_frame_ends(comb.ideal_maj_fp(cpu) - 1).Period -
--                                        comb.major_frame_ends(comb.maj_fp - 1).Period +
--                           comb.min_ticks(cpu)));
--        pragma Assert(for all c in cpu_number =>
--                        (if(c /= cpu and comb.cycles = comb.ideal_cycles(c)) then
--                           (if(comb.maj_fp /= Major_Frame_Range'Last and
--                                comb.ideal_maj_fp(c) /= Major_Frame_Range'Last) then
--                                ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
--                                 (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
--                                        comb.major_frame_ends(comb.maj_fp - 1).Period +
--                                        comb.min_ticks(c))))));
      -- ******************************************************

      -- 1 - relationship between tsc, cmsc, ticks, min_ticks
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_cycles(c) = comb.cycles) then
                         (if(comb.ideal_maj_fp(c) /= Major_Frame_Range'First and
                              comb.maj_fp /= Major_Frame_Range'First) then
                            ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                             (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period +
                                    comb.min_ticks(c)))
                          else (if(comb.maj_fp = Major_Frame_Range'First and
                              comb.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                                ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                                 (comb.major_frame_ends(comb.ideal_maj_fp(c) - 1).Period +
                                      comb.min_ticks(c)))
                            else (if(comb.maj_fp = Major_Frame_Range'First and
                                comb.ideal_maj_fp(c) = Major_Frame_Range'First) then
                                ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                                 (comb.min_ticks(c)))
                             )))
                       else
                         (if(comb.maj_fp /= Major_Frame_Range'First) then
                              ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                               (comb.major_frame_ends(Major_Frame_Range'Last).Period -
                                      comb.major_frame_ends(comb.maj_fp - 1).Period +
                                    (comb.ideal_cycles(c) - comb.cycles) * comb.L +
                                      comb.ticks(c)))
                          else (if(comb.maj_fp = Major_Frame_Range'First) then
                              ((comb.tsc(c) - comb.Current_Major_Start_Cycles) =
                               (comb.major_frame_ends(Major_Frame_Range'Last).Period +
                                  (comb.ideal_cycles(c) - comb.cycles) * comb.L +
                                    comb.ticks(c)))
                           ))));

      -- 2 - relationship between maj_fp and Current_Major_Frame
      pragma Assert(comb.maj_fp = comb.Current_Major_Frame);

      -- 3 - relationship between min_fp and Current_Minor_Frame
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_maj_fp(c) = comb.maj_fp and
                         comb.ideal_cycles(c) = comb.cycles) then
                         (comb.min_fp(c) = comb.Per_CPU_Storage(c).Current_Minor_Frame)));

      -- 4 - relationship between vmx and min_ticks
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_maj_fp(c) = comb.maj_fp and
                         comb.ideal_cycles(c) = comb.cycles) then
                         (comb.vmx_timer(c) = comb.Scheduling_Plans(c)
                          (comb.maj_fp).Minor_Frames(comb.min_fp(c)).Deadline -
                              comb.min_ticks(c))));

      -- 5A - relationship between barrier and abstract ideal_maj_fp and maj_fp
      pragma Assert(for all c in cpu_number =>
                      (if(comb.ideal_maj_fp(c) /= comb.maj_fp or
                         comb.ideal_cycles(c) /= comb.cycles) then
                         (comb.Barrier_Set(c) = True)
                       else (comb.Barrier_Set(c) = False)));

      -- 5B
      pragma Assert(for all c in cpu_number =>
                      (if(comb.Barrier_Set(c) = True) then
                         (comb.ideal_maj_fp(c) /= comb.maj_fp or
                              comb.ideal_cycles(c) /= comb.cycles)
                       else (comb.ideal_maj_fp(c) = comb.maj_fp and
                           comb.ideal_cycles(c) = comb.cycles)));

      pragma Assert(inv_holds(comb));
      -- wrapping
      comb.closed := True;

   end combined_tick;

end tick5;
