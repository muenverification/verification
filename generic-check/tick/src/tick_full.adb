-- case when this CPU is last
--           no_of_last = 1
--           maj_fp /= last

package body tick_full
with SPARK_Mode is

   procedure combined_tick(cpu: cpu_number) is
      all_crossed: Boolean := True;
      next_maj: Major_Frame_Range;
      Current_Major_ID : constant Major_Frame_Range
        := comb.Current_Major_Frame;
      Current_Minor_ID : constant Minor_Frame_Range
        := comb.per_cpu_storage(cpu).Current_Minor_Frame;

      Current_Major_Frame_Start : constant SK.Word64
        := comb.Current_Major_Start_Cycles;
      Now : SK.Word64;
      Cycles : SK.Word64;
      deadline : SK.Word64;

      Next_Minor_ID   : Minor_Frame_Range;
      temp : Major_Frame_Range;
   begin

      -- abstract
      -- update of ideal components

      comb.ticks(cpu) := comb.ticks(cpu) + 1;
      comb.min_ticks(cpu) := comb.min_ticks(cpu) + 1;

      if(comb.min_ticks(cpu) >= -- if <= then can be proved for middle of minor frame case
           comb.Scheduling_Plans(cpu)(comb.ideal_maj_fp(cpu)).Minor_Frames(comb.min_fp(cpu)).Deadline)
      then
         if(comb&.min_fp(cpu) /= comb.Scheduling_Plans(cpu)(comb.ideal_maj_fp(cpu)).Length)
         then
            comb.min_fp(cpu) := comb.min_fp(cpu) + 1;
         else
            comb.min_fp(cpu) := Minor_Frame_Range'First;
            comb.min_ticks(cpu) := 0;
            if(comb.ideal_maj_fp(cpu) /= Major_Frame_Range'Last)
            then
               comb.ideal_maj_fp(cpu) := comb.ideal_maj_fp(cpu) + 1;
            else
               comb.ideal_maj_fp(cpu) := Major_Frame_Range'First;
               comb.ticks(cpu) := 0;
               comb.ideal_cycles(cpu) := comb.ideal_cycles(cpu) + 1;
            end if;
         end if;
      end if;

      if(comb.ideal_maj_fp(cpu) /= comb.maj_fp or
         comb.ideal_cycles(cpu) /= comb.cycles) then

         if(comb.last(cpu) = True) then
            if(comb.no_of_last = 1) then
               if(comb.maj_fp /= Major_Frame_Range'Last) then
                  next_maj := comb.maj_fp + 1;
               else
                  next_maj := Major_Frame_Range'First;
                  comb.cycles := comb.cycles + 1;
               end if;

               comb.maj_fp := next_maj;

               -- update last and no of last
               for c in cpu_number
               loop
                  if (c /= cpu) then
                     if(((comb.ideal_maj_fp(c) = comb.ideal_maj_fp(cpu)) and
                           (comb.ideal_cycles(c) = comb.ideal_cycles(cpu))) and then
                            (comb.ticks(c) = comb.ticks(cpu))) then
                        comb.last(c) := True;
                        comb.no_of_last := comb.no_of_last + 1;
                     end if;
                  end if;
               end loop;
            else
               comb.last(cpu) := False;
               comb.no_of_last := comb.no_of_last - 1;
            end if;
         end if;
      end if;


      -- concrete
      comb.tsc(cpu) := comb.tsc(cpu) + 1;

      if(comb.vmx_timer(cpu) /= 0) then
         comb.vmx_timer(cpu) := comb.vmx_timer(cpu) - 1;
      end if;

      if(comb.vmx_timer(cpu) = 0) then

         if Current_Minor_ID < comb.Scheduling_Plans(cpu)(comb.Current_Major_Frame).Length then

            --  Sync on minor frame barrier if necessary and switch to next
            --  minor frame in current major frame.
            --  not tackling the case of minor frame barrier


            Next_Minor_ID := Current_Minor_ID + 1;
            comb.Per_CPU_Storage(cpu).Current_Minor_Frame := Next_Minor_ID;

            Now := comb.tsc(cpu);
            Deadline := comb.Current_Major_Start_Cycles +
              comb.Scheduling_Plans(cpu)(comb.Current_Major_Frame).Minor_Frames
              (comb.Per_CPU_Storage(cpu).Current_Minor_Frame).deadline;

            if Deadline > Now then
               Cycles := Deadline - Now;
            else
               Cycles := 0;
            end if;

            pragma Assume(Cycles < SK.Word64(2**32));
            comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_VMX_PREEMPT_TIMER := SK.Word32(Cycles);
            comb.vmx_timer(cpu) := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_VMX_PREEMPT_TIMER);
         else

            --  Switch to first minor frame in next major frame.

            Next_Minor_ID := Minor_Frame_Range'First;

         --             MP.Wait_For_All;

            declare
               Barrier_Size  : SK.Byte;
               Barrier_Sense : Boolean;
               CPU_Sense     : Boolean;
            begin
               Barrier_Size  := comb.All_Barrier.Size;
               Barrier_Sense := comb.All_Barrier.Sense;
               CPU_Sense     := not Barrier_Sense;

               --                 Get_And_Increment (Sense_Barrier => comb.All_Barrier,
               --                                    Count         => Count);
               if(comb.Barrier_Set(cpu) = False) then
                  comb.All_Barrier.Wait_Count := comb.All_Barrier.Wait_Count + 1;
               end if;

               comb.Barrier_Set(cpu) := True;

               if comb.All_Barrier.Wait_Count = Barrier_Size then
                  comb.All_Barrier.Wait_Count := 0;
                  comb.All_Barrier.Sense      := CPU_Sense;
                  for c in cpu_number
                  loop
                     comb.Barrier_Set(c) := False;
                  end loop;

                  --                 else
                  --                    loop
                  --                       Barrier_Sense := comb.All_Barrier.Sense;
--
--                       exit when Barrier_Sense = CPU_Sense;
--                       CPU.Pause;
--                    end loop;

                  declare

                     --  Next major frame ID used to access the volatile New_Major
                     --  variable. Do not move the declaration outside of this scope
                     --  as it is only updated on the BSP. All other CPUs must get
                     --  the value from CPU_Global.

                     Next_Major_ID    : Major_Frame_Range;
                     Next_Major_Start : SK.Word64;
                  begin
                     --  Tau0_Interface.Get_Major_Frame (ID => Next_Major_ID);
                     --  Next_Major_ID := Current_Major_ID + 1;
                     if Current_Major_ID = Major_Frame_Range'Last then
                        Next_Major_ID := Major_Frame_Range'First;
                     else
                        Next_Major_ID := Current_Major_ID + 1;
                     end if;

                     --  Increment major frame start time by period of major frame
                     --  that just ended.

                     Next_Major_Start := comb.Current_Major_Start_Cycles
                       + comb.Major_Frames (Current_Major_ID).Period;

                     comb.Current_Major_Frame := Next_Major_ID;
                     --                       CPU_Global.Set_Current_Major_Start_Cycles
                     --                         (TSC_Value => Next_Major_Start);

                     comb.Current_Major_Start_Cycles := Next_Major_Start;
                  end;

                  for c in cpu_number
                  loop
                     pragma Loop_Invariant(for all i in 0..c-1 =>
                                 ((if(comb.Per_CPU_Storage(i).Current_Minor_Frame /= 1) then
                                      (comb.tsc(i) >= comb.Current_Major_Start_Cycles +
                                             comb.Scheduling_Plans(i)(comb.Current_Major_Frame).Minor_Frames
                                       (comb.Per_CPU_Storage(i).Current_Minor_Frame - 1).Deadline)) and
                                    comb.tsc(i) < comb.Current_Major_Start_Cycles + comb.Scheduling_Plans(i)(comb.Current_Major_Frame).Minor_Frames
                                       (comb.Per_CPU_Storage(i).Current_Minor_Frame).Deadline));
                     Next_Minor_ID := Minor_Frame_Range'First;
                     loop
                        pragma Loop_Invariant(for all p in 1..Next_Minor_ID - 1 =>
                                                (comb.tsc(c) >= comb.Current_Major_Start_Cycles +
                                                     comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                                                 (p).Deadline));
                        comb.Per_CPU_Storage(c).Current_Minor_Frame := Next_Minor_ID;
                        Now := comb.tsc(c);
                        Deadline := comb.Current_Major_Start_Cycles +
                          comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                          (comb.Per_CPU_Storage(c).Current_Minor_Frame).deadline;
                        exit when Deadline > Now;
                        Next_Minor_ID := Next_Minor_ID + 1;
                     end loop;
                     Cycles := Deadline - Now;

                     pragma Assume(Cycles < SK.Word64(2**32));
                     comb.VMCSs(comb.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER := SK.Word32(Cycles);
                     comb.vmx_timer(c) := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER);
                  end loop;
--                    pragma Assert(for all c in cpu_number =>
--                                    (comb.tsc))
               end if;

            end;
         end if;
      end if;

   end combined_tick;

end tick_full;
