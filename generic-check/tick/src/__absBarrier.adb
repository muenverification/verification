-- case when this CPU is last
--           no_of_last = 1
--           maj_fp /= last

package body absBarrier
with SPARK_Mode is

   procedure combined_tick(cpu: cpu_number) is
     -- all_crossed: Boolean := True;
      next_maj: Major_Frame_Range;
      --Current_Major_ID : constant Major_Frame_Range
     --   := comb.Current_Major_Frame;
      --Current_Minor_ID : constant Minor_Frame_Range
      --  := comb.per_cpu_storage(cpu).Current_Minor_Frame;

     -- Current_Major_Frame_Start : constant SK.Word64
      --  := comb.Current_Major_Start_Cycles;
     -- Now : SK.Word64;
      Cycles : SK.Word64;
      deadline : SK.Word64;

      --Next_Minor_ID   : Minor_Frame_Range;
      temp : Major_Frame_Range;
   begin

	 -- abstract
      -- update of ideal components

      comb.ticks(cpu) := comb.ticks(cpu) + 1;
      comb.min_ticks(cpu) := comb.min_ticks(cpu) + 1;

      if(comb.min_ticks(cpu) >= -- if <= then can be proved for middle of minor frame case
           comb.Scheduling_Plans(cpu)(comb.ideal_maj_fp(cpu)).Minor_Frames(comb.min_fp(cpu)).Deadline)
      then
         if(comb.min_fp(cpu) /= comb.Scheduling_Plans(cpu)(comb.ideal_maj_fp(cpu)).Length)
         then
            comb.min_fp(cpu) := comb.min_fp(cpu) + 1;
         else
            comb.min_fp(cpu) := Minor_Frame_Range'First;
            comb.min_ticks(cpu) := 0;
            if(comb.ideal_maj_fp(cpu) /= Major_Frame_Range'Last)
            then
               comb.ideal_maj_fp(cpu) := comb.ideal_maj_fp(cpu) + 1;
            else
               comb.ideal_maj_fp(cpu) := Major_Frame_Range'First;
               comb.ticks(cpu) := 0;
               comb.ideal_cycles(cpu) := comb.ideal_cycles(cpu) + 1;
            end if;
         end if;
      end if;

	if(comb.ideal_maj_fp(cpu) /= comb.maj_fp or
         comb.ideal_cycles(cpu) /= comb.cycles) then

         if(comb.last(cpu) = True) then
            if(comb.no_of_last = 1) then
               if(comb.maj_fp /= Major_Frame_Range'Last) then
                  next_maj := comb.maj_fp + 1; -- think about assigning ideal value
               else
                  next_maj := Major_Frame_Range'First;
                  comb.cycles := comb.cycles + 1;
               end if;

	temp := comb.maj_fp + 1;
         comb.maj_fp := next_maj;


	-- update last and no of last
               for c in cpu_number
               loop
                  if (c /= cpu) then
                     if(((comb.ideal_maj_fp(c) = comb.ideal_maj_fp(cpu)) and
                           (comb.ideal_cycles(c) = comb.ideal_cycles(cpu))) and then
                            (comb.ticks(c) = comb.ticks(cpu))) then
                        comb.last(c) := True;
                        comb.no_of_last := comb.no_of_last + 1;
                     end if;
                  end if;
               end loop;
            else
               comb.last(cpu) := False;
               comb.no_of_last := comb.no_of_last - 1;
            end if;
         end if;
      end if;

   end combined_tick;

end absBarrier;
