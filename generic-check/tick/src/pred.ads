package Pred
with SPARK_Mode is
   type newtype is record
      p: Natural;
      q: Natural;
      r: Natural;
   end record
     with Predicate =>
       newtype.q = newtype.p * 2;

   procedure updat(t: in out newtype);

end Pred;
