package body concBarrier
with SPARK_Mode is

   procedure combined_tick(cpu: cpu_number) is
      --all_crossed: Boolean := True;
      next_maj: Major_Frame_Range;
      Current_Major_ID : constant Major_Frame_Range
        := comb.Current_Major_Frame;
      Current_Minor_ID : constant Minor_Frame_Range
        := comb.per_cpu_storage(cpu).Current_Minor_Frame;

      --Current_Major_Frame_Start : constant SK.Word64
       -- := comb.Current_Major_Start_Cycles;
      Now : SK.Word64;
      Cycles : SK.Word64;
      deadline : SK.Word64;

      Next_Minor_ID   : Minor_Frame_Range;
      --temp : Major_Frame_Range;
   begin

	-- concrete
      comb.tsc(cpu) := comb.tsc(cpu) + 1;

      if(comb.vmx_timer(cpu) /= 0) then
         comb.vmx_timer(cpu) := comb.vmx_timer(cpu) - 1;
      end if;

      if(comb.vmx_timer(cpu) = 0) then

         if Current_Minor_ID < comb.Scheduling_Plans(cpu)(comb.Current_Major_Frame).Length then

            --  Sync on minor frame barrier if necessary and switch to next
            --  minor frame in current major frame.

            declare
               Current_Barrier : constant Barrier_Index_Range
                 := comb.Scheduling_Plans(cpu)(Current_Major_ID).Minor_Frames(Current_Minor_ID).Barrier;
               --                    Skp.Scheduling.Get_Barrier
               --                    (CPU_ID   => CPU_Global.CPU_ID,
               --                     Major_ID => Current_Major_ID,
               --                     Minor_ID => Current_Minor_ID);
            begin
               pragma Assume(Current_Barrier /= No_Barrier);
               if Current_Barrier /= No_Barrier then
                  --                   MP.Wait_On_Minor_Frame_Barrier (Index => Current_Barrier);
                  ------------------------------------
                  ------------------------------------
                  ------ put barrier code here.-------
                  ------------------------------------
                  ------------------------------------
                  null;
               end if;
            end;

            Next_Minor_ID := Current_Minor_ID + 1;
            comb.Per_CPU_Storage(cpu).Current_Minor_Frame := Next_Minor_ID;

            Now := comb.tsc(cpu);
            Deadline := comb.Current_Major_Start_Cycles +
              comb.Scheduling_Plans(cpu)(comb.Current_Major_Frame).Minor_Frames
              (comb.Per_CPU_Storage(cpu).Current_Minor_Frame).deadline;

            if Deadline > Now then
               Cycles := Deadline - Now;
            else
               Cycles := 0;
            end if;

            pragma Assume(Cycles < SK.Word64(2**32));
            comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_VMX_PREEMPT_TIMER := SK.Word32(Cycles);
            comb.vmx_timer(cpu) := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(cpu)).GUEST_VMX_PREEMPT_TIMER);
         else

            --  Switch to first minor frame in next major frame.

            Next_Minor_ID := Minor_Frame_Range'First;

         --             MP.Wait_For_All;

            declare
               Barrier_Size  : SK.Byte;
               Barrier_Sense : Boolean;
               CPU_Sense     : Boolean;
            begin
               Barrier_Size  := comb.All_Barrier.Size;
               Barrier_Sense := comb.All_Barrier.Sense;
               CPU_Sense     := not Barrier_Sense;

               --                 Get_And_Increment (Sense_Barrier => comb.All_Barrier,
               --                                    Count         => Count);
               if(comb.Barrier_Set(cpu) = False) then
                  comb.All_Barrier.Wait_Count := comb.All_Barrier.Wait_Count + 1;
               end if;

               comb.Barrier_Set(cpu) := True;

               if comb.All_Barrier.Wait_Count = Barrier_Size then
                  comb.All_Barrier.Wait_Count := 0;
                  comb.All_Barrier.Sense      := CPU_Sense;
                  for c in cpu_number
                  loop
                     comb.Barrier_Set(c) := False;
                  end loop;

                  --                 else
                  --                    loop
                  --                       Barrier_Sense := comb.All_Barrier.Sense;
--
--                       exit when Barrier_Sense = CPU_Sense;
--                       CPU.Pause;
--                    end loop;

                  declare

                     --  Next major frame ID used to access the volatile New_Major
                     --  variable. Do not move the declaration outside of this scope
                     --  as it is only updated on the BSP. All other CPUs must get
                     --  the value from CPU_Global.

                     Next_Major_ID    : Major_Frame_Range;
                     Next_Major_Start : SK.Word64;
                  begin
                     --  Tau0_Interface.Get_Major_Frame (ID => Next_Major_ID);
                     --  Next_Major_ID := Current_Major_ID + 1;
                     if Current_Major_ID = Major_Frame_Range'Last then
                        Next_Major_ID := Major_Frame_Range'First;
                     else
                        Next_Major_ID := Current_Major_ID + 1;
                     end if;

                     --  Increment major frame start time by period of major frame
                     --  that just ended.

                     Next_Major_Start := comb.Current_Major_Start_Cycles
                       + comb.Major_Frames (Current_Major_ID).Period;

                     comb.Current_Major_Frame := Next_Major_ID;
                     --                       CPU_Global.Set_Current_Major_Start_Cycles
                     --                         (TSC_Value => Next_Major_Start);

                     comb.Current_Major_Start_Cycles := Next_Major_Start;

                     if Current_Major_ID /= Next_Major_ID then
                        for I in comb.Major_Frames(Next_Major_ID).Barrier_Config'Range loop
                           pragma Loop_Invariant(True);
                           -- add loop invariant
                           Barriers.Initialize (Barrier => comb.Minor_Frame_Barriers (I),
                                                Size    => SK.Byte (comb.Major_Frames
                                                  (Next_Major_ID).Barrier_Config (I)));
                        end loop;
                        --                            (Config => Skp.Scheduling.Major_Frames
                        --                               (Next_Major_ID).Barrier_Config);
                     end if;
                  end;
               pragma Assume(for all c in cpu_number =>
                               (comb.tsc(c) < (comb.Current_Major_Start_Cycles +
                                    comb.Major_Frames(comb.maj_fp).Period)));

                  for c in cpu_number
                  loop
                     loop
                        comb.Per_CPU_Storage(c).Current_Minor_Frame := Next_Minor_ID;
                        Now := comb.tsc(c);
                        Deadline := comb.Current_Major_Start_Cycles +
                          comb.Scheduling_Plans(c)(comb.Current_Major_Frame).Minor_Frames
                          (comb.Per_CPU_Storage(c).Current_Minor_Frame).deadline;
                        Next_Minor_ID := Next_Minor_ID + 1;
                        exit when Deadline > Now;
                     end loop;
                     Cycles := Deadline - Now;

                     pragma Assume(Cycles < SK.Word64(2**32));
                     comb.VMCSs(comb.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER := SK.Word32(Cycles);
                     comb.vmx_timer(c) := SK.Word64(comb.VMCSs(comb.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER);
                  end loop;
                  pragma Assert(for all c in cpu_number =>
                                  (comb.min_fp(c) =
                                       comb.Per_CPU_Storage(c).Current_Minor_Frame));
               end if;

            end;
         end if;
      end if;
      
	end combined_tick;

end concBarrier;
	
