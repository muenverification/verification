package numbers
is

   max_no_of_channels: constant Positive := 50;

   max_no_of_subjects: constant Positive := 50;

   max_no_of_cpus: constant Positive := 20;

   max_no_of_major_frames: constant Positive := 10;

   max_no_of_minor_frames: constant Positive := 15;

   no_of_channels : Integer range 0..max_no_of_channels
     with Import => True;

   no_of_subjects: Integer range 0..max_no_of_subjects
     with Import => True;

   no_of_cpus: Integer range 0..max_no_of_cpus
     with Import => True;

   no_of_major_frames: Integer range 0..max_no_of_major_frames
     with Import => True;

   no_of_minor_frames: Integer range 0..max_no_of_minor_frames
     with Import => True;

   no_of_groups: Integer
     with Import => True;

   no_of_barriers: Integer
     with Import => True;

   type channel_number is new Integer range 0..no_of_channels;

   type subject_number is new Integer range 0..no_of_subjects;

   type cpu_number is new Integer range 0..no_of_cpus;

   type Major_Frame_Range is new Integer range 0..no_of_major_frames;

   type Minor_Frame_Range is new Integer range 0..no_of_minor_frames;

   type Scheduling_Group_Range is new Integer range 0..no_of_groups;

   type Barrier_Index_Range is new Integer range 0..no_of_barriers;

   subtype Barrier_Size_Type is
     Natural range 1 .. Natural (cpu_number'Last) + 1;

   subtype Barrier_Range is
     Barrier_Index_Range range 1 .. Barrier_Index_Range'Last;

   No_Barrier : constant Barrier_Index_Range := Barrier_Index_Range'First;

end numbers;
