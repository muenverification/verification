package body Pred
with SPARK_Mode is

   procedure updat(t: in out newtype) is
   begin
      pragma Assert(t.q = t.p * 2);
      t.r := 5;
      pragma Assert(t.q = t.p * 2);
      t := (p => 4,
            q => 8,
            r => 9);
   end updat;

end Pred;
