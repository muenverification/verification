#!/bin/bash


####################Change following variable values accordingly##################
gprFilePath="/home/inzemam/Dropbox/Project/Trial-Barrier/trial.gpr"
adbfilePath="reproducer_major_end_case9.adb"
adsfilePath="reproducer_major_end_case9.ads"

level="4"
numOfCPU="7"
timeout="50"
LineNumOfFuncInAds="124"
#############################################################################

echo "Starting........"


targets=$(grep -n "pragma Assert" $adbfilePath |cut -f1 -d: |awk '{print $0,";"}' |tr -d "\n"|tr -d ' ')

elements=$(echo $targets | tr ";" "\n")

for l in $elements
do
	echo "started@"
	date
	echo "Cleaning proof "
	cleanCommand="gnatprove -P$gprFilePath --clean"
	echo $cleanCommand
	$cleanCommand
	echo "*********"
    line="$l"
    
    commandToExec="gnatprove -P$gprFilePath  -j$numOfCPU --limit-line=$adbfilePath:$line --report=statistics --level=$level --limit-subp=$adsfilePath:$LineNumOfFuncInAds --timeout=$timeout "
   	echo "Executing $commandToExec"
    $commandToExec


    echo "finished@"
    date

    echo "======"
   
    echo " "
done


