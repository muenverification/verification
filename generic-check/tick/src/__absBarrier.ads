- case when this CPU is last
--           no_of_last = 1
--           maj_fp /= last

with SK; use SK;
with numbers1; use numbers1;
with SK.CPU;
with SK.Barriers;

package absBarrier
with SPARK_Mode is

   -- type for ticks
   type tick_type is array (cpu_number) of SK.Word64;



   -- types for defining scheduling plans type
   type Minor_Frame_Type is record
      Group_ID : Scheduling_Group_Range;
      Barrier  : Barrier_Index_Range;
      Deadline : SK.Word64;
   end record;

   type Minor_Frame_Array is array (Minor_Frame_Range) of Minor_Frame_Type;

   type Major_Frame_Type is record
      Length       : Minor_Frame_Range;
      Minor_Frames : Minor_Frame_Array;
   end record;

   type Major_Frame_Array is array (Major_Frame_Range) of Major_Frame_Type;

   type Scheduling_Plan_Type is array (cpu_number) of Major_Frame_Array;



   --type to define perCPU Storage
   type Scheduling_Group_Array is array (Scheduling_Group_Range)
     of subject_number;

   type Storage_Type is record
      Scheduling_Groups   : Scheduling_Group_Array;
      Current_Minor_Frame : Minor_Frame_Range;
   end record;

   type Storage_Type_Array is array (cpu_number) of Storage_Type;



   -- type to define Major frame ends
   type Barrier_Config_Array is array (Barrier_Range) of Barrier_Size_Type;

   type Major_Frame_Info_Type is record
      Period         : SK.Word64;
      Barrier_Config : Barrier_Config_Array;
   end record;

   type Major_Frame_Info_Array is array (Major_Frame_Range)
     of Major_Frame_Info_Type;



   -- minor and major frame pointers types
   type minor_fp_array is array (cpu_number) of Minor_Frame_Range;

   type major_fp_array is array (cpu_number) of Major_Frame_Range;



   -- vmcs data structure
   type vmcs_array is array (subject_number) of SK.CPU.VMCStype;

   type page_table_index_type is array (cpu_number) of subject_number;

   type boolean_array is array (cpu_number) of Boolean;

   type Minor_Frame_Barriers_Array is
     array (Barrier_Index_Range) of Barriers.Sense_Barrier_Type;

   -- combined state type
   type combined_state is record
      -- wrapped
      closed : Boolean;

      -- common
      Scheduling_Plans : Scheduling_Plan_Type;
      Major_Frames : Major_Frame_Info_Array; -- array storing period length of each major frame

      -- abstract
      ticks : tick_type; -- equivalent to tsc, keeps on incrementing once initialized
      min_ticks : tick_type; -- ticks within a major frame. grows as the deadlines of minor frames in a major frame
      cycles : SK.Word64;
      ideal_cycles : tick_type;

      min_fp : minor_fp_array; -- minor frame pointer for each cpu
      maj_fp : Major_Frame_Range; -- major frame pointer for each cpu
      ideal_maj_fp : major_fp_array;
--        abs_first_major_start : tick_type; -- tick count at the first major frame, updated when the first major frame is repeated
--        abs_current_major_start : tick_type; -- tick count when the current major frame started
      L : SK.Word64; -- L = major_frame_ends(Major_Frame_Range)
      major_frame_ends : Major_Frame_Info_Array; -- array which stores the end tick of each major frame in a single schedule
      enabled : boolean_array;
      last : boolean_array;
      no_of_last : cpu_number;
   end record;

  procedure combined_tick(cpu: cpu_number);

end absBarrier;


