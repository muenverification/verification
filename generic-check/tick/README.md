# Code for Verification of Tick Operation #
Tick operation takes CPU number (`cpu`) of the CPU which is performing this operation as input and does not return any output.

The operation is declared at lines 118-133 in `./src/tick_full.ads`  with pre and post conditions.

### File containing combined abstract-concrete code ###

`./src/tick_full.adb` and `./src/tick_full.ads`

### Combined State ###

Defined in lines 77-116 in `./src/tick_full.ads`

There are two components of the state:

1. Abstract state (lines 86-101)
2. Concrete state (lines 103-115)

Abstract state components include:

1. `ticks` - It keeps track of the number of ticks passed in a cycle. It is reset at the end of a cycle.
2. `min_ticks` - It keeps track of the number of ticks within a major frame. It is reset at the end of a major frame.
3. `cycles` - It keeps track of the number of cycles passed when there is a barrier.
4. `ideal_cycles` - It keeps track of the number of cycles passed when there is no barrier.
5. `min_fp` - It is the current minor frame pointer. This is a local counter for each logical CPU.
6. `maj_fp` - It is the current major frame pointer when there is a barrier. This is a global counter for all logical CPUs
7. `ideal_maj_fp` - It is the current major frame pointer for each logical CPU when there is no barrier.
8. `L` - It is the length of the cycle. This is a parameter.
9. `major_frame_ends` - This is an array which stores the number of ticks elapsed at the end of a major frame. This is a parameter.
10. `enabled` - This is a Boolean array indexed by the CPU number. If a logical CPU is waiting in a barrier, it is said to be not enabled and the corresponding Boolean value in `enabled` will be false. Otherwise it will be said to be enabled and the corresponding Boolean value in `enabled` will be true.
11. `last` - This is also a Boolean array indexed by the CPU number. If a logical CPU is the last one in the major
12. `no_of_last` - It keeps track of the number of logical CPUs for which `last` is true.

Concrete state components include:

1. `tsc` - This models the Time Stamp Counter register for each CPU.
2. `vmx_timer` - This models the Time Stamp Counter register for each CPU.
3. `VMCSs` - This is an array of VMCS whose type `VMCStype` is defined in lines 94-184 of `sk-cpu.ads`.
4. `VMCS_Pointer` - This models the VMCS pointer register. Here VMCS_Pointer stores the ID of currently active subject instead of the address of the VMCS.
5. `Per_CPU_Storage` - This data structure stores the current minor frame on each CPU
6. `Current_Major_Frame` - This is used to keep track of the current major frame.
7. `Current_Major_Start_Cycles` - This is used to keep track of the scheduled time in terms of ticks at the start of a major frame.
8. `All_Barrier` - This is a record type which is used to implement the sense-reversal barrier in Muen. It contains the fields `sense`, `wait_count` and `size`.
9. `Barrier_Set` - This is an auxiliary Boolean array to keep track of the CPUs which are in the barrier.

There are some state components which are common for both abstract and concrete:

1. `Scheduling_Plans` - This data structure is an array of major frames per CPU. This is a parameter.
2. `Major_Frames` - This array is indexed by `Major_Frame_Range` and contains length of each major frame in terms of ticks. This is a parameter.


### Gluing invariant ###

Defined in lines 421-493 in `./src/tick_full.ads`

Gluing invariant is conjunction of the following:
1. For all the CPUs, the difference between `tsc` and `Current_Major_Start_Cycles` should be equal to `min_ticks` if `ideal_cycles` match with `cycles`. (lines 424-454)

2. `maj_fp` should be equal to `Current_Major_Frame` (lines 458-459)

3. For all CPUs `min_fp` should be equal to `Current_Minor_Frame`. (lines 463-467)

4. For all CPUs, if ideal and non-ideal values of `cycles` and `maj_fp` match, `vmx_timer` (which represents the remaining time) and `min_ticks` (which represents the time elapsed since the beginning of the major frame) add upto the deadline of the minor frame. (lines 471-477)

5. A CPU is (not) in barrier if and only if the `enabled` value is false (true). (lines 481-493)

### Condition R ###

Defined in lines 190-238, 289-365 in `./src/tick_full.ads`

The condition R is conjunction of the following:

1. Deadlines of all minor frames are greater than 0. (lines 188-192)

2. Deadlines of minor frames within a major frame are strictly increasing. (lines 196-202)

3. Values in `major_frame_ends` are greater than 0 and are strictly increasing. (lines 206-217)

4. The value of `Major frames` for a major frame is equal to the deadline of the last minor frame in the major frame in `Scheduling_Plans`. (lines 221-227)

5. Difference between two consecutive values in `major_frame_ends` is length of a major frame. (lines 231-242)

6. `L` which is the length of a cycle in `Scheduling_Plans` is equal to the last value in `major_frame_ends`. (lines 246-247)

7. Deadlines of minor frames in a major frame are monotonous (lines 287-331)

8. `major_frame_ends` is monotonic. (lines 335-363)

### Abstract Code ###

Lines 26-83 in `./src/tick_full.adb`

The abstract tick operation does the following:

1. `ticks` and `min_ticks` are incremented for the CPU on which tick operation came. (lines 29-30)

2. If the CPU is at the end of the current minor frame i.e. `min_ticks` is equal to the deadline mentioned in the scheduling plan for the current minor frame, (lines 32-33)

	a. If current minor frame is not the last minor frame of the major frame i.e. `min_fp` is not equal to number of minor frames in the current major frame, then `min_fp` is incremented. (lines 35-37)

	b. Otherwise the CPU is at the end of the major frame and it follows the following steps.
     
	c. update `min_fp` to point to the first minor frame and `min_ticks` to 0. (lines 39-40)

	d. if the CPU is not at the end of the cycle i.e. `ideal_maj_fp` is not pointing to the last major frame, increment the `ideal_maj_fp`. (lines 41-43)

	e. otherwise the CPU is at the end of the cycle, it udpates the `ideal_maj_fp` to point to first major frame and ticks to 0, and increments `ideal_cycles`. (45-47)

3. If the CPU is already waiting in a barrier (i.e. `ideal_cycles` does not match with `cycles` or `ideal_maj_fp` does not match with the `maj_fp`) and it is the last to reach the major frame then `maj_fp` (and `cycles` too if `maj_fp` is pointing to the last major frame) is incremented and `last` is updated accordingly. (lines 52-83)

### Concrete Code ###

Lines in 86- `./src/tick_full.adb`

The concrete tick operation does the following:

1. Increments the Time Stamp Counter (TSC, modelled by `tsc`) value for the CPU on which tick operation came. (line 87)

2. Decrements the VMX preemption timer (modelled by `vmx_timer`) if it is non-zero for the CPU on which tick operation came. (lines 89-91)

3. If the VMX timer becomes 0, i.e. there is a VM-exit because of VMX preemption timer expiry, then the following is done (line 93)

4. If the current minor frame is not the last minor frame of the current major frame, `Current_Minor_Frame` is incremented and VMX preemption timer (`vmx_timer`) is set to the time remaining for the new minor frame. (lines 95-118)

5. Else the CPU is at the end of the current major frame. It has to wait in the barrier till all the CPUs finish the current major frame. (lines 127-150)

6. Once all the CPUs come out of the barrier, `Current_Major_Frame` and `Current_Major_Start_Cycles` are updated and VMX preemption timer is set to remaining time for the new minor frame. (lines 160-227)

### Files for cases based on the paths in `./src/tick_full.adb` to simplify the proof ###

Case 1 - When CPU is in the middle of a minor frame. Files for this case `tick1.adb` and `tick1.ads`

Case 2 - When CPU is at the end of a minor frame but not at the end of a major frame. Files for this case `tick2.adb` and `tick2.ads`

Case 3 - When CPU is at the end of a major frame, `last` is false for it and it is not in the last major frame of the cycle. Files for this case `tick3.adb` and `tick3.ads`

Case 4 - When CPU is at the end of a major frame, `last` is true for it with `no_of_last` > 1 and it is not in the last major frame of the cycle. Files for this case `tick4.adb` and `tick4.ads`

Case 5 - When CPU is at the end of a major frame, `last` is true for it with `no_of_last` = 1 and it is not in the last major frame of the cycle. Files for this case `tick5.adb` and `tick5.ads`

Case 6 - When CPU is at the end of a major frame, `last` is false for it and it is in the last major frame of the cycle. Files for this case `tick6.adb` and `tick6.ads`

Case 7 - When CPU is at the end of a major frame, `last` is true for it with `no_of_last` > 1 and it is in the last major frame of the cycle. Files for this case `tick7.adb` and `tick7.ads`

Case 8 - When CPU is at the end of a major frame, `last` is true for it with `no_of_last` = 1 and it is in the last major frame of the cycle. Files for this case `tick8.adb` and `tick8.ads`

These cases are implemented as preconditions on the operations in `.ads` files and using assumptions in the `.adb` files.