# Code for Verification of Memory Write and Memory Read Operation #

Memory write operation takes 3 inputs
1. `cpu` which is the CPU on which the operation is taking place.
2. `address` which is the address to which `data` has to be written.
3. `data` which has to be written at `address`.

Memory read operation takes 2 inputs (`cpu` and `address`) and gives 1 output (`conc_data` for concrete and `abs_data` for abstract)

1. `cpu` which is the CPU on which the operation is taking place
2. `address` is the address from which the content has to be read and produced as output in `conc_data` or `abs_data`.

Memory Read operation is declared at lines 92-98 in `./src/memory_op.ads`.

Memory Write operation is declared at lines 79-89 in `./src/memory_op.ads`.


### File containing combined abstract-concrete code ###

`./src/verify3.adb` and `./src/verify3.ads`


### Combined State ###

Defined in lines 38-49 in `./src/verify3.ads`

There are two components of the state:

1. Abstract state (lines 40-43)
2. Concrete state (lines 45-47)

Abstract state components include:

1. `abs_mem` - this is an array of bytes per subject modelling the abstract memory of each subject 
2. `channel` - this is an array of bytes modelling a separate channel memory
3. `abs_curr_sub` - this is an array indexed by CPU number specifying currently running subject on each CPU.

Concrete state components include:

1. `conc_mem` - this is an array of bytes modelling the physical memory.
2. `page_table_index` - this is a per CPU pointer to the page table which is currently being used.

### Gluing invariant ###

Defined in lines 101-127 in `./src/verify3.ads`


### Condition R ###

Conjunction of properties specified by functions 

1. `IsInjective_tr_in_sub` (lines 130-135), 
2. `IsInjective_Cmap_in_sub` (lines 137-142), 
3. `cmap_implies_tr` (lines 144-151) and 
4. `tr_implies_cmap` (lines 153-160)

in `./src/verify3.ads`.


### Abstract Code for Memory Write Operation ###

Lines 22-31 in `./src/verify3.adb`

It does the following:

1. Checks the permission in the subject memory of currently running subject. (lines 23-25)

2. If write permission is true, 

  a. If the given address is part of a channel, the channel memory corresponding to the given logical address is written with the given data. (line 27)

  b. If not, the given address in the subject memory of currently running subject is written with the given data. (line 29)

### Concrete Code for Memory Write Operation ###

Lines 34-42 in `./src/verify3.adb`

It does the following:

1. Checks the permission in the current page table. (line 40)

2. If write permission is true, the CPU writes to the physical memory via the current page table. (line 41)
