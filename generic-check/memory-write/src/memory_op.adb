-- This is the proof of write operation in presence of paging and channels
with Skp;
with SK;
with Skp.Scheduling;
with Sk.CPU_Global;
with SK.Barriers;

package body memory_op
with SPARK_Mode
is


   ---------------------------------------------------------------------------

   -- combined abs and conc procedure which writes to a logical address in the memory
   procedure combined_write(cpu: cpu_number; address: SK.Word64; data: SK.Byte)
   is
      abs_curr_subject: subject_number;
      conc_curr_subject: subject_number;
   begin

      -- abstract
      abs_curr_subject := comb.abs_curr_sub(cpu);

      if(comb.abs_mem(abs_curr_subject)(address).write = True) then -- checking write permission
         if(ch(abs_curr_subject,address)) then -- checking whether part of channel
            comb.channel(cmap(abs_curr_subject,address)) := data; -- writing to abstract channel memory
         else
            comb.abs_mem(abs_curr_subject)(address).data := data; --writing to memory if not part of channel
         end if;
      end if;


      -- concrete

      conc_curr_subject := comb.page_table_index(cpu); -- finding page table of the current CPU

      pragma Assert(abs_curr_subject = conc_curr_subject);

      if(tr(conc_curr_subject, address).write = True) then -- checking write permission
         comb.conc_mem(tr(conc_curr_subject, address).address) := data; -- writing to conc memory
      end if;


      -- checking separate parts of the gluing inv
      pragma Assert(for all s in subject_number =>
                      (for all a in SK.Word64 =>
                         ((comb.abs_mem(s)(a).read = tr(s,a).read) and
                              (comb.abs_mem(s)(a).write = tr(s,a).write) and
                            (comb.abs_mem(s)(a).exec = tr(s,a).exec))));

      pragma Assert(for all c in cpu_number =>
                      (comb.page_table_index(c)=
                           comb.abs_curr_sub(c)));

      pragma Assert(for all s in subject_number =>
                      (for all a in SK.Word64 =>
                         (if(tr(s,a).read or tr(s,a).write or tr(s,a).exec) then
                              (if (ch(s,a) = True) then
                                   (comb.channel(cmap(s,a)) =
                                      comb.conc_mem(tr(s,a).address))
                               else (comb.abs_mem(s)(a).data =
                                   comb.conc_mem(tr(s,a).address))))));

   end combined_write;

   -------------------------------------------------------------

   -- combined abs and conc procedure to read from a given logical address
   procedure combined_read(cpu: cpu_number; address: SK.Word64; abs_data: out SK.Byte; conc_data: out SK.Byte)
   is
      abs_curr_subject: subject_number;
      conc_curr_subject: subject_number;
   begin

      --abstract
      abs_curr_subject := comb.abs_curr_sub(cpu);
      if(comb.abs_mem(abs_curr_subject)(address).read = True) then -- checking read permission
         if(ch(abs_curr_subject, address)) then -- if address part of channel
            abs_data := comb.channel(cmap(abs_curr_subject,address)); -- reading from channel
         else
            abs_data := comb.abs_mem(abs_curr_subject)(address).data; -- else reading from memory
         end if;
      end if;

      --concrete
      conc_curr_subject := comb.page_table_index(cpu);
      if(tr(conc_curr_subject, address).read = True) then -- checking read permission
         conc_data := comb.conc_mem(tr(conc_curr_subject, address).address); -- reading directly from memory
      end if;

   end combined_read;

end memory_op;
