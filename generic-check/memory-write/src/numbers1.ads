package numbers1
is

   max_no_of_channels: constant Positive := 50;

   max_no_of_subjects: constant Positive := 50;

   max_no_of_cpus: constant Positive := 20;

   max_no_of_major_frames: constant Positive := 10;

   max_no_of_minor_frames: constant Positive := 15;

   no_of_channels : Constant Integer range 0..max_no_of_channels
     with Import => True;

   no_of_subjects: Constant Integer range 1..max_no_of_subjects
     with Import => True;

   no_of_cpus: Constant Integer range 1..max_no_of_cpus
     with Import => True;

   no_of_major_frames: Constant Integer range 1..max_no_of_major_frames
     with Import => True;

   no_of_minor_frames: Constant Integer range 1..max_no_of_minor_frames
     with Import => True;

   no_of_groups: Constant Integer
     with Import => True;

   no_of_barriers: Constant Integer
     with Import => True;

   type channel_number is new Integer range 0..no_of_channels;

   type subject_number is new Integer range 0..no_of_subjects-1;

   type cpu_number is new Integer range 0..no_of_cpus-1;

   type Major_Frame_Range is new Integer range 0..no_of_major_frames-1;

   type Minor_Frame_Range is new Integer range 1..no_of_minor_frames;

   type Scheduling_Group_Range is new Integer range 0..no_of_groups;

   type Barrier_Index_Range is new Integer range 0..no_of_barriers;

   subtype Barrier_Size_Type is
     Natural range 1 .. Natural (cpu_number'Last) + 1;

   subtype Barrier_Range is
     Barrier_Index_Range range 1 .. Barrier_Index_Range'Last;

   No_Barrier : constant Barrier_Index_Range := Barrier_Index_Range'First;

end numbers1;
