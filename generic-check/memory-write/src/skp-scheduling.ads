with SK;

package Skp.Scheduling
  with SPARK_Mode
is

   VMX_Timer_Rate : constant := 0;

   type Scheduling_Group_Range is range 1 .. 6;

   type Barrier_Index_Range is range 0 .. 2;

   subtype Barrier_Range is
     Barrier_Index_Range range 1 .. Barrier_Index_Range'Last;

   No_Barrier : constant Barrier_Index_Range := Barrier_Index_Range'First;

   type Minor_Frame_Type is record
      Group_ID : Scheduling_Group_Range;
      Barrier  : Barrier_Index_Range;
      Deadline : SK.Word64;
   end record;

   Null_Minor_Frame : constant Minor_Frame_Type := Minor_Frame_Type'
     (Group_ID => Scheduling_Group_Range'First,
      Barrier  => No_Barrier,
      Deadline => 0);

   type Minor_Frame_Range is range 1 .. 3;

   type Minor_Frame_Array is array (Minor_Frame_Range) of Minor_Frame_Type;

   type Major_Frame_Type is record
      Length       : Minor_Frame_Range;
      Minor_Frames : Minor_Frame_Array;
   end record;

   type Major_Frame_Range is range 0 .. 1;

   type Major_Frame_Array is array (Major_Frame_Range) of Major_Frame_Type;

   Null_Major_Frames : constant Major_Frame_Array := Major_Frame_Array'
     (others => Major_Frame_Type'
        (Length       => Minor_Frame_Range'First,
         Minor_Frames => Minor_Frame_Array'
           (others => Null_Minor_Frame)));

   type Scheduling_Plan_Type is array (Skp.CPU_Range) of Major_Frame_Array;

   Scheduling_Plans : constant Scheduling_Plan_Type := Scheduling_Plan_Type'(
     0 => Major_Frame_Array'(
       0 => Major_Frame_Type'
         (Length       => 3,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 1,
                                    Barrier  => 1,
                                    Deadline => 100000),
             2 => Minor_Frame_Type'(Group_ID => 2,
                                    Barrier  => 2,
                                    Deadline => 350000),
             3 => Minor_Frame_Type'(Group_ID => 2,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000))),
       1 => Major_Frame_Type'
         (Length       => 2,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 2,
                                    Barrier  => No_Barrier,
                                    Deadline => 500000),
             2 => Minor_Frame_Type'(Group_ID => 1,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000),
             others => Null_Minor_Frame))),
     1 => Major_Frame_Array'(
       0 => Major_Frame_Type'
         (Length       => 2,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 3,
                                    Barrier  => 1,
                                    Deadline => 100000),
             2 => Minor_Frame_Type'(Group_ID => 3,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000),
             others => Null_Minor_Frame)),
       1 => Major_Frame_Type'
         (Length       => 1,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 3,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000),
             others => Null_Minor_Frame))),
     2 => Major_Frame_Array'(
       0 => Major_Frame_Type'
         (Length       => 2,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 4,
                                    Barrier  => 2,
                                    Deadline => 350000),
             2 => Minor_Frame_Type'(Group_ID => 4,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000),
             others => Null_Minor_Frame)),
       1 => Major_Frame_Type'
         (Length       => 1,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 4,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000),
             others => Null_Minor_Frame))),
     3 => Major_Frame_Array'(
       0 => Major_Frame_Type'
         (Length       => 2,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 5,
                                    Barrier  => No_Barrier,
                                    Deadline => 50000),
             2 => Minor_Frame_Type'(Group_ID => 6,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000),
             others => Null_Minor_Frame)),
       1 => Major_Frame_Type'
         (Length       => 1,
          Minor_Frames => Minor_Frame_Array'(
             1 => Minor_Frame_Type'(Group_ID => 6,
                                    Barrier  => No_Barrier,
                                    Deadline => 600000),
             others => Null_Minor_Frame))));

   subtype Barrier_Size_Type is
     Natural range 1 .. Natural (Skp.CPU_Range'Last) + 1;

   type Barrier_Config_Array is array (Barrier_Range) of Barrier_Size_Type;

   type Major_Frame_Info_Type is record
      Period         : SK.Word64;
      Barrier_Config : Barrier_Config_Array;
   end record;

   type Major_Frame_Info_Array is array (Major_Frame_Range)
     of Major_Frame_Info_Type;

   Major_Frames : constant Major_Frame_Info_Array := Major_Frame_Info_Array'(
       0 => Major_Frame_Info_Type'
         (Period         => 600000,
          Barrier_Config => Barrier_Config_Array'(
            1 => 2,
            2 => 2)),
       1 => Major_Frame_Info_Type'
         (Period         => 600000,
          Barrier_Config => Barrier_Config_Array'(
            others => Barrier_Size_Type'First)));

   --  Returns the barrier index of the specified minor frame for the given
   --  major frame and CPU identified by ID.
   function Get_Barrier
     (CPU_ID   : CPU_Range;
      Major_ID : Major_Frame_Range;
      Minor_ID : Minor_Frame_Range)
      return Barrier_Index_Range
   is
     (Scheduling_Plans (CPU_ID)(Major_ID).Minor_Frames (Minor_ID).Barrier);

   --  Returns the deadline of the specified minor frame for the given major
   --  frame and CPU identified by ID.
   function Get_Deadline
     (CPU_ID   : CPU_Range;
      Major_ID : Major_Frame_Range;
      Minor_ID : Minor_Frame_Range)
      return SK.Word64
   is
     (Scheduling_Plans (CPU_ID)(Major_ID).Minor_Frames (Minor_ID).Deadline);

   --  Returns the scheduling group ID of the specified minor frame for the
   --  given major frame and CPU identified by ID.
   function Get_Group_ID
     (CPU_ID   : CPU_Range;
      Major_ID : Major_Frame_Range;
      Minor_ID : Minor_Frame_Range)
      return Scheduling_Group_Range
   is
     (Scheduling_Plans (CPU_ID)(Major_ID).Minor_Frames (Minor_ID).Group_ID)
   with post => (Get_Group_ID'Result =
       (Scheduling_Plans (CPU_ID)(Major_ID).Minor_Frames (Minor_ID).Group_ID));

   type Scheduling_Group_Array is array (Scheduling_Group_Range)
     of Subject_Id_Type;

   Scheduling_Groups : constant Scheduling_Group_Array
     := Scheduling_Group_Array'(
          1 => 0,
          2 => 1,
          3 => 5,
          4 => 6,
          5 => 4,
          6 => 7);

end Skp.Scheduling;
