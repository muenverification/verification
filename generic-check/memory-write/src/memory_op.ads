with SK; use SK;
with SKP;
with SKP.Scheduling;
with numbers1; use numbers1;
with SK.CPU;
with SK.Barriers;

package verify3
with SPARK_Mode
is

   -- defining an array of bytes
   type memory is array (SK.Word64) of SK.Byte;

   -- defining abstract memory type
   type subject_memory_cell is record
      data: SK.Byte;
      read: Boolean;
      write: Boolean;
      exec: Boolean;
   end record;

   type subject_memory is array (SK.Word64) of subject_memory_cell;

   type abstract_memory is array (subject_number) of subject_memory;

   type curr_sub_array is array (cpu_number) of subject_number;

   type page_table_entry_type is record
      address: SK.Word64;
      read: Boolean;
      write: Boolean;
      exec: Boolean;
   end record;

   type page_table_index_type is array (cpu_number) of subject_number;

   -- combined type
   type combined_state is record
      -- abstract
      abs_mem : abstract_memory;
      channel : memory;
      abs_curr_sub : curr_sub_array;

      -- concrete
      conc_mem : memory;
      page_table_index : page_table_index_type;

   end record;
--       with Predicate =>
--         -- memory and channel
--       (if(closed = True) then
--            (for all s in subject_number =>
--                 (for all a in SK.Word64 =>
--                    (if (ch(s,a) = True) then
--                       (combined_state.channel(cmap(s,a)) =
--                            combined_state.conc_mem(tr(s,a).address))
--                       else (combined_state.abs_mem(s)(a).data =
--                             combined_state.conc_mem(tr(s,a).address)))))
--        and
--          (for all s in subject_number =>
--               (for all a in SK.Word64 =>
--                    ((combined_state.abs_mem(s)(a).read = tr(s,a).read) and
--                       (combined_state.abs_mem(s)(a).write = tr(s,a).write) and
--                    (combined_state.abs_mem(s)(a).exec = tr(s,a).exec))))
--       );

   function tr (subject: subject_number; address: SK.Word64) return page_table_entry_type
     with Import => True;

   function cmap (subject: subject_number; address: SK.Word64) return SK.Word64
     with Import => True;

   function ch (subject: subject_number; address: SK.Word64) return Boolean
     with Import => True;

   comb: combined_state;

   procedure combined_write(cpu: cpu_number; address: SK.Word64; data: SK.Byte)
     with
       Pre => (inv_holds(comb) and IsInjective_tr_in_sub and IsInjective_Cmap_in_sub and
                 cmap_implies_tr and tr_implies_cmap),

         Post => (inv_holds(comb) and
                  (if(tr(comb.abs_curr_sub(cpu),address).write) then
                       (if (ch(comb.abs_curr_sub(cpu),address) = False) then
                          (comb.abs_mem(comb.abs_curr_sub(cpu))(address).data = data)
                            else
                          (comb.channel(cmap(comb.abs_curr_sub(cpu),address)) = data))));


   procedure combined_read(cpu: cpu_number; address: SK.Word64; abs_data: out SK.Byte; conc_data: out SK.Byte)
     with
       Pre => (inv_holds(comb) and IsInjective_tr_in_sub and IsInjective_Cmap_in_sub and
                 cmap_implies_tr and tr_implies_cmap),
         Post => (inv_holds(comb) and
                    (if (tr(comb.abs_curr_sub(cpu),address).read)
                         then(abs_data = conc_data)));


   function inv_holds(comb_st: combined_state) return Boolean is
     (
      -- invariant for data being same in abs and conc memory
        (for all s in subject_number =>
             (for all a in SK.Word64 =>
                  (if(tr(s,a).read or tr(s,a).write or tr(s,a).exec) then
                      (if (ch(s,a) = True) then
                         (comb_st.channel(cmap(s,a)) =
                              comb_st.conc_mem(tr(s,a).address))
                       else (comb_st.abs_mem(s)(a).data =
                               comb_st.conc_mem(tr(s,a).address))))))

      and

      -- invariant for relating permissions in abs and conc
        (for all s in subject_number =>
             (for all a in SK.Word64 =>
                  ((comb_st.abs_mem(s)(a).read = tr(s,a).read) and
                     (comb_st.abs_mem(s)(a).write = tr(s,a).write) and
                       (comb_st.abs_mem(s)(a).exec = tr(s,a).exec))))

      and

      -- invariant that correct page tables for currently running subject is loaded
        (for all c in cpu_number => (comb_st.page_table_index(c)=
                                         comb_st.abs_curr_sub(c)))
     );


   function IsInjective_tr_in_sub return Boolean is
     (for all s1 in subject_number =>
        (for all a1 in SK.Word64 =>
             (for all a2 in Sk.Word64=>
                  (if (tr(s1, a1).address = tr(s1, a2).address) then
                     (a1=a2)))));

   function IsInjective_Cmap_in_sub return Boolean is
     (for all s1 in subject_number =>
        (for all a1 in SK.Word64 =>
             (for all a2 in SK.Word64 =>
                  (if (cmap(s1,a1) = cmap(s1,a2)) then
                     (a1=a2)))));

   function cmap_implies_tr return Boolean is
     (for all s1 in subject_number =>
        (for all a1 in SK.Word64 =>
             (for all s2 in subject_number =>
                  (for all a2 in SK.Word64 =>
                     (if (ch(s1,a1) and ch(s2,a2) and s1/=s2 and
                            cmap(s1,a1) = cmap(s2,a2)) then
                        (tr(s1,a1).address = tr(s2,a2).address))))));

   function tr_implies_cmap return Boolean is
     (for all s1 in subject_number =>
        (for all a1 in SK.Word64 =>
             (for all s2 in subject_number =>
                  (for all a2 in SK.Word64 =>
                     (if (s1/=s2 and tr(s1,a1).address = tr(s2,a2).address) then
                        ((cmap(s1,a1) = cmap(s2,a2)) and
                             ch(s1,a1) and ch(s2,a2)))))));

end verify3;
