package body experiment2
with SPARK_Mode is

   procedure exp2 is
      x: ind2 with Import => True;
   begin
      --*********** Inner loop *************
      pragma Assume(for all p in 0..x-1 =>
                      (for all r in ind1 =>
                         (if(A(r).id = x) then
                              (A(r).value = 5))));
      for y in ind1
      loop
         pragma Loop_Invariant(for all r in 0..y-1 =>
                                 (if(A(r).id = x) then
                                    (A(r).value = 5)));
         if(A(y).id = x) then
            A(y).value := 5;
         end if;
      end loop;
      pragma Assert(for all r in ind1 =>
                      (if(A(r).id = x) then
                         (A(r).value = 5)));
      pragma Assert(for all p in 0..x =>
                      (for all r in ind1 =>
                         (if(A(r).id = x) then
                              (A(r).value = 5))));
   end exp2;

end experiment2;
