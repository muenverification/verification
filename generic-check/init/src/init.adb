**-- Remaining work comments starting with **
package body initialization1
with SPARK_Mode is

   SEL_KERN_CODE : constant := 16#08#;
   SEL_KERN_DATA : constant := 16#10#;
   SEL_TSS       : constant := 16#18#;
   procedure initialize(state : in out system_state)
   is

   begin
      pragma Assume(assumptions(state));
    
      --abstract initialization


      --initialize subjects state
      for s in subject_number
      loop
         pragma Loop_Invariant(for all p in 0..s-1 =>
                                 (state.subjects_state(p) = Null_Subject_State));
         state.subjects_state(s)     := Null_Subject_State;
      end loop;

      pragma Assert(for all s in subject_number =>
                      state.subjects_state(s) = Null_Subject_State);

      for s in subject_number
      loop
         pragma Loop_Invariant(for all p in 0..s-1  =>
                                 (state.subjects_state(p).RIP =
                                      state.Subject_Specs(p).Entry_Point and
                                      state.subjects_state(p).RSP =
                                      state.Subject_Specs(p).Stack_Address and
                                      state.subjects_state(p).CR0 =
                                      state.Subject_Specs(p).CR0_Value));
         state.subjects_state(s).RIP := state.Subject_Specs(s).Entry_Point;
         state.subjects_state(s).RSP := state.Subject_Specs(s).Stack_Address;
         state.subjects_state(s).CR0 := state.Subject_Specs(s).CR0_Value;
      end loop;

      pragma Assert(for all s in subject_number =>
                      ((state.subjects_state(s).RIP =
                          state.Subject_Specs(s).Entry_Point) and
                          (state.subjects_state(s).RSP =
                              state.Subject_Specs(s).Stack_Address)));

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c-1 =>
                                 (state.ticks(p) = 0 and
                                      state.min_ticks(p) = 0 and
                                      state.ideal_cycles(p) = 0));
         state.ticks(c)          :=   0;
         state.min_ticks(c)      :=   0;
         state.ideal_cycles(c)   :=   0;
      end loop;

      pragma Assert(for all p in cpu_number =>
                      (state.ticks(p) = 0));

      state.cycles               :=   0;

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c-1 =>
                                 (state.min_fp(p) = Minor_Frame_Range'First and
                                      state.ideal_maj_fp(p) = Major_Frame_Range'First and
                                      state.enabled(p) = True and state.last(p) = True));
         state.min_fp(c)         :=   Minor_Frame_Range'First;
         state.ideal_maj_fp(c)   :=   Major_Frame_Range'First;

         state.enabled(c)        :=   True;
         state.last(c)           :=   True;

      end loop;

      pragma Assert(for all p in cpu_number =>
                      (state.min_fp(p) = Minor_Frame_Range'First and
                           state.ideal_maj_fp(p) = Major_Frame_Range'First and
                                      state.enabled(p) = True and state.last(p) = True));

      state.maj_fp               :=   Major_Frame_Range'First;

      state.no_of_last           :=   cpu_number'Last;

      state.L   :=   state.major_frame_ends(Major_Frame_Range'Last).Period;

      pragma Assert(for all c in cpu_number =>
                      (state.min_fp(c) = Minor_Frame_Range'First));

      -- initializing current subject info
      for s in subject_number
      loop
         pragma Loop_Invariant(for all p in 0..s-1 =>
                                 (state.current_subject(p) = False));
         state.current_subject(s) := False;
      end loop;

      pragma Assert(for all s in subject_number =>
                      (state.current_subject(s) = False));
    
      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c-1 =>
                                 (state.current_subject(state.Scheduling_Groups
                                  (state.Scheduling_Plans(p)(state.maj_fp).Minor_Frames
                                      (state.min_fp(p)).Group_ID))));
         declare
            group : Scheduling_Group_Range;
            sub   : subject_number;
         begin
            group := state.Scheduling_Plans(c)(state.maj_fp).Minor_Frames
              (state.min_fp(c)).Group_ID;
            sub := state.Scheduling_Groups(group);
            state.current_subject(sub) := True;
         end;
      end loop;




      -- concrete intialization

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c-1 => (state.tsc(p) = 0));
         state.tsc(c) := 0;
      end loop;

      pragma Assert(for all c in cpu_number => (state.tsc(c) = 0));

      -- cpu_global.init - done only once
      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c-1 =>
                                 (state.Current_Major_Frame = Major_Frame_Range'First and
                                    state.Current_Major_Start_Cycles = 0 and
                                      state.Per_CPU_Storage(p).Current_Minor_Frame =
                                      Minor_Frame_Range'First));
         declare
            Null_Storage : constant Storage_Type := Storage_Type'
              (Scheduling_Groups   => (others => subject_number'First),
               Current_Minor_Frame => Minor_Frame_Range'First);
         begin
            state.Current_Major_Frame        := Major_Frame_Range'First;

            state.Current_Major_Start_Cycles := 0;

            state.Per_CPU_Storage(c)         := Null_Storage;

         end;
      end loop;

      pragma Assert(for all c in cpu_number =>
                      (state.Per_CPU_Storage(c).Current_Minor_Frame =
                           Minor_Frame_Range'First));

      -- scheduler.init - done by each CPU, hence a loop
      for c in cpu_number
      loop
         pragma Loop_Invariant(for all x in 0..c-1 =>
                                 (for all y in subject_number =>
                                    (if(state.Subject_Specs(y).CPU_Id = x) then
                                          (state.Descriptors(y) = Null_Subject_State))));
         declare
            Now                : constant SK.Word64 := state.tsc(c);
            Initial_Subject_ID : subject_number;
            Initial_VMCS_Addr  : SK.Word64 := 0;
            Controls           : VMX_Controls_Type;
            --VMCS_Addr          : SK.Word64;
           -- PD                 : Pseudo_Descriptor_Type;
         begin
--              CPU_Global.Set_Scheduling_Groups -- modify it for our setting
--                (Data => Skp.Scheduling.Scheduling_Groups);
            state.Per_CPU_Storage(c).Scheduling_Groups := state.Scheduling_Groups;

--              Initial_Subject_ID := CPU_Global.Get_Current_Subject_ID; --replace the functions body here
            Initial_Subject_ID := state.Per_CPU_Storage(c).Scheduling_Groups
              (state.Scheduling_Plans (c)(state.Current_Major_Frame).Minor_Frames
               (state.Per_CPU_Storage(c).Current_Minor_Frame).Group_ID);
--                (Skp.Scheduling.Get_Group_ID -- make this function in line
--                   (CPU_ID   => CPU_ID,
--                    Major_ID => Current_Major_Frame,
--                    Minor_ID => Per_CPU_Storage.Current_Minor_Frame));

            --  Setup VMCS and state of subjects running on this logical CPU.

            for I in subject_number
            loop
               pragma Loop_Invariant(for all p in 0..I-1 =>
                                       (if(state.Subject_Specs(p).CPU_Id = c) then
                                          (state.Descriptors(p) = Null_Subject_State)));
--                 if Skp.Subjects.Get_CPU_Id (Subject_Id => I) = CPU_Global.CPU_ID then --replace the functions body here
               --pragma Loop_Invariant(state.Descriptors(I).Regs)
               if state.Subject_Specs(I).CPU_Id = c then
                  --  Initialize subject state.

--                    Subjects.Clear_State (Id => I);
                  state.Descriptors (I) := SK.Null_Subject_State;

                  --  Initialize subject timer.

--                    Timers.Init_Timer (Subject => I);

                  --  VMCS

--                    VMCS_Addr := Skp.Subjects.Get_VMCS_Address (Subject_Id => I);
--                    VMCS_Addr := state.Subject_Specs(I).VMCS_Address;
--                    Controls  := Skp.Subjects.Get_VMX_Controls (Subject_Id => I);
                  Controls  := state.Subject_Specs(I).VMX_Controls;

--                    VMX.Clear (VMCS_Address => VMCS_Addr);
--                    VMX.Load  (VMCS_Address => VMCS_Addr);
                  state.VMCS_Pointer(c) := I;
--                    VMX.VMCS_Setup_Control_Fields
--                      (IO_Bitmap_Address  => state.Subject_Specs (I).IO_Bitmap_Address,
--                       MSR_Bitmap_Address => state.Subject_Specs (I).MSR_Bitmap_Address,
--                       MSR_Store_Address  => state.Subject_Specs (I).MSR_Store_Address,
--                       MSR_Count          => state.Subject_Specs (I).MSR_Count,
--                       Ctls_Exec_Pin      => Controls.Exec_Pin,
--                       Ctls_Exec_Proc     => Controls.Exec_Proc,
--                       Ctls_Exec_Proc2    => Controls.Exec_Proc2,
--                       Ctls_Exit          => Controls.Exit_Ctrls,
--                       Ctls_Entry         => Controls.Entry_Ctrls,
--                       CR0_Mask           => state.Subject_Specs (I).CR0_Mask,
--                       CR4_Mask           => state.Subject_Specs (I).CR4_Mask,
--                       Exception_Bitmap   => state.Subject_Specs (I).Exception_Bitmap);

--                    VMX.VMCS_Setup_Host_Fields;
-- adding vmwrites for setup host fields
--                    VMCS_Write (Field => Constants.HOST_SEL_CS,
--                                Value => SEL_KERN_CODE);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_CS :=
--                      SEL_KERN_CODE;
--                    VMCS_Write (Field => Constants.HOST_SEL_DS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_DS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_ES,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_ES :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_SS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_SS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_FS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_FS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_GS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_GS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_TR,
--                                Value => SEL_TSS);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_TR :=
--                      SEL_TSS;

--                    VMCS_Write (Field => Constants.HOST_CR0,
--                                Value => CPU.Get_CR0);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_CR0 :=
--                      state.CPU_Regs2(c).CR0; --see where CR0 is setup in initialization
--                    VMCS_Write (Field => Constants.CR0_READ_SHADOW,
--                                Value => CPU.Get_CR0);
--                    state.VMCSs(state.VMCS_Pointer(c)).CR0_READ_SHADOW :=
--                      state.CPU_Regs2(c).CR0;
--                    VMCS_Write (Field => Constants.HOST_CR3,
--                                Value => CPU.Get_CR3);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_CR3 :=
--                      state.CPU_Regs2(c).CR3;
--                    VMCS_Write (Field => Constants.HOST_CR4,
--                                Value => CPU.Get_CR4);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_CR4 :=
--                      state.CPU_Regs2(c).CR4;

--                    PD := Interrupts.Get_IDT_Pointer;
--                    PD := state.IDT_Pointer;
--                    VMCS_Write (Field => Constants.HOST_BASE_IDTR,
--                                Value => PD.Base);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_IDTR :=
--                      PD.Base;
--                    PD := GDT.GDT_Pointer;
--                    PD := state.GDT_Pointer;
--                    VMCS_Write (Field => Constants.HOST_BASE_GDTR,
--                                Value => PD.Base);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_GDTR :=
--                      PD.Base;

--                    VMCS_Write (Field => Constants.HOST_BASE_FS,
--                                Value => 0);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_FS := 0;
--                    VMCS_Write (Field => Constants.HOST_BASE_GS,
--                                Value => 0);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_GS := 0;
--
--                    VMCS_Write (Field => Constants.HOST_RSP,
--                                Value => Skp.Kernel.Stack_Address);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_RSP :=
--                      Skp.Kernel.Stack_Address;
--                    VMCS_Write (Field => Constants.HOST_RIP,
--                                Value => VMX_Exit_Address);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_RIP :=
--                      VMX_Exit_Address;
--                    VMCS_Write (Field => Constants.HOST_IA32_EFER,
--                                Value => CPU.Get_MSR64 (Register => Constants.IA32_EFER));
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_IA32_EFER :=
--                      MSR;
-- ending vmwrites for setup host fields

--                    VMX.VMCS_Setup_Guest_Fields
--                      (PML4_Address => state.Subject_Specs (I).PML4_Address,
--                       EPT_Pointer  => state.Subject_Specs (I).EPT_Pointer,
--                       CR0_Value    => state.Subject_Specs (I).CR0_Value,
--                       CR4_Value    => state.Subject_Specs (I).CR4_Value,
--                       CS_Access    => state.Subject_Specs (I).CS_Access);

-- Adding vmwrites of setup guest fields below
--                    VMCS_Write (Field => Constants.VMCS_LINK_POINTER,
--                                Value => SK.Word64'Last);
--                    state.VMCSs(state.VMCS_Pointer(c)).VMCS_LINK_POINTER :=
--                      SK.Word64'Last;
--                    VMCS_Write (Field => Constants.GUEST_SEL_CS,
--                                Value => SEL_KERN_CODE);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_CS :=
                    SEL_KERN_CODE;
--                    VMCS_Write (Field => Constants.GUEST_SEL_DS,
--                                Value => SEL_KERN_DATA);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_DS :=
                    SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.GUEST_SEL_ES,
--                                Value => SEL_KERN_DATA);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_ES :=
                    SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.GUEST_SEL_SS,
--                                Value => SEL_KERN_DATA);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_SS :=
                    SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.GUEST_SEL_TR,
--                                Value => SEL_TSS);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_TR :=
                    SEL_TSS;
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_CS,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_CS :=
                    SK.Word32'Last; -- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_DS,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_DS :=
                    SK.Word32'Last;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_ES,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_ES :=
                    SK.Word32'Last;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_SS,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_SS :=
                    SK.Word32'Last;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_TR,
--                                Value => SK.Word64 (SK.Byte'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_TR :=
                    SK.Word32(SK.Byte'Last);-- they are writing 64 bit word in a 32 bit field

--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_CS,
--                                Value => SK.Word64 (CS_Access));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_CS :=
                    state.Subject_Specs (I).CS_Access;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_DS,
--                                Value => 16#c093#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_DS :=
                    16#c093#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_ES,
--                                Value => 16#c093#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_ES :=
                    16#c093#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_SS,
--                                Value => 16#c093#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_SS :=
                    16#c093#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_TR,
--                                Value => 16#8b#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_TR :=
                    16#8b#;

                  --  Disable fs, gs and ldt segments; they can be enabled by guest code
                  --  if needed.

--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_FS,
--                                Value => 16#10000#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_FS :=
                    16#10000#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_GS,
--                                Value => 16#10000#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_GS :=
                    16#10000#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_LDTR,
--                                Value => 16#10000#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_LDTR :=
                    16#10000#;

--                    VMCS_Write (Field => Constants.GUEST_CR0,
--                                Value => CR0_Value);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR0 :=
                    state.Subject_Specs (I).CR0_Value;
--                    VMCS_Write (Field => Constants.GUEST_CR3,
--                                Value => PML4_Address);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR3 :=
                    state.Subject_Specs (I).PML4_Address;
--                    VMCS_Write (Field => Constants.GUEST_CR4,
--                                Value => CR4_Value);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR4 :=
                    state.Subject_Specs (I).CR4_Value;

--                    VMCS_Write (Field => Constants.EPT_POINTER,
--                                Value => EPT_Pointer);
                  state.VMCSs(state.VMCS_Pointer(c)).EPT_POINTER :=
                    state.Subject_Specs (I).EPT_Pointer;

--                    VMCS_Write (Field => Constants.GUEST_RFLAGS,
--                                Value => 2);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_RFLAGS := 2;
--                    VMCS_Write (Field => Constants.GUEST_IA32_EFER,
--                                Value => 0);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_IA32_EFER := 0;
-- ending vmwrites of setup guest fields

--                    if Initial_Subject_ID = I then
--                       Initial_VMCS_Addr := VMCS_Addr;
--                       Subjects_Sinfo.Export_Scheduling_Info
--                         (Id                 => I,
--                          TSC_Schedule_Start => Now,
--                          TSC_Schedule_End   => Now + Skp.Scheduling.Get_Deadline
--                            (CPU_ID   => CPU_Global.CPU_ID,
--                             Major_ID => Skp.Scheduling.Major_Frame_Range'First,
--                             Minor_ID => Skp.Scheduling.Minor_Frame_Range'First));
--                    end if;

                  --  State
               end if;
            end loop;
            pragma Assert(for all s in subject_number =>
                            (if(state.Subject_Specs(s).CPU_Id = c) then
                               (state.Descriptors(s) = Null_Subject_State)));
         end;
      end loop;

      pragma Assert(for all c in cpu_number => (state.tsc(c) = 0));
      pragma Assert(state.Current_Major_Start_Cycles = 0);
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min in Minor_Frame_Range =>
                              (state.Scheduling_Plans(c)(maj).Minor_Frames
                              (min).Deadline > 0))));

      pragma Assert(if(for all c in cpu_number =>
                         (for all s in subject_number =>
                            (if(state.Subject_Specs(s).CPU_Id = c) then
                                 (state.Descriptors(s).RIP = state.Subject_Specs(s).Entry_Point)))) then
                      (for all s in subject_number =>
                         (state.Descriptors(s).RIP =
                              state.Subject_Specs(s).Entry_Point)));

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all i in 0..c-1 =>
                                 (for all j in subject_number =>
                                    (if(state.Subject_Specs(j).CPU_Id = i) then
                                         (state.Descriptors(j).RIP =
                                                state.Subject_Specs(j).Entry_Point and
                                                state.Descriptors(j).RSP =
                                                state.Subject_Specs(j).Stack_Address and
                                                state.Descriptors(j).CR0 =
                                                state.Subject_Specs(j).CR0_Value))));
         declare
            Now                : constant SK.Word64 := state.tsc(c);
            Initial_Subject_ID : subject_number;
            Initial_VMCS_Addr  : SK.Word64 := 0;
            --Controls           : VMX_Controls_Type;
            --VMCS_Addr          : SK.Word64;
            --PD                 : Pseudo_Descriptor_Type;
         begin
            --              CPU_Global.Set_Scheduling_Groups -- modify it for our setting
--                (Data => Skp.Scheduling.Scheduling_Groups);
--              state.Per_CPU_Storage(c).Scheduling_Groups := state.Scheduling_Groups;
--
--              --              Initial_Subject_ID := CPU_Global.Get_Current_Subject_ID; --replace the functions body here
--              Initial_Subject_ID := state.Per_CPU_Storage(c).Scheduling_Groups
--                (state.Scheduling_Plans (c)(state.Current_Major_Frame).Minor_Frames
--                 (state.Per_CPU_Storage(c).Current_Minor_Frame).Group_ID);
            for I in subject_number
            loop
               pragma Loop_Invariant(for all q in 0..c =>
                                       (for all p in 0..I-1 =>
                                          (if(state.Subject_Specs(p).CPU_Id = q) then
                                               (state.Descriptors(p).RIP =
                                                      state.Subject_Specs(p).Entry_Point and
                                                      state.Descriptors(p).RSP =
                                                      state.Subject_Specs(p).Stack_Address and
                                                      state.Descriptors(p).CR0 =
                                                      state.Subject_Specs(p).CR0_Value))));
               if state.Subject_Specs(I).CPU_Id = c then

                  --                    Subjects.Set_RIP
                  --                      (Id    => I,
                  --                       Value => Skp.Subjects.Get_Entry_Point (Subject_Id => I));
                  state.Descriptors(I).RIP := state.Subject_Specs(I).Entry_Point;
--                    Subjects.Set_RSP
--                      (Id    => I,
--                       Value => Skp.Subjects.Get_Stack_Address (Subject_Id => I));
                  state.Descriptors(I).RSP := state.Subject_Specs(I).Stack_Address;
                  --                    Subjects.Set_CR0
                  --                      (Id    => I,
                  --                       Value => Skp.Subjects.Get_CR0 (Subject_Id => I));
                  state.Descriptors(I).CR0 := state.Subject_Specs(I).CR0_Value;
               end if;
            end loop;

            --  Load first subject and set preemption timer ticks.

            --              VMX.Load (VMCS_Address => Initial_VMCS_Addr);
            state.VMCS_Pointer(c) := Initial_Subject_ID;

            if CPU_Global.Is_BSP then

               --  Set minor frame barriers config.

               MP.Set_Minor_Frame_Barrier_Config
                 (Config => Skp.Scheduling.Major_Frames
                    (Skp.Scheduling.Major_Frame_Range'First).Barrier_Config);

               --  Set initial major frame start time to now.

               CPU_Global.Set_Current_Major_Start_Cycles (TSC_Value => Now);
            end if;

        pragma Assert(for all j in subject_number =>
                            (if(state.Subject_Specs(j).CPU_Id = c) then
                               (state.Descriptors(j).RIP =
                                    state.Subject_Specs(j).Entry_Point and
                                    state.Descriptors(j).RSP =
                                    state.Subject_Specs(j).Stack_Address and
                                    state.Descriptors(j).CR0 =
                                    state.Subject_Specs(j).CR0_Value)));
        pragma Assert(for all p in 0..c =>
                            (for all q in subject_number =>
                               (state.Descriptors(q).RIP =
                                    state.Subject_Specs(q).Entry_Point and
                                    state.Descriptors(q).RSP =
                                    state.Subject_Specs(q).Stack_Address and
                                    state.Descriptors(q).CR0 =
                                    state.Subject_Specs(q).CR0_Value)));
        end;
      end loop;

      pragma Assert(for all c in cpu_number =>
                      (for all s in subject_number =>
                         (if(state.Subject_Specs(s).CPU_Id = c) then
                         (state.Descriptors(s).RIP = state.Subject_Specs(s).Entry_Point))));
      pragma Assert(for all s in subject_number =>
                      (state.Descriptors(s).RIP =
                           state.Subject_Specs(s).Entry_Point));
      
      -- scheduler.set_vmx_timer - done by each CPU, hence a loop

      for c in cpu_number
      loop
         pragma Loop_Invariant((for all p in 0..c-1 =>
                                  (state.vmx_timer(p) =
                                       state.Scheduling_Plans(p)
                                   (state.Current_Major_Frame).Minor_Frames
                                   (state.Per_CPU_Storage(p).Current_Minor_Frame).Deadline)));-- and
--                                       state.tsc(p) = 0)) and
--                                     state.Current_Major_Start_Cycles = 0);

         declare
            Now      : constant SK.Word64 := state.tsc(c);
            Deadline : SK.Word64;
            Cycles   : SK.Word64;
         begin
--              pragma Assert(state.Scheduling_Plans(c)(state.Current_Major_Frame).Minor_Frames
--                (state.Per_CPU_Storage(c).Current_Minor_Frame).Deadline <= SK.Word64(SK.Word32'Last));
            Deadline := state.Current_Major_Start_Cycles +
              state.Scheduling_Plans(c)(state.Current_Major_Frame).Minor_Frames
              (state.Per_CPU_Storage(c).Current_Minor_Frame).Deadline;
--                  (CPU_ID   => CPU_Global.CPU_ID,
--                   Major_ID => CPU_Global.Get_Current_Major_Frame_ID,
--                   Minor_ID => CPU_Global.Get_Current_Minor_Frame_ID);

            if Deadline > Now then
               Cycles := Deadline - Now;
            else
               Cycles := 0;
            end if;

--              VMX.VMCS_Write (Field => Constants.GUEST_VMX_PREEMPT_TIMER,
--                              Value => Cycles / 2 ** Skp.Scheduling.VMX_Timer_Rate);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER :=
              SK.Word32(Cycles);
--              state.vmx_timer(c) := SK.Word64(state.VMCSs(state.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER);
            state.vmx_timer(c) := Cycles;

            pragma Assert(state.vmx_timer(c) = state.Scheduling_Plans(c)
                          (state.Current_Major_Frame).Minor_Frames(state.Per_CPU_Storage
                            (c).Current_Minor_Frame).Deadline);

         end;
      end loop;

      pragma Assert(for all c in cpu_number =>
                      (state.vmx_timer(c) =
                          state.Scheduling_Plans(c)(state.Current_Major_Frame).Minor_Frames
                       (state.Per_CPU_Storage(c).Current_Minor_Frame).Deadline));
   
      -- subjects.restore_state - done by each CPU, hence a loop
      for c in cpu_number
      loop
         declare
            Id   : subject_number ;
         begin
            Id := state.Per_CPU_Storage(c).Scheduling_Groups
              (state.Scheduling_Plans (c)(state.Current_Major_Frame).Minor_Frames
               (state.Per_CPU_Storage(c).Current_Minor_Frame).Group_ID);
--              VMX.VMCS_Write (Field => Constants.GUEST_RIP,
--                              Value => Descriptors (Id).RIP);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_RIP := state.Descriptors(Id).RIP;
--              VMX.VMCS_Write (Field => Constants.GUEST_RSP,
--                              Value => Descriptors (Id).RSP);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_RSP := state.Descriptors(Id).RSP;
--              VMX.VMCS_Write (Field => Constants.GUEST_CR0,
--                              Value => Descriptors (Id).CR0);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR0 := state.Descriptors(Id).CR0;
--              VMX.VMCS_Write (Field => Constants.CR0_READ_SHADOW,
--                              Value => Descriptors (Id).SHADOW_CR0);
            state.VMCSs(state.VMCS_Pointer(c)).CR0_READ_SHADOW :=
              state.Descriptors(Id).SHADOW_CR0;
            state.CPU_Regs(c).Regs := state.Descriptors (Id).Regs;
         end;
      end loop;

      pragma Assert(for all c in cpu_number =>
                      (state.Per_CPU_Storage(c).Current_Minor_Frame =
                           Minor_Frame_Range'First));
      pragma Assert(for all c in cpu_number =>
                      (state.min_fp(c) = Minor_Frame_Range'First));

      -- checking each invariant

      -- Abstract

      -- 1 - ideal cycles always >= cycles
      pragma Assert(for all c in cpu_number =>
                      (state.ideal_cycles(c) >= state.cycles));

      -- 2 - maj_fp = least of ideal_maj_fp's
--        pragma Assert(for all c in cpu_number =>
--                        (if (state.last(c) = True) then
--                           (state.maj_fp = state.ideal_maj_fp(c) and
--                                state.cycles = state.ideal_cycles(c))));

      -- 3 - min_ticks < deadline of the last minor frame of the major frame
      pragma Assert(for all c in cpu_number =>
                      (state.min_ticks(c) <
                           state.Scheduling_Plans(c)(state.ideal_maj_fp(c)).Minor_Frames
                       (state.Scheduling_Plans(c)(state.ideal_maj_fp(c)).Length).Deadline));

      -- 4 - ticks < L
      pragma Assert(for all c in cpu_number =>
                      (state.ticks(c) < state.L));

      -- 5 - min_ticks <= ticks
      pragma Assert(for all c in cpu_number =>
                      (state.min_ticks(c) <= state.ticks(c)));

      -- 6 - min_fp < number of minor frames in the major frame
      pragma Assert(for all c in cpu_number =>
                      (state.min_fp(c) <=
                           state.Scheduling_Plans(c)(state.ideal_maj_fp(c)).Length));

      -- 7 - relationship between min_ticks, ticks and ideal_maj_fp
      pragma Assert(for all c in cpu_number =>
                      (if(state.ideal_maj_fp(c) /= 0) then
                         (state.ticks(c) =
                          (state.major_frame_ends(state.ideal_maj_fp(c) - 1).Period +
                                   state.min_ticks(c)))
                       else
                         (state.ticks(c) = state.min_ticks(c))));

      -- 8 - deadlines of minor frames within a major frame are all > 0
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min in 1..state.Scheduling_Plans(c)(maj).Length =>
                              (state.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline > 0))));

      -- 9 - deadline of minor frames in a major frame are strictly increasing
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min in 1..state.Scheduling_Plans(c)(maj).Length =>
                              (if(min/=1) then
                                 (state.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline >
                                      state.Scheduling_Plans(c)(maj).Minor_Frames(min-1).Deadline)))));

      -- 10 - every value in major frame ends and major_frames are > 0
      pragma Assert(for all maj in Major_Frame_Range =>
                      ((state.major_frame_ends(maj).Period > 0) and
                         (state.Major_Frames(maj).Period > 0)));

      -- 11 - major_frame_ends values are strictly increasing
      pragma Assert(for all maj in Major_Frame_Range =>
                      (if (maj/=Major_Frame_Range'First) then
                           (state.major_frame_ends(maj).Period >
                                  state.major_frame_ends(maj - 1).Period)));

      -- 12 - relationship between major_frames and scheduling_plans
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (state.Major_Frames(maj).Period =
                              state.Scheduling_Plans(c)
                          (maj).Minor_Frames(state.Scheduling_Plans(c)(maj).Length).Deadline
                         )));

      -- 13A - relationship between major_frame_ends and major_frames (a)
      pragma Assert(for all maj in Major_Frame_Range =>
               (if (maj /= 0) then
                    (state.Major_Frames(maj).Period =
                     (state.major_frame_ends(maj).Period -
                        state.major_frame_ends(maj - 1).Period))));

      -- 13B - relationship between major_frame_ends and major_frames (b)
      pragma Assert(state.Major_Frames(Major_Frame_Range'First).Period =
                      state.major_frame_ends(Major_Frame_Range'First).Period);

      -- 14 - L = major_frame_ends(Major_Frame_Range'Last)
      pragma Assert(state.L = state.major_frame_ends(Major_Frame_Range'Last).Period);

      -- 15 - ticks should be between two major frame end values
      pragma Assert(for all c in cpu_number =>
                      (if(state.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                         ((state.ticks(c) <
                              state.major_frame_ends(state.ideal_maj_fp(c)).Period)
                          and
                            (state.ticks(c) >=
                                 state.major_frame_ends(state.ideal_maj_fp(c) - 1).Period))
                       else
                         ((state.ticks(c) <
                              state.major_frame_ends(state.ideal_maj_fp(c)).Period)
                          and
                            (state.ticks(c) >= 0))));

      -- 16 - min_ticks should be between two minor frame deadlines
      pragma Assert(for all c in cpu_number =>
                      (if (state.min_fp(c) /= Minor_Frame_Range'First) then
                         ((state.min_ticks(c) <
                              state.Scheduling_Plans(c)
                          (state.ideal_maj_fp(c)).Minor_Frames(state.min_fp(c)).Deadline)
                          and
                            (state.min_ticks(c) >=
                                 state.Scheduling_Plans(c)
                             (state.ideal_maj_fp(c)).Minor_Frames(state.min_fp(c) - 1).Deadline))
                       else
                         ((state.min_ticks(c) <
                              state.Scheduling_Plans(c)
                          (state.ideal_maj_fp(c)).Minor_Frames(state.min_fp(c)).Deadline)
                          and
                            (state.min_ticks(c) >= 0))
             ));

      -- 17A - monotonicity of deadlines of minor frames within a major frame (a)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(min1 < min2) then
                                      (state.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                           state.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                                   )))));

      -- 17B - monotonicity of deadlines of minor frames within a major frame (b)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(min1 = min2) then
                                      (state.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                           state.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                                   )))));

      -- 17C - monotonicity of deadlines of minor frames within a major frame (c)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(state.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                      state.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                                      (min1 < min2)
                                   )))));

      -- 17D - monotonicity of deadlines of minor frames within a major frame (d)
      pragma Assert(for all c in cpu_number =>
                      (for all maj in Major_Frame_Range =>
                         (for all min1 in Minor_Frame_Range =>
                              (for all min2 in Minor_Frame_Range =>
                                   (if(state.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                      state.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                                      (min1 = min2)
                                   )))));

      -- 18A - monotonicity of values in major_frame_ends
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(maj1 < maj2) then
                              (state.major_frame_ends(maj1).Period < state.major_frame_ends(maj2).Period))));

      --
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(maj1 = maj2) then
                              (state.major_frame_ends(maj1).Period = state.major_frame_ends(maj2).Period))));

      --
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(state.major_frame_ends(maj1).Period <
                              state.major_frame_ends(maj2).Period) then
                              (maj1 < maj2))));

      --
      pragma Assert(for all maj1 in Major_Frame_Range =>
                      (for all maj2 in Major_Frame_Range =>
                         (if(state.major_frame_ends(maj1).Period = state.major_frame_ends(maj2).Period) then
                              (maj1 = maj2))));

      -- 19 - all the CPUs can never be in barrier
      pragma Assert(for some c in cpu_number =>
                      (state.ideal_cycles(c) = state.cycles and
                           state.ideal_maj_fp(c) = state.maj_fp));

      -- 20 - if cycles = ideal_cycles then ideal_maj_fp >= maj_fp
      pragma Assert(for all c in cpu_number =>
                      (if(state.ideal_cycles(c) = state.cycles) then
                         (state.ideal_maj_fp(c) >= state.maj_fp)));

      -- 21A - enabled <=> maj_fp = ideal_maj_fp and cycles = ideal_cycles
      pragma Assert(for all c in cpu_number =>
                      (if(state.ideal_cycles(c) = state.cycles and
                         state.ideal_maj_fp(c) = state.maj_fp) then
                         (state.enabled(c) = True)));

      -- 21B -
      pragma Assert(for all c in cpu_number =>
                      (if(state.enabled(c) = True) then
                         (state.ideal_cycles(c) = state.cycles and
                              state.ideal_maj_fp(c) = state.maj_fp)
                       else
                         (state.ideal_cycles(c) /= state.cycles or
                              state.maj_fp /= state.ideal_maj_fp(c))));



      -- Concrete

      -- 1 - least of the tsc's will be in the current major frame
--        pragma Assert(for all c in cpu_number =>
--                        (if(state.last(c) = True) then
--                           (state.tsc(c) <=
--                            (state.Current_Major_Start_Cycles +
--                                     state.Major_Frames(state.Current_Major_Frame).Period))));

      -- 2 - all the CPUs can never be in the barrier
      pragma Assert(for some c in cpu_number =>
                      (state.Barrier_Set(c) = False));



      -- Gluing
      -- *** proving gluing invariant  1 ****
      pragma Assert(for all c in cpu_number => (state.ideal_cycles(c) = 0));
      pragma Assert(state.cycles = 0);
      pragma Assert(for all c in cpu_number => (state.ideal_maj_fp(c) =
                                                  Major_Frame_Range'First));
      pragma Assert(state.maj_fp = Major_Frame_Range'First);
      pragma Assert(for all c in cpu_number => (state.tsc(c) = 0));
      pragma Assert(state.Current_Major_Start_Cycles = 0);
      pragma Assert(for all c in cpu_number => (state.min_ticks(c) = 0));
      -- ***        end                  ****
      -- 1 - relationship between tsc, cmsc, ticks, min_ticks
      pragma Assert(for all c in cpu_number =>
                      (if(state.ideal_cycles(c) = state.cycles) then
                         (if(state.ideal_maj_fp(c) /= Major_Frame_Range'First and
                              state.maj_fp /= Major_Frame_Range'First) then
                            ((state.tsc(c) - state.Current_Major_Start_Cycles) =
                             (state.major_frame_ends(state.ideal_maj_fp(c) - 1).Period -
                                      state.major_frame_ends(state.maj_fp - 1).Period +
                                    state.min_ticks(c)))
                          else (if(state.maj_fp = Major_Frame_Range'First and
                              state.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                                ((state.tsc(c) - state.Current_Major_Start_Cycles) =
                                 (state.major_frame_ends(state.ideal_maj_fp(c) - 1).Period +
                                      state.min_ticks(c)))
                            else (if(state.maj_fp = Major_Frame_Range'First and
                                state.ideal_maj_fp(c) = Major_Frame_Range'First) then
                                ((state.tsc(c) - state.Current_Major_Start_Cycles) =
                                 (state.min_ticks(c)))
                             )))
                       else
                         (if(state.maj_fp /= Major_Frame_Range'First) then
                              ((state.tsc(c) - state.Current_Major_Start_Cycles) =
                               (state.major_frame_ends(Major_Frame_Range'Last).Period -
                                      state.major_frame_ends(state.maj_fp - 1).Period +
                                    (state.ideal_cycles(c) - state.cycles) * state.L +
                                      state.ticks(c)))
                          else (if(state.maj_fp = Major_Frame_Range'First) then
                              ((state.tsc(c) - state.Current_Major_Start_Cycles) =
                               (state.major_frame_ends(Major_Frame_Range'Last).Period +
                                  (state.ideal_cycles(c) - state.cycles) * state.L +
                                    state.ticks(c)))
                           ))));

      -- 2 - relationship between maj_fp and Current_Major_Frame
      pragma Assert(state.maj_fp = state.Current_Major_Frame);

      -- **** Assertions to prove following invariant ****
      pragma Assert(for all c in cpu_number =>
                      (state.Per_CPU_Storage(c).Current_Minor_Frame =
                           Minor_Frame_Range'First));
      pragma Assert(for all c in cpu_number =>
                      (state.min_fp(c) = Minor_Frame_Range'First));
      -- ****                end                      ****
      -- 3 - relationship between min_fp and Current_Minor_Frame
      pragma Assert(for all c in cpu_number =>
                      (if(state.ideal_maj_fp(c) = state.maj_fp and
                         state.ideal_cycles(c) = state.cycles) then
                         (state.min_fp(c) = state.Per_CPU_Storage(c).Current_Minor_Frame)));

      -- 4 - relationship between vmx and min_ticks
      pragma Assert(for all c in cpu_number =>
                      (if(state.ideal_maj_fp(c) = state.maj_fp and
                         state.ideal_cycles(c) = state.cycles) then
                         (state.vmx_timer(c) = state.Scheduling_Plans(c)
                          (state.maj_fp).Minor_Frames(state.min_fp(c)).Deadline -
                              state.min_ticks(c))));

      -- 5A - relationship between barrier and enabled
      pragma Assert(for all c in cpu_number =>
                      (if(state.enabled(c) = False) then
                         (state.Barrier_Set(c) = True)
                       else (state.Barrier_Set(c) = False)));

      -- 5B
      pragma Assert(for all c in cpu_number =>
                      (if(state.Barrier_Set(c) = True) then
                         (state.enabled(c) = False)
                       else (state.enabled(c) = True)));

      -- 6 - Registers of a subject are equal to CPU register value and
      --            when it is one of the current subject
      --     and
      --     it should be equal to VMCS and descriptor register values
      --            when it is not the current subject
      pragma Assert(for all s in subject_number =>
                      (if(state.current_subject(s) = True) then
                         (state.subjects_state(s) =
                              state.CPU_Regs(state.Subject_Specs(s).CPU_Id))
                       else
                         (state.subjects_state(s) = state.Descriptors(s))));

      pragma Assert(inv_holds(state));

   end initialize;

end initialization1;
