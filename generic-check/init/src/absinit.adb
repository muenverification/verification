-- Remaining work comments starting with **
package body absinit
with SPARK_Mode is

   SEL_KERN_CODE : constant := 16#08#;
   SEL_KERN_DATA : constant := 16#10#;
   SEL_TSS       : constant := 16#18#;
   procedure initialize(state : in out system_state)
   is

   begin
      --abstract initialization

      --initialize subjects state
      for s in subject_number
      loop
         pragma Loop_Invariant(for all p in 0..s-1 =>
                                 (state.subjects_state(p) = Null_Subject_State));
         state.subjects_state(s)     := Null_Subject_State;
      end loop;
      pragma Assert(for all s in subject_number =>
                      state.subjects_state(s) = Null_Subject_State);

      for s in subject_number
      loop
         pragma Loop_Invariant(for all p in 0..s =>
                                 (state.subjects_state(p).RIP =
                                      state.Subject_Specs(p).Entry_Point and
                                      state.subjects_state(p).RSP =
                                      state.Subject_Specs(p).Stack_Address and
                                      state.subjects_state(p).CR0 =
                                      state.Subject_Specs(p).CR0_Value));
         state.subjects_state(s).RIP := state.Subject_Specs(s).Entry_Point;
         state.subjects_state(s).RSP := state.Subject_Specs(s).Stack_Address;
         state.subjects_state(s).CR0 := state.Subject_Specs(s).CR0_Value;
      end loop;
      pragma Assert(for all s in subject_number =>
                      ((state.subjects_state(s).RIP =
                         state.Subject_Specs(s).Entry_Point) and
                         (state.subjects_state(s).RSP =
                              state.Subject_Specs(s).Stack_Address)));

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c =>
                                 (state.ticks(p) = 0 and
                                      state.min_ticks(p) = 0 and
                                      state.ideal_cycles(p) = 0));
         state.ticks(c)          :=   0;
         state.min_ticks(c)      :=   0;
         state.ideal_cycles(c)   :=   0;
      end loop;
      pragma Assert(for all p in cpu_number =>
                      (state.ticks(p) = 0));

      state.cycles               :=   0;

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c =>
                                 (state.min_fp(p) = Minor_Frame_Range'First and
                                      state.ideal_maj_fp(p) = Major_Frame_Range'First and
                                      state.enabled(p) = True and state.last(p) = True));
         state.min_fp(c)         :=   Minor_Frame_Range'First;
         state.ideal_maj_fp(c)   :=   Major_Frame_Range'First;

         state.enabled(c)        :=   True;
         state.last(c)           :=   True;

      end loop;
      pragma Assert(for all p in cpu_number =>
                      (state.min_fp(p) = Minor_Frame_Range'First and
                           state.ideal_maj_fp(p) = Major_Frame_Range'First and
                                      state.enabled(p) = True and state.last(p) = True));

      state.maj_fp               :=   Major_Frame_Range'First;

      state.no_of_last           :=   cpu_number'Last;

      state.L   :=   state.major_frame_ends(Major_Frame_Range'Last).Period;

      pragma Assert(for all c in cpu_number =>
                      (state.min_fp(c) = Minor_Frame_Range'First));

      -- initializing current subject info
      for s in subject_number
      loop
         pragma Loop_Invariant(for all p in 0..s =>
                                 (state.current_subject(p) = False));
         state.current_subject(s) := False;
      end loop;
      pragma Assert(for all s in subject_number =>
                      (state.current_subject(s) = False));

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c =>
                                 (state.current_subject(state.Scheduling_Groups
                                  (state.Scheduling_Plans(p)(state.maj_fp).Minor_Frames
                                      (state.min_fp(p)).Group_ID))));
         declare
            group : Scheduling_Group_Range;
            sub   : subject_number;
         begin
            group := state.Scheduling_Plans(c)(state.maj_fp).Minor_Frames
              (state.min_fp(c)).Group_ID;
            sub := state.Scheduling_Groups(group);
            state.current_subject(sub) := True;
         end;
      end loop;

   end initialize;

end absinit;
