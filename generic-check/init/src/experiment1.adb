package body experiment1
with SPARK_Mode is

   procedure exp1 is
   begin
      for x in ind1
      loop
         A(x).sx := 5;
      end loop;
      pragma Assert(if(for all p in ind2 =>
                         (for all q in ind1 =>
                            (if(A(q).id = p) then
                                 (A(q).value = 5)))) then
                      (for all y in ind1 =>
                         A(y).value = 5));

      for x in ind2
      loop
         pragma Loop_Invariant(for all p in 0..x-1 =>
                                 (for all q in ind1 =>
                                    (if(A(q).id = p) then
                                         (A(q).value = 5))));
         for y in ind1
         loop
            pragma Loop_Invariant(for all p in 0..x =>
                                  (for all r in 0..y-1 =>
                                       (if(A(r).id = p) then
                                            (A(r).value = 5))));
            if(A(y).id = x) then
               A(y).value := 5;
            end if;
         end loop;
         pragma Assert((for all j in ind1 =>
                            (if(A(j).id = x) then
                                 (A(j).value = 5))));
         pragma Assert(for all i in 0..x =>
                         (for all j in ind1 =>
                            (if(A(j).id = i) then
                                 (A(j).value = 5))));
      end loop;

      pragma Assert(for all p in ind2 =>
                      (for all q in ind1 =>
                         (if(A(q).id = p) then
                               (A(q).value = 5))));
      pragma Assert(for all y in ind1 =>
                      A(y).value = 5);

   end exp1;

end experiment1;
