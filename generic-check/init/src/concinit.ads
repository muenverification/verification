with SK; use SK;
with numbers1; use numbers1;
with SK.CPU;
with SK.Barriers;

package concInit
with SPARK_Mode is

   type subject_state_array is array(subject_number) of Subject_State_Type;
  -- type CPU_GenReg_Array_Type is array (cpu_number) of CPU_Registers_Type;
   type CPU_ControlReg_Array_Type is array(cpu_number) of Subject_State_Type;
   type tick_type is array (cpu_number) of SK.Word64;
   --type minor_fp_array is array (cpu_number) of Minor_Frame_Range;
  -- type major_fp_array is array (cpu_number) of Major_Frame_Range;
   type boolean_array is array (cpu_number) of Boolean;


   type Barrier_Config_Array is array (Barrier_Range) of Barrier_Size_Type;
   type Major_Frame_Info_Type is record
      Period         : SK.Word64;
      Barrier_Config : Barrier_Config_Array;
   end record;
   type Major_Frame_Info_Array is array (Major_Frame_Range)
     of Major_Frame_Info_Type;

   type Minor_Frame_Type is record
      Group_ID : Scheduling_Group_Range;
      Barrier  : Barrier_Index_Range;
      Deadline : SK.Word64;
   end record;

   type Minor_Frame_Array is array (Minor_Frame_Range) of Minor_Frame_Type;

   type Major_Frame_Type is record
      Length       : Minor_Frame_Range;
      Minor_Frames : Minor_Frame_Array;
   end record;

   type Major_Frame_Array is array (Major_Frame_Range) of Major_Frame_Type;

   type Scheduling_Plan_Type is array (cpu_number) of Major_Frame_Array;


   --type to define perCPU Storage
   type Scheduling_Group_Array is array (Scheduling_Group_Range)
     of subject_number;

   type Storage_Type is record
      Scheduling_Groups   : Scheduling_Group_Array;
      Current_Minor_Frame : Minor_Frame_Range;
   end record;

   type Storage_Type_Array is array (cpu_number) of Storage_Type;


   type vmcs_array is array (subject_number) of SK.CPU.VMCStype;

   type page_table_index_type is array (cpu_number) of subject_number;

   type Minor_Frame_Barriers_Array is
     array (Barrier_Index_Range) of Barriers.Sense_Barrier_Type;


   -- types to define subject specs
   type VMX_Controls_Type is record
      Exec_Pin    : SK.Word32;
      Exec_Proc   : SK.Word32;
      Exec_Proc2  : SK.Word32;
      Exit_Ctrls  : SK.Word32;
      Entry_Ctrls : SK.Word32;
   end record;

   type Trap_Range is range 0 .. 59;

   type Event_Range is range 0 .. 31;

   Invalid_Subject : constant Integer := Integer(subject_number'Last) + 1;

   subtype Dst_Subject_Type is Natural range 0 .. Invalid_Subject;

   type Vector_Range is range 0 .. 255;

   Invalid_Vector : constant := 256;

   type Dst_Vector_Range is range 0 .. Invalid_Vector;

   type Trap_Entry_Type is record
      Dst_Subject : Dst_Subject_Type;
      Dst_Vector  : Dst_Vector_Range;
   end record;

   type Event_Entry_Type is record
      Dst_Subject : Dst_Subject_Type;
      Dst_Vector  : Dst_Vector_Range;
      Handover    : Boolean;
      Send_IPI    : Boolean;
   end record;

   type Trap_Table_Type is array (Trap_Range) of Trap_Entry_Type;

   type Event_Table_Type is array (Event_Range) of Event_Entry_Type;

   type Subject_Spec_Type is record
      CPU_Id             : cpu_number;
      PML4_Address       : SK.Word64;
      EPT_Pointer        : SK.Word64;
      VMCS_Address       : SK.Word64;
      IO_Bitmap_Address  : SK.Word64;
      MSR_Bitmap_Address : SK.Word64;
      MSR_Store_Address  : SK.Word64;
      Stack_Address      : SK.Word64;
      Entry_Point        : SK.Word64;
      CR0_Value          : SK.Word64;
      CR0_Mask           : SK.Word64;
      CR4_Value          : SK.Word64;
      CR4_Mask           : SK.Word64;
      CS_Access          : SK.Word32;
      Exception_Bitmap   : SK.Word32;
      MSR_Count          : SK.Word32;
      VMX_Controls       : VMX_Controls_Type;
      Trap_Table         : Trap_Table_Type;
      Event_Table        : Event_Table_Type;
   end record;

   type Subject_Spec_Array is array (subject_number) of Subject_Spec_Type;

   type Pseudo_Descriptor_Type is record
      Limit : SK.Word16;
      Base  : SK.Word64;
   end record;

  -- type subject_boolean_array is array (subject_number) of Boolean;

   type system_state is record

      Scheduling_Plans : Scheduling_Plan_Type;
      Subject_Specs : Subject_Spec_Array;
      Major_Frames : Major_Frame_Info_Array;

      Scheduling_Groups: Scheduling_Group_Array;
   -- concrete
      tsc : tick_type;
      vmx_timer : tick_type;
      VMCSs : vmcs_array;
      VMCS_Pointer : page_table_index_type;

      Per_CPU_Storage : Storage_Type_Array;
      Current_Major_Frame : Major_Frame_Range;
      Current_Major_Start_Cycles : SK.Word64;

      All_Barrier : Barriers.Sense_Barrier_Type;
      Minor_Frame_Barriers : Minor_Frame_Barriers_Array;
      Barrier_Set : boolean_array;

      Descriptors: Subject_State_Array;
      --CPU_Regs1 : CPU_GenReg_Array_Type;
      CPU_Regs : CPU_ControlReg_Array_Type;

      IDT_Pointer : Pseudo_Descriptor_Type;
      GDT_Pointer : Pseudo_Descriptor_Type;
   end record;

   state : system_state;


   procedure initialize(state : in out system_state);


end concInit;
