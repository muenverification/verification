-- Remaining work comments starting with **
package body concInit
with SPARK_Mode is

   SEL_KERN_CODE : constant := 16#08#;
   SEL_KERN_DATA : constant := 16#10#;
   SEL_TSS       : constant := 16#18#;
   procedure initialize(state : in out system_state)
   is

   begin


      -- concrete intialization

      for c in cpu_number
      loop
         pragma Loop_Invariant(for all p in 0..c => (state.tsc(p) = 0));
         state.tsc(c) := 0;
      end loop;
      pragma Assert(for all c in cpu_number => (state.tsc(c) = 0));

      -- cpu_global.init - done only once
      for c in cpu_number
      loop

         declare
            Null_Storage : constant Storage_Type := Storage_Type'
              (Scheduling_Groups   => (others => subject_number'First),
               Current_Minor_Frame => Minor_Frame_Range'First);
         begin
            state.Current_Major_Frame        := Major_Frame_Range'First;

            state.Current_Major_Start_Cycles := 0;

            state.Per_CPU_Storage(c)         := Null_Storage;

         end;
      end loop;

      -- scheduler.init - done by each CPU, hence a loop
      for c in cpu_number
      loop
         pragma Loop_Invariant(for all x in 0..c =>
                                 (for all y in subject_number =>
                                    (if(state.Subject_Specs(y).CPU_Id = x) then
                                          (state.Descriptors(y) = Null_Subject_State))));
         declare
            Now                : constant SK.Word64 := state.tsc(c);
            Initial_Subject_ID : subject_number;
            Initial_VMCS_Addr  : SK.Word64 := 0;
            Controls           : VMX_Controls_Type;
            --VMCS_Addr          : SK.Word64;
           -- PD                 : Pseudo_Descriptor_Type;
         begin
--              CPU_Global.Set_Scheduling_Groups -- modify it for our setting
--                (Data => Skp.Scheduling.Scheduling_Groups);
            state.Per_CPU_Storage(c).Scheduling_Groups := state.Scheduling_Groups;

--              Initial_Subject_ID := CPU_Global.Get_Current_Subject_ID; --replace the functions body here
            Initial_Subject_ID := state.Per_CPU_Storage(c).Scheduling_Groups
              (state.Scheduling_Plans (c)(state.Current_Major_Frame).Minor_Frames
               (state.Per_CPU_Storage(c).Current_Minor_Frame).Group_ID);
--                (Skp.Scheduling.Get_Group_ID -- make this function in line
--                   (CPU_ID   => CPU_ID,
--                    Major_ID => Current_Major_Frame,
--                    Minor_ID => Per_CPU_Storage.Current_Minor_Frame));

            --  Setup VMCS and state of subjects running on this logical CPU.

            for I in subject_number
            loop
--                 pragma Loop_Invariant(for all p in 0..I =>
--                                         (if(state.Subject_Specs(p).CPU_Id = c) then
--                                            (state.Descriptors(p) = Null_Subject_State)));
--                 if Skp.Subjects.Get_CPU_Id (Subject_Id => I) = CPU_Global.CPU_ID then --replace the functions body here
               --pragma Loop_Invariant(state.Descriptors(I).Regs)
               if state.Subject_Specs(I).CPU_Id = c then
                  --  Initialize subject state.

--                    Subjects.Clear_State (Id => I);
                  state.Descriptors (I) := SK.Null_Subject_State;

                  --  Initialize subject timer.

--                    Timers.Init_Timer (Subject => I);

                  --  VMCS

--                    VMCS_Addr := Skp.Subjects.Get_VMCS_Address (Subject_Id => I);
--                    VMCS_Addr := state.Subject_Specs(I).VMCS_Address;
--                    Controls  := Skp.Subjects.Get_VMX_Controls (Subject_Id => I);
                  Controls  := state.Subject_Specs(I).VMX_Controls;

--                    VMX.Clear (VMCS_Address => VMCS_Addr);
--                    VMX.Load  (VMCS_Address => VMCS_Addr);
                  state.VMCS_Pointer(c) := I;
--                    VMX.VMCS_Setup_Control_Fields
--                      (IO_Bitmap_Address  => state.Subject_Specs (I).IO_Bitmap_Address,
--                       MSR_Bitmap_Address => state.Subject_Specs (I).MSR_Bitmap_Address,
--                       MSR_Store_Address  => state.Subject_Specs (I).MSR_Store_Address,
--                       MSR_Count          => state.Subject_Specs (I).MSR_Count,
--                       Ctls_Exec_Pin      => Controls.Exec_Pin,
--                       Ctls_Exec_Proc     => Controls.Exec_Proc,
--                       Ctls_Exec_Proc2    => Controls.Exec_Proc2,
--                       Ctls_Exit          => Controls.Exit_Ctrls,
--                       Ctls_Entry         => Controls.Entry_Ctrls,
--                       CR0_Mask           => state.Subject_Specs (I).CR0_Mask,
--                       CR4_Mask           => state.Subject_Specs (I).CR4_Mask,
--                       Exception_Bitmap   => state.Subject_Specs (I).Exception_Bitmap);

--                    VMX.VMCS_Setup_Host_Fields;
-- adding vmwrites for setup host fields
--                    VMCS_Write (Field => Constants.HOST_SEL_CS,
--                                Value => SEL_KERN_CODE);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_CS :=
--                      SEL_KERN_CODE;
--                    VMCS_Write (Field => Constants.HOST_SEL_DS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_DS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_ES,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_ES :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_SS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_SS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_FS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_FS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_GS,
--                                Value => SEL_KERN_DATA);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_GS :=
--                      SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.HOST_SEL_TR,
--                                Value => SEL_TSS);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_SEL_TR :=
--                      SEL_TSS;

--                    VMCS_Write (Field => Constants.HOST_CR0,
--                                Value => CPU.Get_CR0);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_CR0 :=
--                      state.CPU_Regs2(c).CR0; --see where CR0 is setup in initialization
--                    VMCS_Write (Field => Constants.CR0_READ_SHADOW,
--                                Value => CPU.Get_CR0);
--                    state.VMCSs(state.VMCS_Pointer(c)).CR0_READ_SHADOW :=
--                      state.CPU_Regs2(c).CR0;
--                    VMCS_Write (Field => Constants.HOST_CR3,
--                                Value => CPU.Get_CR3);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_CR3 :=
--                      state.CPU_Regs2(c).CR3;
--                    VMCS_Write (Field => Constants.HOST_CR4,
--                                Value => CPU.Get_CR4);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_CR4 :=
--                      state.CPU_Regs2(c).CR4;

--                    PD := Interrupts.Get_IDT_Pointer;
--                    PD := state.IDT_Pointer;
--                    VMCS_Write (Field => Constants.HOST_BASE_IDTR,
--                                Value => PD.Base);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_IDTR :=
--                      PD.Base;
--                    PD := GDT.GDT_Pointer;
--                    PD := state.GDT_Pointer;
--                    VMCS_Write (Field => Constants.HOST_BASE_GDTR,
--                                Value => PD.Base);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_GDTR :=
--                      PD.Base;

--                    VMCS_Write (Field => Constants.HOST_BASE_FS,
--                                Value => 0);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_FS := 0;
--                    VMCS_Write (Field => Constants.HOST_BASE_GS,
--                                Value => 0);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_BASE_GS := 0;
--
--                    VMCS_Write (Field => Constants.HOST_RSP,
--                                Value => Skp.Kernel.Stack_Address);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_RSP :=
--                      Skp.Kernel.Stack_Address;
--                    VMCS_Write (Field => Constants.HOST_RIP,
--                                Value => VMX_Exit_Address);
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_RIP :=
--                      VMX_Exit_Address;
--                    VMCS_Write (Field => Constants.HOST_IA32_EFER,
--                                Value => CPU.Get_MSR64 (Register => Constants.IA32_EFER));
--                    state.VMCSs(state.VMCS_Pointer(c)).HOST_IA32_EFER :=
--                      MSR;
-- ending vmwrites for setup host fields

--                    VMX.VMCS_Setup_Guest_Fields
--                      (PML4_Address => state.Subject_Specs (I).PML4_Address,
--                       EPT_Pointer  => state.Subject_Specs (I).EPT_Pointer,
--                       CR0_Value    => state.Subject_Specs (I).CR0_Value,
--                       CR4_Value    => state.Subject_Specs (I).CR4_Value,
--                       CS_Access    => state.Subject_Specs (I).CS_Access);

-- Adding vmwrites of setup guest fields below
--                    VMCS_Write (Field => Constants.VMCS_LINK_POINTER,
--                                Value => SK.Word64'Last);
--                    state.VMCSs(state.VMCS_Pointer(c)).VMCS_LINK_POINTER :=
--                      SK.Word64'Last;
--                    VMCS_Write (Field => Constants.GUEST_SEL_CS,
--                                Value => SEL_KERN_CODE);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_CS :=
                    SEL_KERN_CODE;
--                    VMCS_Write (Field => Constants.GUEST_SEL_DS,
--                                Value => SEL_KERN_DATA);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_DS :=
                    SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.GUEST_SEL_ES,
--                                Value => SEL_KERN_DATA);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_ES :=
                    SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.GUEST_SEL_SS,
--                                Value => SEL_KERN_DATA);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_SS :=
                    SEL_KERN_DATA;
--                    VMCS_Write (Field => Constants.GUEST_SEL_TR,
--                                Value => SEL_TSS);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_SEL_TR :=
                    SEL_TSS;
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_CS,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_CS :=
                    SK.Word32'Last; -- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_DS,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_DS :=
                    SK.Word32'Last;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_ES,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_ES :=
                    SK.Word32'Last;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_SS,
--                                Value => SK.Word64 (SK.Word32'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_SS :=
                    SK.Word32'Last;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_LIMIT_TR,
--                                Value => SK.Word64 (SK.Byte'Last));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_LIMIT_TR :=
                    SK.Word32(SK.Byte'Last);-- they are writing 64 bit word in a 32 bit field

--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_CS,
--                                Value => SK.Word64 (CS_Access));
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_CS :=
                    state.Subject_Specs (I).CS_Access;-- they are writing 64 bit word in a 32 bit field
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_DS,
--                                Value => 16#c093#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_DS :=
                    16#c093#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_ES,
--                                Value => 16#c093#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_ES :=
                    16#c093#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_SS,
--                                Value => 16#c093#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_SS :=
                    16#c093#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_TR,
--                                Value => 16#8b#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_TR :=
                    16#8b#;

                  --  Disable fs, gs and ldt segments; they can be enabled by guest code
                  --  if needed.

--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_FS,
--                                Value => 16#10000#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_FS :=
                    16#10000#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_GS,
--                                Value => 16#10000#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_GS :=
                    16#10000#;
--                    VMCS_Write (Field => Constants.GUEST_ACCESS_RIGHTS_LDTR,
--                                Value => 16#10000#);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_ACCESS_RIGHTS_LDTR :=
                    16#10000#;

--                    VMCS_Write (Field => Constants.GUEST_CR0,
--                                Value => CR0_Value);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR0 :=
                    state.Subject_Specs (I).CR0_Value;
--                    VMCS_Write (Field => Constants.GUEST_CR3,
--                                Value => PML4_Address);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR3 :=
                    state.Subject_Specs (I).PML4_Address;
--                    VMCS_Write (Field => Constants.GUEST_CR4,
--                                Value => CR4_Value);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR4 :=
                    state.Subject_Specs (I).CR4_Value;

--                    VMCS_Write (Field => Constants.EPT_POINTER,
--                                Value => EPT_Pointer);
                  state.VMCSs(state.VMCS_Pointer(c)).EPT_POINTER :=
                    state.Subject_Specs (I).EPT_Pointer;

--                    VMCS_Write (Field => Constants.GUEST_RFLAGS,
--                                Value => 2);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_RFLAGS := 2;
--                    VMCS_Write (Field => Constants.GUEST_IA32_EFER,
--                                Value => 0);
                  state.VMCSs(state.VMCS_Pointer(c)).GUEST_IA32_EFER := 0;
-- ending vmwrites of setup guest fields

--                    if Initial_Subject_ID = I then
--                       Initial_VMCS_Addr := VMCS_Addr;
--                       Subjects_Sinfo.Export_Scheduling_Info
--                         (Id                 => I,
--                          TSC_Schedule_Start => Now,
--                          TSC_Schedule_End   => Now + Skp.Scheduling.Get_Deadline
--                            (CPU_ID   => CPU_Global.CPU_ID,
--                             Major_ID => Skp.Scheduling.Major_Frame_Range'First,
--                             Minor_ID => Skp.Scheduling.Minor_Frame_Range'First));
--                    end if;

                  --  State
               end if;
            end loop;
         end;
      end loop;

      for c in cpu_number
      loop
           declare
            Now                : constant SK.Word64 := state.tsc(c);
            Initial_Subject_ID : subject_number;
            Initial_VMCS_Addr  : SK.Word64 := 0;
            --Controls           : VMX_Controls_Type;
            --VMCS_Addr          : SK.Word64;
            --PD                 : Pseudo_Descriptor_Type;
         begin
            --              CPU_Global.Set_Scheduling_Groups -- modify it for our setting
--                (Data => Skp.Scheduling.Scheduling_Groups);
            state.Per_CPU_Storage(c).Scheduling_Groups := state.Scheduling_Groups;

            --              Initial_Subject_ID := CPU_Global.Get_Current_Subject_ID; --replace the functions body here
            Initial_Subject_ID := state.Per_CPU_Storage(c).Scheduling_Groups
              (state.Scheduling_Plans (c)(state.Current_Major_Frame).Minor_Frames
               (state.Per_CPU_Storage(c).Current_Minor_Frame).Group_ID);
            for I in subject_number
            loop
--                 pragma Loop_Invariant(for all p in 0..I =>
--                                         (if(state.Subject_Specs(p).CPU_Id = c) then
--                                            (state.Descriptors(p).RIP =
--                                                 state.Subject_Specs(p).Entry_Point and
--                                                 state.Descriptors(p).RSP =
--                                                 state.Subject_Specs(p).Stack_Address and
--                                                 state.Descriptors(p).CR0 =
--                                                 state.Subject_Specs(p).CR0_Value)));
               if state.Subject_Specs(I).CPU_Id = c then

                  --                    Subjects.Set_RIP
                  --                      (Id    => I,
                  --                       Value => Skp.Subjects.Get_Entry_Point (Subject_Id => I));
                  state.Descriptors(I).RIP := state.Subject_Specs(I).Entry_Point;
--                    Subjects.Set_RSP
--                      (Id    => I,
--                       Value => Skp.Subjects.Get_Stack_Address (Subject_Id => I));
                  state.Descriptors(I).RSP := state.Subject_Specs(I).Stack_Address;
                  --                    Subjects.Set_CR0
                  --                      (Id    => I,
                  --                       Value => Skp.Subjects.Get_CR0 (Subject_Id => I));
                  state.Descriptors(I).CR0 := state.Subject_Specs(I).CR0_Value;
               end if;
            end loop;

            --  Load first subject and set preemption timer ticks.

            --              VMX.Load (VMCS_Address => Initial_VMCS_Addr);
            state.VMCS_Pointer(c) := Initial_Subject_ID;


         end;
      end loop;

      for c in cpu_number
      loop

         declare
            Now      : constant SK.Word64 := state.tsc(c);
            Deadline : SK.Word64;
            Cycles   : SK.Word64;
         begin
--              pragma Assert(state.Scheduling_Plans(c)(state.Current_Major_Frame).Minor_Frames
--                (state.Per_CPU_Storage(c).Current_Minor_Frame).Deadline <= SK.Word64(SK.Word32'Last));
            Deadline := state.Current_Major_Start_Cycles +
              state.Scheduling_Plans(c)(state.Current_Major_Frame).Minor_Frames
              (state.Per_CPU_Storage(c).Current_Minor_Frame).Deadline;
--                  (CPU_ID   => CPU_Global.CPU_ID,
--                   Major_ID => CPU_Global.Get_Current_Major_Frame_ID,
--                   Minor_ID => CPU_Global.Get_Current_Minor_Frame_ID);

            if Deadline > Now then
               Cycles := Deadline - Now;
            else
               Cycles := 0;
            end if;
            pragma Assert(Cycles = state.Scheduling_Plans(c)
                          (state.Current_Major_Frame).Minor_Frames(state.Per_CPU_Storage
                            (c).Current_Minor_Frame).Deadline);
--              pragma Assert(Cycles <= SK.Word64(SK.Word32'Last));
--              VMX.VMCS_Write (Field => Constants.GUEST_VMX_PREEMPT_TIMER,
--                              Value => Cycles / 2 ** Skp.Scheduling.VMX_Timer_Rate);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER :=
              SK.Word32(Cycles);
--              state.vmx_timer(c) := SK.Word64(state.VMCSs(state.VMCS_Pointer(c)).GUEST_VMX_PREEMPT_TIMER);
            state.vmx_timer(c) := Cycles;

         end;
      end loop;

      -- subjects.restore_state - done by each CPU, hence a loop
      for c in cpu_number
      loop
         declare
            Id   : subject_number ;
         begin
            Id := state.Per_CPU_Storage(c).Scheduling_Groups
              (state.Scheduling_Plans (c)(state.Current_Major_Frame).Minor_Frames
               (state.Per_CPU_Storage(c).Current_Minor_Frame).Group_ID);
--              VMX.VMCS_Write (Field => Constants.GUEST_RIP,
--                              Value => Descriptors (Id).RIP);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_RIP := state.Descriptors(Id).RIP;
--              VMX.VMCS_Write (Field => Constants.GUEST_RSP,
--                              Value => Descriptors (Id).RSP);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_RSP := state.Descriptors(Id).RSP;
--              VMX.VMCS_Write (Field => Constants.GUEST_CR0,
--                              Value => Descriptors (Id).CR0);
            state.VMCSs(state.VMCS_Pointer(c)).GUEST_CR0 := state.Descriptors(Id).CR0;
--              VMX.VMCS_Write (Field => Constants.CR0_READ_SHADOW,
--                              Value => Descriptors (Id).SHADOW_CR0);
            state.VMCSs(state.VMCS_Pointer(c)).CR0_READ_SHADOW :=
              state.Descriptors(Id).SHADOW_CR0;
            state.CPU_Regs(c).Regs := state.Descriptors (Id).Regs;
         end;
      end loop;


   end initialize;

end concInit;
