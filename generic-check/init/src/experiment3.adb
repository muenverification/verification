package body experiment3
with SPARK_Mode is

   procedure havoc with
      SPARK_Mode => Off is
   begin
      null;
   end havoc;

   procedure exp3 is
   begin
      --*********** Outer loop *************

      for x in ind2
      loop
         pragma Loop_Invariant(for all p in 0..x-1 =>
                                 (for all q in ind1 =>
                                    (if(A(q).id = p) then
                                         (A(q).value = 5))));
         havoc;
         pragma Assume(for all p in 0..x =>
                       (for all r in ind1 =>
                          (if(A(r).id = p) then
                            (A(r).value = 5))));
      end loop;

      pragma Assert(for all p in ind2 =>
                      (for all q in ind1 =>
                         (if(A(q).id = p) then
                              (A(q).value = 5))));
      pragma Assert(for all y in ind1 =>
                      A(y).value = 5);

   end exp3;

end experiment3;
