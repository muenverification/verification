package experiment3
with SPARK_Mode is

   type ind1 is new Integer range 0..3;
   type ind2 is new Integer range 0..16;

   type bigtype is record
      value : Integer;
      id    : ind2;
   end record;

   type bigtype_array is array (ind1) of bigtype;

   A: bigtype_array;

   procedure exp3;

   procedure havoc with
      Global => (In_Out => A);

end experiment3;
