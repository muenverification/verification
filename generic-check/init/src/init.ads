with SK; use SK;
with numbers1; use numbers1;
with SK.CPU;
with SK.Barriers;

package initialization1
with SPARK_Mode is

   type subject_state_array is array(subject_number) of Subject_State_Type;
   type CPU_GenReg_Array_Type is array (cpu_number) of CPU_Registers_Type;
   type CPU_ControlReg_Array_Type is array(cpu_number) of Subject_State_Type;
   type tick_type is array (cpu_number) of SK.Word64;
   type minor_fp_array is array (cpu_number) of Minor_Frame_Range;
   type major_fp_array is array (cpu_number) of Major_Frame_Range;
   type boolean_array is array (cpu_number) of Boolean;


   type Barrier_Config_Array is array (Barrier_Range) of Barrier_Size_Type;
   type Major_Frame_Info_Type is record
      Period         : SK.Word64;
      Barrier_Config : Barrier_Config_Array;
   end record;
   type Major_Frame_Info_Array is array (Major_Frame_Range)
     of Major_Frame_Info_Type;

   type Minor_Frame_Type is record
      Group_ID : Scheduling_Group_Range;
      Barrier  : Barrier_Index_Range;
      Deadline : SK.Word64;
   end record;

   type Minor_Frame_Array is array (Minor_Frame_Range) of Minor_Frame_Type;

   type Major_Frame_Type is record
      Length       : Minor_Frame_Range;
      Minor_Frames : Minor_Frame_Array;
   end record;

   type Major_Frame_Array is array (Major_Frame_Range) of Major_Frame_Type;

   type Scheduling_Plan_Type is array (cpu_number) of Major_Frame_Array;

   --type to define perCPU Storage
   type Scheduling_Group_Array is array (Scheduling_Group_Range)
     of subject_number;

   type Storage_Type is record
      Scheduling_Groups   : Scheduling_Group_Array;
      Current_Minor_Frame : Minor_Frame_Range;
   end record;

   type Storage_Type_Array is array (cpu_number) of Storage_Type;


   type vmcs_array is array (subject_number) of SK.CPU.VMCStype;

   type page_table_index_type is array (cpu_number) of subject_number;

   type Minor_Frame_Barriers_Array is
     array (Barrier_Index_Range) of Barriers.Sense_Barrier_Type;


   -- types to define subject specs
   type VMX_Controls_Type is record
      Exec_Pin    : SK.Word32;
      Exec_Proc   : SK.Word32;
      Exec_Proc2  : SK.Word32;
      Exit_Ctrls  : SK.Word32;
      Entry_Ctrls : SK.Word32;
   end record;

   type Trap_Range is range 0 .. 59;

   type Event_Range is range 0 .. 31;

   Invalid_Subject : constant Integer := Integer(subject_number'Last) + 1;

   subtype Dst_Subject_Type is Natural range 0 .. Invalid_Subject;

   type Vector_Range is range 0 .. 255;

   Invalid_Vector : constant := 256;

   type Dst_Vector_Range is range 0 .. Invalid_Vector;

   type Trap_Entry_Type is record
      Dst_Subject : Dst_Subject_Type;
      Dst_Vector  : Dst_Vector_Range;
   end record;

   type Event_Entry_Type is record
      Dst_Subject : Dst_Subject_Type;
      Dst_Vector  : Dst_Vector_Range;
      Handover    : Boolean;
      Send_IPI    : Boolean;
   end record;

   type Trap_Table_Type is array (Trap_Range) of Trap_Entry_Type;

   type Event_Table_Type is array (Event_Range) of Event_Entry_Type;

   type Subject_Spec_Type is record
      CPU_Id             : cpu_number;
      PML4_Address       : SK.Word64;
      EPT_Pointer        : SK.Word64;
      VMCS_Address       : SK.Word64;
      IO_Bitmap_Address  : SK.Word64;
      MSR_Bitmap_Address : SK.Word64;
      MSR_Store_Address  : SK.Word64;
      Stack_Address      : SK.Word64;
      Entry_Point        : SK.Word64;
      CR0_Value          : SK.Word64;
      CR0_Mask           : SK.Word64;
      CR4_Value          : SK.Word64;
      CR4_Mask           : SK.Word64;
      CS_Access          : SK.Word32;
      Exception_Bitmap   : SK.Word32;
      MSR_Count          : SK.Word32;
      VMX_Controls       : VMX_Controls_Type;
      Trap_Table         : Trap_Table_Type;
      Event_Table        : Event_Table_Type;
   end record;

   type Subject_Spec_Array is array (subject_number) of Subject_Spec_Type;

   type Pseudo_Descriptor_Type is record
      Limit : SK.Word16;
      Base  : SK.Word64;
   end record;

   type subject_boolean_array is array (subject_number) of Boolean;

   type system_state is record

      Scheduling_Plans : Scheduling_Plan_Type;
      Subject_Specs : Subject_Spec_Array;
      Major_Frames : Major_Frame_Info_Array;

   -- abstract
      Scheduling_Groups: Scheduling_Group_Array;
      subjects_state   : subject_state_array;

      ticks            : tick_type;
      min_ticks        : tick_type;
      cycles           : SK.Word64;
      ideal_cycles     : tick_type;

      min_fp           : minor_fp_array;
      maj_fp           : Major_Frame_Range;
      ideal_maj_fp     : major_fp_array;

      L                : SK.Word64;
      major_frame_ends : Major_Frame_Info_Array;
      enabled          : boolean_array;
      last             : boolean_array;
      no_of_last       : cpu_number;

      current_subject  : subject_boolean_array;
      pending_event_array: pending_event_array_type;


   -- concrete
      tsc : tick_type;
      vmx_timer : tick_type;
      VMCSs : vmcs_array;
      VMCS_Pointer : page_table_index_type;

      Per_CPU_Storage : Storage_Type_Array;
      Current_Major_Frame : Major_Frame_Range;
      Current_Major_Start_Cycles : SK.Word64;

      All_Barrier : Barriers.Sense_Barrier_Type;
      Minor_Frame_Barriers : Minor_Frame_Barriers_Array;
      Barrier_Set : boolean_array;

      Descriptors: Subject_State_Array;
      --CPU_Regs1 : CPU_GenReg_Array_Type;
      CPU_Regs : CPU_ControlReg_Array_Type;

      IDT_Pointer : Pseudo_Descriptor_Type;
      GDT_Pointer : Pseudo_Descriptor_Type;
   end record;

   state : system_state;


   procedure initialize(state : in out system_state) with
   Pre => assumptions(state),
   Post => inv_holds(state);

   function assumptions(st: system_state) return Boolean is
     (
      -- 1 - deadlines of minor frames within a major frame are all > 0
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min in 1..st.Scheduling_Plans(c)(maj).Length =>
                     (st.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline > 0))))

      and

      -- 2 - deadline of minor frames in a major frame are strictly increasing
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min in 1..st.Scheduling_Plans(c)(maj).Length =>
                     (if(min/=1) then
                          (st.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline >
                                 st.Scheduling_Plans(c)(maj).Minor_Frames(min-1).Deadline)))))

      and

      -- 3 - every value in major frame ends and major_frames are > 0
        (for all maj in Major_Frame_Range =>
             ((st.major_frame_ends(maj).Period > 0) and
                  (st.Major_Frames(maj).Period > 0)))

      and

      -- 4 - major_frame_ends values are strictly increasing
        (for all maj in Major_Frame_Range =>
             (if (maj/=Major_Frame_Range'First) then
                (st.major_frame_ends(maj).Period >
                     st.major_frame_ends(maj - 1).Period)))

      and

      -- 5 - relationship between major_frames and scheduling_plans
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (st.Major_Frames(maj).Period =
                         st.Scheduling_Plans(c)
                   (maj).Minor_Frames(st.Scheduling_Plans(c)(maj).Length).Deadline
                  )))

      and

      -- 6A - relationship between major_frame_ends and major_frames (a)
        (for all maj in Major_Frame_Range =>
             (if (maj /= 0) then
                (st.Major_Frames(maj).Period =
                 (st.major_frame_ends(maj).Period -
                    st.major_frame_ends(maj - 1).Period))))

      and

      -- 6B - relationship between major_frame_ends and major_frames (b)
        (st.Major_Frames(Major_Frame_Range'First).Period =
             st.major_frame_ends(Major_Frame_Range'First).Period)

      and

      -- 7 - L = major_frame_ends(Major_Frame_Range'Last)
        (st.L = st.major_frame_ends(Major_Frame_Range'Last).Period)

      and

      -- 8A - monotonicity of deadlines of minor frames within a major frame (a)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(min1 < min2) then
                             (st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                    st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                        )))))

      and

      -- 8B - monotonicity of deadlines of minor frames within a major frame (b)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(min1 = min2) then
                             (st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                    st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                        )))))

      and

      -- 8C - monotonicity of deadlines of minor frames within a major frame (c)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                             (min1 < min2)
                        )))))

      and

      -- 8D - monotonicity of deadlines of minor frames within a major frame (d)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                             (min1 = min2)
                        )))))

      and

      -- 9A - monotonicity of values in major_frame_ends
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(maj1 < maj2) then
                     (st.major_frame_ends(maj1).Period < st.major_frame_ends(maj2).Period))))

      and

      -- 9B
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(maj1 = maj2) then
                     (st.major_frame_ends(maj1).Period = st.major_frame_ends(maj2).Period))))

      and

      -- 9C
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(st.major_frame_ends(maj1).Period < st.major_frame_ends(maj2).Period) then
                     (maj1 < maj2))))

      and

      -- 9D
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(st.major_frame_ends(maj1).Period = st.major_frame_ends(maj2).Period) then
                     (maj1 = maj2))))

      and

      -- 10
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min in Minor_Frame_Range =>
                     (st.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline <=
                          SK.Word64(SK.Word32'Last)))))
     );

   function inv_holds(st: system_state) return Boolean is
     (
      -- Abstract

      -- 1 - ideal cycles always >= cycles
        (for all c in cpu_number =>
             (st.ideal_cycles(c) >= st.cycles))

      and

      -- 2 - maj_fp = least of ideal_maj_fp's
        (for all c in cpu_number =>
             (if (st.last(c) = True) then
                  (st.maj_fp = st.ideal_maj_fp(c) and
                         st.cycles = st.ideal_cycles(c))))

      and

      -- 3 - min_ticks < deadline of the last minor frame of the major frame
        (for all c in cpu_number =>
             (st.min_ticks(c) <
                    st.Scheduling_Plans(c)(st.ideal_maj_fp(c)).Minor_Frames
              (st.Scheduling_Plans(c)(st.ideal_maj_fp(c)).Length).Deadline))

      and

      -- 4 - ticks < L
        (for all c in cpu_number =>
             (st.ticks(c) < st.L))

      and

      -- 5 - min_ticks <= ticks
        (for all c in cpu_number =>
             (st.min_ticks(c) <= st.ticks(c)))

      and

      -- 6 - min_fp < number of minor frames in the major frame
        (for all c in cpu_number =>
             (st.min_fp(c) <=
                    st.Scheduling_Plans(c)(st.ideal_maj_fp(c)).Length))

      and

      -- 7 - relationship between min_ticks, ticks and ideal_maj_fp
        (for all c in cpu_number =>
             (if(st.ideal_maj_fp(c) /= 0) then
                  (st.ticks(c) =
                   (st.major_frame_ends(st.ideal_maj_fp(c) - 1).Period +
                      st.min_ticks(c)))
              else
                (st.ticks(c) = st.min_ticks(c))))

      and

      -- 8 - deadlines of minor frames within a major frame are all > 0
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min in 1..st.Scheduling_Plans(c)(maj).Length =>
                     (st.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline > 0))))

      and

      -- 9 - deadline of minor frames in a major frame are strictly increasing
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min in 1..st.Scheduling_Plans(c)(maj).Length =>
                     (if(min/=1) then
                          (st.Scheduling_Plans(c)(maj).Minor_Frames(min).Deadline >
                                 st.Scheduling_Plans(c)(maj).Minor_Frames(min-1).Deadline)))))

      and

      -- 10 - every value in major frame ends and major_frames are > 0
        (for all maj in Major_Frame_Range =>
             ((st.major_frame_ends(maj).Period > 0) and
                  (st.Major_Frames(maj).Period > 0)))

      and

      -- 11 - major_frame_ends values are strictly increasing
        (for all maj in Major_Frame_Range =>
             (if (maj/=Major_Frame_Range'First) then
                (st.major_frame_ends(maj).Period >
                     st.major_frame_ends(maj - 1).Period)))

      and

      -- 12 - relationship between major_frames and scheduling_plans
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (st.Major_Frames(maj).Period =
                         st.Scheduling_Plans(c)
                   (maj).Minor_Frames(st.Scheduling_Plans(c)(maj).Length).Deadline
                  )))

      and

      -- 13A - relationship between major_frame_ends and major_frames (a)
        (for all maj in Major_Frame_Range =>
             (if (maj /= 0) then
                (st.Major_Frames(maj).Period =
                 (st.major_frame_ends(maj).Period -
                    st.major_frame_ends(maj - 1).Period))))

      and

      -- 13B - relationship between major_frame_ends and major_frames (b)
        (st.Major_Frames(Major_Frame_Range'First).Period =
             st.major_frame_ends(Major_Frame_Range'First).Period)

      and

      -- 14 - L = major_frame_ends(Major_Frame_Range'Last)
        (st.L = st.major_frame_ends(Major_Frame_Range'Last).Period)

      and

      -- 15 - ticks should be between two major frame end values
        (for all c in cpu_number =>
             (if(st.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                  ((st.ticks(c) <
                      st.major_frame_ends(st.ideal_maj_fp(c)).Period)
                   and
                     (st.ticks(c) >=
                          st.major_frame_ends(st.ideal_maj_fp(c) - 1).Period))
              else
                ((st.ticks(c) <
                    st.major_frame_ends(st.ideal_maj_fp(c)).Period)
                 and
                   (st.ticks(c) >= 0))))

      and

      -- 16 - min_ticks should be between two minor frame deadlines
        (for all c in cpu_number =>
             (if (st.min_fp(c) /= Minor_Frame_Range'First) then
                  ((st.min_ticks(c) <
                      st.Scheduling_Plans(c)
                    (st.ideal_maj_fp(c)).Minor_Frames(st.min_fp(c)).Deadline)
                   and
                     (st.min_ticks(c) >=
                          st.Scheduling_Plans(c)
                      (st.ideal_maj_fp(c)).Minor_Frames(st.min_fp(c) - 1).Deadline))
              else
                ((st.min_ticks(c) <
                    st.Scheduling_Plans(c)
                  (st.ideal_maj_fp(c)).Minor_Frames(st.min_fp(c)).Deadline)
                 and
                   (st.min_ticks(c) >= 0))
             ))

      and

      -- 17A - monotonicity of deadlines of minor frames within a major frame (a)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(min1 < min2) then
                             (st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                    st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                        )))))

      and

      -- 17B - monotonicity of deadlines of minor frames within a major frame (b)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(min1 = min2) then
                             (st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                    st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline)
                        )))))

      and

      -- 17C - monotonicity of deadlines of minor frames within a major frame (c)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline <
                                st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                             (min1 < min2)
                        )))))

      and

      -- 17D - monotonicity of deadlines of minor frames within a major frame (d)
        (for all c in cpu_number =>
             (for all maj in Major_Frame_Range =>
                  (for all min1 in Minor_Frame_Range =>
                     (for all min2 in Minor_Frame_Range =>
                        (if(st.Scheduling_Plans(c)(maj).Minor_Frames(min1).Deadline =
                                st.Scheduling_Plans(c)(maj).Minor_Frames(min2).Deadline) then
                             (min1 = min2)
                        )))))

      and

      -- 18A - monotonicity of values in major_frame_ends
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(maj1 < maj2) then
                     (st.major_frame_ends(maj1).Period < st.major_frame_ends(maj2).Period))))

      and

      --
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(maj1 = maj2) then
                     (st.major_frame_ends(maj1).Period = st.major_frame_ends(maj2).Period))))

      and

      --
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(st.major_frame_ends(maj1).Period < st.major_frame_ends(maj2).Period) then
                     (maj1 < maj2))))

      and

      --
        (for all maj1 in Major_Frame_Range =>
             (for all maj2 in Major_Frame_Range =>
                  (if(st.major_frame_ends(maj1).Period = st.major_frame_ends(maj2).Period) then
                     (maj1 = maj2))))

      and

      -- 19 - all the CPUs can never be in barrier
        (for some c in cpu_number =>
             (st.ideal_cycles(c) = st.cycles and
                    st.ideal_maj_fp(c) = st.maj_fp))

      and

      -- 20 - if cycles = ideal_cycles then ideal_maj_fp >= maj_fp
        (for all c in cpu_number =>
             (if(st.ideal_cycles(c) = st.cycles) then
                  (st.ideal_maj_fp(c) >= st.maj_fp)))

      and

      -- 21A - enabled <=> maj_fp = ideal_maj_fp and cycles = ideal_cycles
        (for all c in cpu_number =>
             (if(st.ideal_cycles(c) = st.cycles and
                     st.ideal_maj_fp(c) = st.maj_fp) then
                  (st.enabled(c) = True)))

      and

      -- 21B -
        (for all c in cpu_number =>
             (if(st.enabled(c) = True) then
                  (st.ideal_cycles(c) = st.cycles and
                         st.ideal_maj_fp(c) = st.maj_fp)
              else
                (st.ideal_cycles(c) /= st.cycles or
                     st.maj_fp /= st.ideal_maj_fp(c))))

      and



      -- Concrete

      -- 1 - least of the tsc's will be in the current major frame
        (for all c in cpu_number =>
             (if(st.last(c) = True) then
                  (st.tsc(c) <=
                   (st.Current_Major_Start_Cycles +
                          st.Major_Frames(st.Current_Major_Frame).Period))))

      and

      -- 2 - all the CPUs can never be in the barrier
        (for some c in cpu_number =>
             (st.Barrier_Set(c) = False))

      and



      -- Gluing

      -- 1 - relationship between tsc, cmsc, ticks, min_ticks
        (for all c in cpu_number =>
             (if(st.ideal_cycles(c) = st.cycles) then
                  (if(st.ideal_maj_fp(c) /= Major_Frame_Range'First and
                        st.maj_fp /= Major_Frame_Range'First) then
                       ((st.tsc(c) - st.Current_Major_Start_Cycles) =
                        (st.major_frame_ends(st.ideal_maj_fp(c) - 1).Period -
                           st.major_frame_ends(st.maj_fp - 1).Period +
                           st.min_ticks(c)))
                   else (if(st.maj_fp = Major_Frame_Range'First and
                              st.ideal_maj_fp(c) /= Major_Frame_Range'First) then
                           ((st.tsc(c) - st.Current_Major_Start_Cycles) =
                            (st.major_frame_ends(st.ideal_maj_fp(c) - 1).Period +
                                 st.min_ticks(c)))
                         else (if(st.maj_fp = Major_Frame_Range'First and
                                    st.ideal_maj_fp(c) = Major_Frame_Range'First) then
                                 ((st.tsc(c) - st.Current_Major_Start_Cycles) =
                                  (st.min_ticks(c)))
                              )))
              else
                (if(st.maj_fp /= Major_Frame_Range'First) then
                     ((st.tsc(c) - st.Current_Major_Start_Cycles) =
                      (st.major_frame_ends(Major_Frame_Range'Last).Period -
                         st.major_frame_ends(st.maj_fp - 1).Period +
                       (st.ideal_cycles(c) - st.cycles) * st.L +
                         st.ticks(c)))
                 else (if(st.maj_fp = Major_Frame_Range'First) then
                         ((st.tsc(c) - st.Current_Major_Start_Cycles) =
                          (st.major_frame_ends(Major_Frame_Range'Last).Period +
                           (st.ideal_cycles(c) - st.cycles) * st.L +
                             st.ticks(c)))
                      ))))

      and

      -- 2 - relationship between maj_fp and Current_Major_Frame
        (st.maj_fp = st.Current_Major_Frame)

      and

      -- 3 - relationship between min_fp and Current_Minor_Frame
        (for all c in cpu_number =>
             (if(st.ideal_maj_fp(c) = st.maj_fp and
                     st.ideal_cycles(c) = st.cycles) then
                  (st.min_fp(c) = st.Per_CPU_Storage(c).Current_Minor_Frame)))

      and

      -- 4 - relationship between vmx and min_ticks
        (for all c in cpu_number =>
             (if(st.ideal_maj_fp(c) = st.maj_fp and
                     st.ideal_cycles(c) = st.cycles) then
                  (st.vmx_timer(c) = st.Scheduling_Plans(c)
                   (st.maj_fp).Minor_Frames(st.min_fp(c)).Deadline -
                     st.min_ticks(c))))

      and

      -- 5A - relationship between barrier and enabled
        (for all c in cpu_number =>
             (if(st.enabled(c) = False) then
                  (st.Barrier_Set(c) = True)
              else (st.Barrier_Set(c) = False)))

      and

      -- 5B
        (for all c in cpu_number =>
             (if(st.Barrier_Set(c) = True) then
                  (st.enabled(c) = False)
              else (st.enabled(c) = True)))

      and

      -- 6 - Registers of a subject are equal to CPU register value and
      --            when it is one of the current subject
      --     and
      --     it should be equal to VMCS and descriptor register values
      --            when it is not the current subject
        (for all s in subject_number =>
             (if(st.current_subject(s) = True) then
                  (st.subjects_state(s) =
                         st.CPU_Regs(st.Subject_Specs(s).CPU_Id))
              else
                (st.subjects_state(s) = st.Descriptors(s))))
     );

end initialization1;
