with SK; use SK;
with numbers1; use numbers1;
with SK.CPU;
with SK.Barriers;

package absinit
with SPARK_Mode is

   type subject_state_array is array(subject_number) of Subject_State_Type;

   type tick_type is array (cpu_number) of SK.Word64;

   type minor_fp_array is array (cpu_number) of Minor_Frame_Range;
   type major_fp_array is array (cpu_number) of Major_Frame_Range;

   type boolean_array is array (cpu_number) of Boolean;

   type Major_Frame_Info_Type is record
      Period         : SK.Word64;
      Barrier_Config : Barrier_Config_Array;
   end record;

   type Major_Frame_Info_Array is array (Major_Frame_Range)
     of Major_Frame_Info_Type;

   type Minor_Frame_Type is record
      Group_ID : Scheduling_Group_Range;
      Barrier  : Barrier_Index_Range;
      Deadline : SK.Word64;
   end record;

   type Minor_Frame_Array is array (Minor_Frame_Range) of Minor_Frame_Type;

   type Major_Frame_Type is record
      Length       : Minor_Frame_Range;
      Minor_Frames : Minor_Frame_Array;
   end record;

   type Major_Frame_Array is array (Major_Frame_Range) of Major_Frame_Type;

   type Scheduling_Plan_Type is array (cpu_number) of Major_Frame_Array;

   -- types to define subject specs
   type VMX_Controls_Type is record
      Exec_Pin    : SK.Word32;
      Exec_Proc   : SK.Word32;
      Exec_Proc2  : SK.Word32;
      Exit_Ctrls  : SK.Word32;
      Entry_Ctrls : SK.Word32;
   end record;

   type Trap_Range is range 0 .. 59;

   type Event_Range is range 0 .. 31;

   Invalid_Subject : constant Integer := Integer(subject_number'Last) + 1;

   subtype Dst_Subject_Type is Natural range 0 .. Invalid_Subject;

   type Vector_Range is range 0 .. 255;

   Invalid_Vector : constant := 256;

   type Dst_Vector_Range is range 0 .. Invalid_Vector;

   type Trap_Entry_Type is record
      Dst_Subject : Dst_Subject_Type;
      Dst_Vector  : Dst_Vector_Range;
   end record;

   type Event_Entry_Type is record
      Dst_Subject : Dst_Subject_Type;
      Dst_Vector  : Dst_Vector_Range;
      Handover    : Boolean;
      Send_IPI    : Boolean;
   end record;

   type Trap_Table_Type is array (Trap_Range) of Trap_Entry_Type;

   type Event_Table_Type is array (Event_Range) of Event_Entry_Type;

   type Subject_Spec_Type is record
      CPU_Id             : cpu_number;
      PML4_Address       : SK.Word64;
      EPT_Pointer        : SK.Word64;
      VMCS_Address       : SK.Word64;
      IO_Bitmap_Address  : SK.Word64;
      MSR_Bitmap_Address : SK.Word64;
      MSR_Store_Address  : SK.Word64;
      Stack_Address      : SK.Word64;
      Entry_Point        : SK.Word64;
      CR0_Value          : SK.Word64;
      CR0_Mask           : SK.Word64;
      CR4_Value          : SK.Word64;
      CR4_Mask           : SK.Word64;
      CS_Access          : SK.Word32;
      Exception_Bitmap   : SK.Word32;
      MSR_Count          : SK.Word32;
      VMX_Controls       : VMX_Controls_Type;
      Trap_Table         : Trap_Table_Type;
      Event_Table        : Event_Table_Type;
   end record;

   type Subject_Spec_Array is array (subject_number) of Subject_Spec_Type;

   type Pseudo_Descriptor_Type is record
      Limit : SK.Word16;
      Base  : SK.Word64;
   end record;

   type subject_boolean_array is array (subject_number) of Boolean;

   type system_state is record

      Scheduling_Plans : Scheduling_Plan_Type;
      Subject_Specs : Subject_Spec_Array;
      Major_Frames : Major_Frame_Info_Array;

   -- abstract
      Scheduling_Groups: Scheduling_Group_Array;
      subjects_state   : subject_state_array;

      ticks            : tick_type;
      min_ticks        : tick_type;
      cycles           : SK.Word64;
      ideal_cycles     : tick_type;

      min_fp           : minor_fp_array;
      maj_fp           : Major_Frame_Range;
      ideal_maj_fp     : major_fp_array;

      L                : SK.Word64;
      major_frame_ends : Major_Frame_Info_Array;
      enabled          : boolean_array;
      last             : boolean_array;
      no_of_last       : cpu_number;

      current_subject  : subject_boolean_array;

   end record;

   state : system_state;

   procedure initialize(state : in out system_state);

end absinit;
