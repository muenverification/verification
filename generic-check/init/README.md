# Code for Verification of Init Operation #

Init operation does not take any input nor does it return any output.

The operation is declared at lines 187-189 in `./src/init.ads`  with pre and post conditions.

### File containing combined abstract-concrete code ###

`./src/init.adb` and `./src/init.ads`


### Combined State ###

Defined in lines 133-182 in `./src/init.ads`

There are two components of the state:

1. Abstract state (lines 139-159)
2. Concrete state (lines 162-181)

Abstract state components include:

1. `Scheduling_Groups` - Used to specify subjects corresponding to a minor frame.
2. `subjects_state` - This is a per-subject array used to specify the states of a subject, in particular the register state.
3. `ticks` - It keeps track of the number of ticks passed in a cycle. It is reset at the end of a cycle.
4. `min_ticks` - It keeps track of the number of ticks within a major frame. It is reset at the end of a major frame.
5. `cycles` - It keeps track of the number of cycles passed when there is a barrier.
6. `ideal_cycles` - It keeps track of the number of cycles passed when there is no barrier.
7. `min_fp` - It is the current minor frame pointer. This is a local counter for each logical CPU.
8. `maj_fp` - It is the current major frame pointer when there is a barrier. This is a global counter for all logical CPUs
9. `ideal_maj_fp` - It is the current major frame pointer for each logical CPU when there is no barrier.
10. `L` - It is the length of the cycle. This is a parameter.
11. `major_frame_ends` - This is an array which stores the number of ticks elapsed at the end of a major frame. This is a parameter.
12. `enabled` - This is a Boolean array indexed by the CPU number. If a logical CPU is waiting in a barrier, it is said to be not enabled and the corresponding Boolean value in `enabled` will be false. Otherwise it will be said to be enabled and the corresponding Boolean value in `enabled` will be true.
13. `last` - This is also a Boolean array indexed by the CPU number. If a logical CPU is the last one in the major
14. `no_of_last` - It keeps track of the number of logical CPUs for which `last` is true.
15. `current_subject` - This is Boolean array indexed by Subject IDs. If a subject is not active, the corresponding entry is false and vice versa.

Concrete state components include:

1. `tsc` - This models the Time Stamp Counter register for each CPU.
2. `vmx_timer` - This models the Time Stamp Counter register for each CPU.
3. `VMCSs` - This is an array of VMCS whose type `VMCStype` is defined in lines 94-184 of `sk-cpu.ads`.This is an array of VMCS whose type `VMCStype` is defined in lines 94-184 of `sk-cpu.ads`.
4. `VMCS_Pointer` - This models the VMCS pointer register. Here VMCS_Pointer stores the ID of currently active subject instead of the address of the VMCS.
5. `Per_CPU_Storage` - This data structure stores the current minor frame on each CPU
6. `Current_Major_Frame` - This is used to keep track of the current major frame.
7. `Current_Major_Start_Cycles` - This is used to keep track of the scheduled time in terms of ticks at the start of a major frame.
8. `All_Barrier` - This is a record type which is used to implement the sense-reversal barrier in Muen. It contains the fields `sense`, `wait_count` and `size`.
9. `Barrier_Set` - This is an auxiliary Boolean array to keep track of the CPUs which are in the barrier.
10. `Descriptors` - This structure stores the state of every subject when it is not active.
12. `CPU_Regs` - This structure models the CPU registers for each CPU.
13. `IDT_Pointer` - This models IDTR
14. `GDT_Pointer` - This models GDTR

In addition to these there are some state components which are common to both abstract and concrete. (lines 135-137)

1. `Scheduling_Plans` - This data structure is an array of major frames per CPU. This is a parameter.
2. `Major_Frames` - This array is indexed by `Major_Frame_Range` and contains length of each major frame in terms of ticks. This is a parameter.
3. `Subject_Specs` - This data structure contains information about the initial states of the subjects. It also contain the address of page tables to be used for the subjects. This is used to initialize the VMCS. This is a parameter.

### Gluing invariant ###

Defined in lines 633-720 in `./src/init.ads`.

It is conjunction of following properties:

1. For all the CPUs, the difference between `tsc` and `Current_Major_Start_Cycles` should be equal to `min_ticks` if `ideal_cycles` match with `cycles`. (lines 635-666)

2. `maj_fp` should be equal to `Current_Major_Frame` (lines 670-671)

3. For all CPUs `min_fp` should be equal to `Current_Minor_Frame`. (lines 675-679)

4. For all CPUs, if ideal and non-ideal values of `cycles` and `maj_fp` match, `vmx_timer` (which represents the remaining time) and `min_ticks` (which represents the time elapsed since the beginning of the major frame) add upto the deadline of the minor frame. (lines 683-689)

5. A CPU is (not) in barrier if and only if the `enabled` value is false (true). (lines 693-705)

6. Contents of CPU registers are same as those of abstract subjects when the subjects are active. When subjects are not active contents of descriptors are same as those of subject state. (lines 709-719)

### Condition R ###

Defined in lines 193-332 in `./src/init.ads`.

It is conjunction of following properties:

1. Deadlines of all minor frames are greater than 0. (lines 193-197)

2. Deadlines of minor frames within a major frame are strictly increasing. (lines 201-207)

3. Values in `major_frame_ends` are greater than 0 and are strictly increasing. (lines 211-222)

4. The value of `Major frames` for a major frame is equal to the deadline of the last minor frame in the major frame in `Scheduling_Plans`. (lines 226-232)

5. Difference between two consecutive values in `major_frame_ends` is length of a major frame. (lines 236-247)

6. `L` which is the length of a cycle in `Scheduling_Plans` is equal to the last value in `major_frame_ends`. (lines 251-252)

7. Deadlines of minor frames in a major frame are monotonous (lines 256-300)

8. `major_frame_ends` is monotonic. (lines 304-332)

### Abstract Code for Init Operation ###

Lines 14-96 in `./src/init.adb`

Briefly it does the following:

1. Initializes `subjects_state` for every subject to `Null_Subject_State`. (lines 17-23)
2. Initializes `RIP`, `RSP` and `CR0` in `subjects_state` for every subject to the values in `Entry_Point`, `Stack_Address` and `CR_0` of corresponding subjects in `Subject_Specs`. (lines 28-40)
3. Initializes `ticks`, `min_ticks` and `ideal_cycles` for every CPU to 0. (lines 48-57)
4. Initializes `cycles` to 0. (line 62)
5. Initializes `min_fp`, `ideal_maj_fp` to point to first minor frame and major frame on each CPU. (lines 70-71)
6. Initializes `enabled` and `last` to be `True` for each CPU. (lines 73-74)
7. Initializes `maj_fp` to point to the first major frame. (line 83)
8. Initializes `no_of_last` to number of CPUs. (line 85)
9. Initializes `L` to point to the length of the cycle. (line 87)
10. Initializes `current_subject` to `False` for each subject. (lines 93-98)
11. Initializes `current_subject` to `True` for first minor frames. (lines 103-118)

### Concrete Code for Init Operation ###

Lines  in `./src/init.adb`

Briefly it does the following:

1. Initializes `tsc` to 0 for all CPUs. (lines 125-129)
2. Initializes `Current_Major_Frame` to point to the first major frame. (line 146)
3. Initializes `Current_Major_Start_Cycles` to 0. (line 148)
4. Initializes `Current_Minor_Frame` on each CPU to point to the first minor frame. (line 150)
5. VMCS of each subject is initialized. (lines 160-446)
6. `RIP`, `RSP` and `CR0` in Descriptors are initialized to `Entry_Point`, `Stack_Address` and `CR0_Value` values in `Subject_Specs` for the corresponding subjects. (lines 491-517)
7. On each CPU VMCS_Pointer is set to point to the subject on the first minor frame. (line 522) 
8. VMX preemption timer (`VMX`) is set to the deadlines for the current minor frame on each CPU. (lines 566-608)
9. Restoring state of subjects which are first minor frame on each CPU (lines 615-639)