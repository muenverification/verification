# Muen Verification #

### What does this repository contain? ###

This repository contains the artifacts of the verification of the Muen kernel.

### Structure of the repository ###

Our proof consists of two steps:

* generic step to prove refinement of parametric machines irrespective of the input policy.

* policy-specific check to check the condition $R$ on the parameters.

Accordingly we have organized the artifacts into two folders `generic-check` and `policy-specific-check`. `generic-check` folder contains the artifact of the generic step. `policy-specific-check` folder contains the artifact of the policy-specific check step which is a tool which takes policy as input and checks condition R.

In the generic step we show that (init) and (sim) conditions of the parametric gluing relation $\pi$ with respect to the condition $R$, as defined in the thesis, are satisfied for each of the four operations including \init. 

Therefore we have four sub-folders in the `generic-check` folder. These folders are named `handle-interrupt`, `init`,  `memory-write`,  and `tick`.

In each of the folders inside the `generic-check` folder we have added a file which is also called `README.md`. 

This file documents the abstract-concrete state, abstract and concrete operations, the parametric gluing relation $\pi$, the condition $R$, and the refinement conditions specified in the code. In each operation we only use part of the state which gets affected by the operation. Similarly for each operation a subset of conjuncts of the gluing relation is used.

### Structure of README.md in sub-folders for each operation in `generic-check`###

In each of the sub-folders for each operation there is a README file. These README contain the following information 

1. Input and output of the operation.
2. Names of the files which contain the combined abstract-concrete code for the operation.
3. Explanation of abstract and concrete state
4. Gluing relation specification in the code
5. Condition R specification in the code
6. Explanation of abstract and concrete code
7. Names of files for each of the cases to simplify the proof

### Basics of SPARK Ada files ###

There are usually two files for a program

* .adb file - file for body
* .ads file - file for specification

The specification involves the definition of types, signatures and function contracts of functions and procedures.

### Link to Muen website ###

https://muen.sk/

### Link to Muen source code ###

https://bit.ly/2Tgo2ZU